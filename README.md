## Django setup

```
$ adduser --system jardines
$ sudo -u jardines -s
$ git clone https://gitlab.com/sergioh/jardines.git
$ virtualenv -p python3 env
$ source env/bin/activate
$ cd jardines
$ pip install -r requirements.txt
$ cp jardines/local_settings_example.py jardines/local_settings.py
```

Edit local_settings.py and then

```
$ ./manage.py migrate
$ ./manage.py collectstatic
```

## Apache + mod_wsgi

```
$ apt install libapache2-mod-wsgi-py3
$ a2enmod wsgi
```

### Apache vhost

```
Alias /static /var/www/$FQDN/jardines/assets/static
<Directory "/var/www/$FQDN/jardines/assets/static">
     Order deny,allow
     Allow from all
</Directory>
WSGIDaemonProcess jardines user=jardines python-home=/var/www/$FQDN/env python-path=/var/www/$FQDN/jardines
WSGIScriptAlias / /var/www/$FQDN/jardines/jardines/wsgi.py process-group=jardines
# Pass token authentication headers
WSGIPassAuthorization On
DocumentRoot /var/www/$FQDN/jardines
```

### Apache vhost SSL

Don't include SSLCertificateChainFile
```
SSLCertificateFile      /etc/letsencrypt/live/candelaria.tejido.io/cert.pem
SSLCertificateKeyFile   /etc/letsencrypt/live/candelaria.tejido.io/privkey.pem
#SSLCertificateChainFile /etc/letsencrypt/live/candelaria.tejido.io/chain.pem
```

### Apache SSL conf

Allow at least TLSv1.1
```
SSLCipherSuite HIGH:!aNULL
SSLProtocol all -SSLv2 -SSLv3 -TLSv1
```

## HTTP POST from NodeMCU using httpclient with SSL

Get the certificate from your site (443=HTTPS port)

```
openssl s_client -connect dweet.io:443
```

Copy the certificate (from “-----BEGIN CERTIFICATE-----” to “-----END CERTIFICATE-----”) and paste into a file (cert.perm).

Then use the cert.perm to generate SHA1 fingerprint

```
openssl x509 -noout -in ./cert.perm -fingerprint -sha1
```

Copy the SHA1 key, and put into your code.

```
const char* fingerprint = "AA:BB:....";
http.begin(url, fingerprint);
http.addHeader("Content-Type", "application/json");
http.addHeader("Authorization", "Token 1234567.....");


```

## HTTP POST from Arduino and ESP12 WiFiEsp with SSL

If your ESP firmware does not support SNI for SSL, yo have to use a dedicated IP address for you vhost

```
char server[] = "$FQDN";
if (client.connectSSL(server, 443)) {
    Serial.println("Connected to server");
    client.println("GET /path/ HTTP/1.1");
    client.println(F("Host: $FQDN:443"));
    client.println("Content-Type: application/json");
    client.println("Authorization: Token 1234567.....");
    client.println("Connection: close");
    client.println();
}
```

## Get weather data from external api (opendata.aemet.es)

Add api_key to local_settings.py and add cronjob:

```
# update weather from external api
11 * * * *     $USER   /var/www/$FQDN/env/bin/python /var/www/$FQDN/jardines/manage.py aemet
' ' ' 
