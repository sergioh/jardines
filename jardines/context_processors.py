from django.conf import settings

def site_processor(request):
    """Injects into global context information about the site"""

    document_title       = settings.DOCUMENT_TITLE
    document_description = settings.DOCUMENT_DESCRIPTION
    debug     = settings.DEBUG
    debug_js  = settings.DEBUG_JS
    debug_css = settings.DEBUG_CSS

    return locals()
