import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'YOUR SECRET KEY'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [
 'APP_FQDN',
]

TIME_ZONE = 'Europe/Madrid'

STATIC_ROOT = os.path.join(BASE_DIR, 'assets/static')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'DBNAME',
        'USER': 'DBUSER',
        'PASSWORD': 'DBPASSWORD',
        'HOST': 'localhost',
    }
}

AEMET_API_KEY = 'opendata.aemet.es_KEY'
