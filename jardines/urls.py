from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.views.generic import TemplateView

# project
from api import urls as api_urls

urlpatterns = [
    # API
    # v1.0
    path(
        'api/',
        include(api_urls, namespace="api")
    ),
    path(
        'admin/', 
        admin.site.urls
    ),
    path(
        'cpanel', 
        TemplateView.as_view(
            template_name='cpanel.html'
        ), 
        name='front'
    ),
    path(
        '', 
        TemplateView.as_view(
            template_name='page-water.html'
        ), 
        name='front'
    ),
    path(
    'temperatura', 
    TemplateView.as_view(
    template_name='page-temperature.html'
    ), 
    name='front'
    )
]
