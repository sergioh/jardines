/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"temperature": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index-temperature.js","vendors~temperature"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn-bd": "./node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-mx": "./node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "./node_modules/moment/locale/es-mx.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/index-temperature.js":
/*!**********************************!*\
  !*** ./src/index-temperature.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Dependencies
const d3 = __webpack_require__(/*! d3 */ "./node_modules/d3/src/index.js");

const leaflet = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");

const moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js"); // default namespace for SVGs


const NS = "http://www.w3.org/2000/svg";
const today = new Date();
let monthago = new Date();
monthago.setMonth(monthago.getMonth() - 1);
playing = false; // When DOM is ready

document.addEventListener('DOMContentLoaded', function () {
  // player buttons
  var presentation = document.querySelector('.presentation');
  const slides = document.querySelectorAll('.presentation__page');
  var player = document.querySelector('.player');
  document.querySelector('[data-action="play"]').addEventListener('click', function () {
    player.dataset.on = "play";
    playing = true;
  });
  document.querySelector('[data-action="pause"]').addEventListener('click', function () {
    player.dataset.on = "pause";
    playing = false;
  });
  setInterval(() => {
    if (playing) {
      presentation.dataset.current = (+presentation.dataset.current + 1) % slides.length;
      var next = slides.item(+presentation.dataset.current);
      window.scrollTo(0, next.getBoundingClientRect().top + window.scrollY);
    }
  }, 8000);
  let flowdata, sensordata;
  fetch('/api/sensor').then(response => response.json()).then(data => {
    // Format data properly
    data.forEach(i => {
      i.fdate = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
    });
    sensordata = d3.group(data, item => item.name);
    var min_date = d3.min(data, item => item.fdate); // Group data

    fetch('/api/weather').then(response => response.json()).then(aeroport_data => {
      aeroport_data.forEach(i => {
        i.fdate = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
      });
      let min = d3.max(sensordata.get('au2'), i => i.fdate);
      min.setMonth(min.getMonth() - 1);
      let max = d3.max(sensordata.get('au2'), i => i.fdate);
      let au2 = sensordata.get('au2').filter(i => i.fdate > min);
      let au4 = sensordata.get('au4').filter(i => i.fdate > min); // Get average difference between au2 and au4

      const au2_avg = d3.mean(au2, d => d.temperature);
      const au4_avg = d3.mean(au4, d => d.temperature);
      let diff = Math.abs(au2_avg - au4_avg).toFixed(2);
      document.querySelector('#au2_au4 .chart__chart--number').innerHTML = `+${diff}°`; // Get average difference between e0 and aeroport

      const e0 = sensordata.get('e0').filter(i => i.fdate > min);
      const e0_avg = d3.mean(e0, d => d.temperature);
      let a = aeroport_data.filter(i => i.fdate > min && i.fdate < max);
      const ae_avg = d3.mean(a, d => d.temperature);
      diff = Math.abs(e0_avg - ae_avg).toFixed(2); // document.querySelector('#e0_ae .chart__chart--number').innerHTML = `${diff}°`;
      // Add chart wrapper

      let svg = d3.select('#chart-average--month');
      const width = 800;
      const height = 600;
      svg.attr('width', width).attr('height', height); // Create chart scales and add them to chart

      let x = d3.scaleTime().domain([min, max]).range([0, width]);
      svg.append("g").attr('class', 'chart__x').attr("transform", `translate(0, ${height})`).call(d3.axisBottom(x).ticks(15).tickFormat(d => {
        return moment(d).format('DD-MM');
      }));
      const y = d3.scaleLinear().domain([0, 50]).range([height, 0]);
      svg.append("g").attr('class', 'chart__y').attr("transform", `translate(0, 0)`).call(d3.axisLeft(y).tickFormat(d => d + "° ")).selectAll('text').attr('x', '-20px');
      svg.append('path').attr('class', 'chart__line chart__line--au4').datum(au2).attr('d', d3.line().x(d => x(d.fdate)).y(d => y(d.temperature)));
      svg.append('path').attr('class', 'chart__line chart__line--au2').datum(au4).attr('d', d3.line().x(d => x(d.fdate)).y(d => y(d.temperature)));
      svg.append('path').attr('class', 'chart__line chart__line--aeroport').datum(a).attr('d', d3.line().x(d => x(d.fdate)).y(d => y(d.temperature))); // Add chart wrapper

      svg = d3.select('#chart-average--week');
      svg.attr('width', width).attr('height', height); // Create chart scales and add them to chart

      max = moment('20210802', 'YYYYMMDD');
      min = moment(max).subtract(6, 'days');
      x = d3.scaleTime().domain([min, max]).range([0, width]);
      svg.append("g").attr('class', 'chart__x').attr("transform", `translate(0, ${height})`).call(d3.axisBottom(x).ticks(7).tickFormat(d => {
        return moment(d).format('DD-MM');
      }));
      svg.append("g").attr('class', 'chart__y').attr("transform", `translate(0, 0)`).call(d3.axisLeft(y).tickFormat(d => d + "° ")).selectAll('text').attr('x', '-20px');
      au2 = au2.filter(i => i.fdate > min && i.fdate < max);
      au4 = au4.filter(i => i.fdate > min && i.fdate < max);
      a = a.filter(i => i.fdate > min && i.fdate < max);
      svg.append('path').attr('class', 'chart__line chart__line--au4').datum(au2).attr('d', d3.line().x(d => x(d.fdate)).y(d => y(d.temperature)));
      svg.append('path').attr('class', 'chart__line chart__line--au2').datum(au4).attr('d', d3.line().x(d => x(d.fdate)).y(d => y(d.temperature)));
      svg.append('path').attr('class', 'chart__line chart__line--aeroport').datum(a).attr('d', d3.line().x(d => x(d.fdate)).y(d => y(d.temperature))); // Get average of minimum tempaeratures

      let measures = {};
      au2.forEach(i => {
        const dat = moment(i.fdate).format('DDMM');

        if (!(dat in measures)) {
          measures[dat] = [i.temperature];
        } else {
          measures[dat].push(i.temperature);
        }
      });
      let minimum = [];

      for (day in measures) {
        minimum.push(d3.min(measures[day], i => i));
      }

      const avg_min = d3.mean(minimum);
      const avg = d3.mean(au2, i => i.temperature);
      diff = Math.abs(avg - avg_min).toFixed(2);
      document.querySelector('#au2_min .chart__chart--number').innerHTML = `+${diff}°`; // Map

      fetch('/static/jardines/js/data/countries.geo.json').then(i => i.json()).then(data => {
        var map = L.map('map', {
          center: [20, 0],
          zoom: 3,
          maxZoom: 18,
          attributionControl: false,
          zoomControl: false,
          scrollWheelZoom: false
        });
        L.geoJson(data, {}).addTo(map);
        const tts = [{
          t: d3.max(au2.filter(i => i.fdate == d3.max(au2, i => i.fdate)), i => i.temperature),
          c: 'yellow'
        }, {
          t: 20,
          c: 'blue'
        }, {
          t: 36,
          c: 'red'
        }];
        fetch('/static/jardines/js/data/data.json').then(i => i.json()).then(data => {
          for (const [k, v] of Object.entries(data)) {
            tts.forEach(ref => {
              console.log(parseInt(max.format('M')));
              var val = Math.abs(parseFloat(v['average_temperature'][parseInt(max.format('M'))]) - ref.t);

              if (v.lat && v.lon && val <= 1) {
                L.marker([parseFloat(v.lat), parseFloat(v.lon)], {
                  icon: L.divIcon({
                    iconSize: [20, 20],
                    iconAnchor: [10, 10],
                    html: `<div class="inner ${ref.c}"></div>`
                  })
                }).addTo(map);
              }
            });
          }

          ;
        });
      });
    });
  });
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUgc3luYyBeXFwuXFwvLiokIiwid2VicGFjazovLy8uL3NyYy9pbmRleC10ZW1wZXJhdHVyZS5qcyJdLCJuYW1lcyI6WyJkMyIsInJlcXVpcmUiLCJsZWFmbGV0IiwibW9tZW50IiwiTlMiLCJ0b2RheSIsIkRhdGUiLCJtb250aGFnbyIsInNldE1vbnRoIiwiZ2V0TW9udGgiLCJwbGF5aW5nIiwiZG9jdW1lbnQiLCJhZGRFdmVudExpc3RlbmVyIiwicHJlc2VudGF0aW9uIiwicXVlcnlTZWxlY3RvciIsInNsaWRlcyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJwbGF5ZXIiLCJkYXRhc2V0Iiwib24iLCJzZXRJbnRlcnZhbCIsImN1cnJlbnQiLCJsZW5ndGgiLCJuZXh0IiwiaXRlbSIsIndpbmRvdyIsInNjcm9sbFRvIiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwidG9wIiwic2Nyb2xsWSIsImZsb3dkYXRhIiwic2Vuc29yZGF0YSIsImZldGNoIiwidGhlbiIsInJlc3BvbnNlIiwianNvbiIsImRhdGEiLCJmb3JFYWNoIiwiaSIsImZkYXRlIiwidGltZVBhcnNlIiwiZ3JvdXAiLCJuYW1lIiwibWluX2RhdGUiLCJtaW4iLCJhZXJvcG9ydF9kYXRhIiwibWF4IiwiZ2V0IiwiYXUyIiwiZmlsdGVyIiwiYXU0IiwiYXUyX2F2ZyIsIm1lYW4iLCJkIiwidGVtcGVyYXR1cmUiLCJhdTRfYXZnIiwiZGlmZiIsIk1hdGgiLCJhYnMiLCJ0b0ZpeGVkIiwiaW5uZXJIVE1MIiwiZTAiLCJlMF9hdmciLCJhIiwiYWVfYXZnIiwic3ZnIiwic2VsZWN0Iiwid2lkdGgiLCJoZWlnaHQiLCJhdHRyIiwieCIsInNjYWxlVGltZSIsImRvbWFpbiIsInJhbmdlIiwiYXBwZW5kIiwiY2FsbCIsImF4aXNCb3R0b20iLCJ0aWNrcyIsInRpY2tGb3JtYXQiLCJmb3JtYXQiLCJ5Iiwic2NhbGVMaW5lYXIiLCJheGlzTGVmdCIsInNlbGVjdEFsbCIsImRhdHVtIiwibGluZSIsInN1YnRyYWN0IiwibWVhc3VyZXMiLCJkYXQiLCJwdXNoIiwibWluaW11bSIsImRheSIsImF2Z19taW4iLCJhdmciLCJtYXAiLCJMIiwiY2VudGVyIiwiem9vbSIsIm1heFpvb20iLCJhdHRyaWJ1dGlvbkNvbnRyb2wiLCJ6b29tQ29udHJvbCIsInNjcm9sbFdoZWVsWm9vbSIsImdlb0pzb24iLCJhZGRUbyIsInR0cyIsInQiLCJjIiwiayIsInYiLCJPYmplY3QiLCJlbnRyaWVzIiwicmVmIiwiY29uc29sZSIsImxvZyIsInBhcnNlSW50IiwidmFsIiwicGFyc2VGbG9hdCIsImxhdCIsImxvbiIsIm1hcmtlciIsImljb24iLCJkaXZJY29uIiwiaWNvblNpemUiLCJpY29uQW5jaG9yIiwiaHRtbCJdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsUUFBUSxvQkFBb0I7UUFDNUI7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxpQkFBaUIsNEJBQTRCO1FBQzdDO1FBQ0E7UUFDQSxrQkFBa0IsMkJBQTJCO1FBQzdDO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsZ0JBQWdCLHVCQUF1QjtRQUN2Qzs7O1FBR0E7UUFDQTtRQUNBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ3ZKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkU7Ozs7Ozs7Ozs7O0FDblNBO0FBRUEsTUFBTUEsRUFBRSxHQUFHQyxtQkFBTyxDQUFDLDBDQUFELENBQWxCOztBQUNBLE1BQU1DLE9BQU8sR0FBR0QsbUJBQU8sQ0FBQywyREFBRCxDQUF2Qjs7QUFDQSxNQUFNRSxNQUFNLEdBQUdGLG1CQUFPLENBQUMsK0NBQUQsQ0FBdEIsQyxDQUVBOzs7QUFDQSxNQUFNRyxFQUFFLEdBQUcsNEJBQVg7QUFDQSxNQUFNQyxLQUFLLEdBQUksSUFBSUMsSUFBSixFQUFmO0FBQ0EsSUFBSUMsUUFBUSxHQUFHLElBQUlELElBQUosRUFBZjtBQUNBQyxRQUFRLENBQUNDLFFBQVQsQ0FBbUJELFFBQVEsQ0FBQ0UsUUFBVCxLQUFzQixDQUF6QztBQUVBQyxPQUFPLEdBQUcsS0FBVixDLENBRUE7O0FBQ0FDLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsa0JBQTFCLEVBQThDLFlBQzlDO0FBRUk7QUFDQSxNQUFJQyxZQUFZLEdBQUdGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixlQUF2QixDQUFuQjtBQUNBLFFBQU1DLE1BQU0sR0FBR0osUUFBUSxDQUFDSyxnQkFBVCxDQUEwQixxQkFBMUIsQ0FBZjtBQUNBLE1BQUlDLE1BQU0sR0FBR04sUUFBUSxDQUFDRyxhQUFULENBQXVCLFNBQXZCLENBQWI7QUFDQUgsVUFBUSxDQUFDRyxhQUFULENBQXVCLHNCQUF2QixFQUErQ0YsZ0JBQS9DLENBQWdFLE9BQWhFLEVBQXlFLFlBQVU7QUFDL0VLLFVBQU0sQ0FBQ0MsT0FBUCxDQUFlQyxFQUFmLEdBQW9CLE1BQXBCO0FBQ0FULFdBQU8sR0FBRyxJQUFWO0FBQ0gsR0FIRDtBQUlBQyxVQUFRLENBQUNHLGFBQVQsQ0FBdUIsdUJBQXZCLEVBQWdERixnQkFBaEQsQ0FBaUUsT0FBakUsRUFBMEUsWUFBVTtBQUNoRkssVUFBTSxDQUFDQyxPQUFQLENBQWVDLEVBQWYsR0FBb0IsT0FBcEI7QUFDQVQsV0FBTyxHQUFHLEtBQVY7QUFDSCxHQUhEO0FBSUFVLGFBQVcsQ0FBRSxNQUFNO0FBQ2YsUUFBR1YsT0FBSCxFQUFZO0FBQ1JHLGtCQUFZLENBQUNLLE9BQWIsQ0FBcUJHLE9BQXJCLEdBQStCLENBQUUsQ0FBRVIsWUFBWSxDQUFDSyxPQUFiLENBQXFCRyxPQUF2QixHQUFpQyxDQUFuQyxJQUF5Q04sTUFBTSxDQUFDTyxNQUEvRTtBQUNBLFVBQUlDLElBQUksR0FBR1IsTUFBTSxDQUFDUyxJQUFQLENBQWEsQ0FBQ1gsWUFBWSxDQUFDSyxPQUFiLENBQXFCRyxPQUFuQyxDQUFYO0FBQ0FJLFlBQU0sQ0FBQ0MsUUFBUCxDQUNJLENBREosRUFFSUgsSUFBSSxDQUFDSSxxQkFBTCxHQUE2QkMsR0FBN0IsR0FBbUNILE1BQU0sQ0FBQ0ksT0FGOUM7QUFJSDtBQUNKLEdBVFUsRUFTUixJQVRRLENBQVg7QUFXQSxNQUFJQyxRQUFKLEVBQWNDLFVBQWQ7QUFFQUMsT0FBSyxDQUFDLGFBQUQsQ0FBTCxDQUFxQkMsSUFBckIsQ0FBMEJDLFFBQVEsSUFBSUEsUUFBUSxDQUFDQyxJQUFULEVBQXRDLEVBQXVERixJQUF2RCxDQUE0REcsSUFBSSxJQUFJO0FBQ2xFO0FBQ0FBLFFBQUksQ0FBQ0MsT0FBTCxDQUFhQyxDQUFDLElBQUk7QUFDaEJBLE9BQUMsQ0FBQ0MsS0FBRixHQUFVdkMsRUFBRSxDQUFDd0MsU0FBSCxDQUFhLG1CQUFiLEVBQWtDRixDQUFDLENBQUNDLEtBQXBDLENBQVY7QUFDRCxLQUZEO0FBR0FSLGNBQVUsR0FBRy9CLEVBQUUsQ0FBQ3lDLEtBQUgsQ0FBU0wsSUFBVCxFQUFlWixJQUFJLElBQUlBLElBQUksQ0FBQ2tCLElBQTVCLENBQWI7QUFDQSxRQUFJQyxRQUFRLEdBQUczQyxFQUFFLENBQUM0QyxHQUFILENBQU9SLElBQVAsRUFBYVosSUFBSSxJQUFJQSxJQUFJLENBQUNlLEtBQTFCLENBQWYsQ0FOa0UsQ0FRbEU7O0FBQ0FQLFNBQUssQ0FBQyxjQUFELENBQUwsQ0FBc0JDLElBQXRCLENBQTJCQyxRQUFRLElBQUlBLFFBQVEsQ0FBQ0MsSUFBVCxFQUF2QyxFQUF3REYsSUFBeEQsQ0FBNkRZLGFBQWEsSUFDMUU7QUFDSUEsbUJBQWEsQ0FBQ1IsT0FBZCxDQUFzQkMsQ0FBQyxJQUFJO0FBQ3pCQSxTQUFDLENBQUNDLEtBQUYsR0FBVXZDLEVBQUUsQ0FBQ3dDLFNBQUgsQ0FBYSxtQkFBYixFQUFrQ0YsQ0FBQyxDQUFDQyxLQUFwQyxDQUFWO0FBQ0QsT0FGRDtBQUlBLFVBQUlLLEdBQUcsR0FBRzVDLEVBQUUsQ0FBQzhDLEdBQUgsQ0FBUWYsVUFBVSxDQUFDZ0IsR0FBWCxDQUFlLEtBQWYsQ0FBUixFQUErQlQsQ0FBQyxJQUFJQSxDQUFDLENBQUNDLEtBQXRDLENBQVY7QUFDQUssU0FBRyxDQUFDcEMsUUFBSixDQUFjb0MsR0FBRyxDQUFDbkMsUUFBSixLQUFpQixDQUEvQjtBQUNBLFVBQUlxQyxHQUFHLEdBQUc5QyxFQUFFLENBQUM4QyxHQUFILENBQVFmLFVBQVUsQ0FBQ2dCLEdBQVgsQ0FBZSxLQUFmLENBQVIsRUFBK0JULENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxLQUF0QyxDQUFWO0FBRUEsVUFBSVMsR0FBRyxHQUFHakIsVUFBVSxDQUFDZ0IsR0FBWCxDQUFlLEtBQWYsRUFBc0JFLE1BQXRCLENBQThCWCxDQUFDLElBQUlBLENBQUMsQ0FBQ0MsS0FBRixHQUFVSyxHQUE3QyxDQUFWO0FBQ0EsVUFBSU0sR0FBRyxHQUFHbkIsVUFBVSxDQUFDZ0IsR0FBWCxDQUFlLEtBQWYsRUFBc0JFLE1BQXRCLENBQThCWCxDQUFDLElBQUlBLENBQUMsQ0FBQ0MsS0FBRixHQUFVSyxHQUE3QyxDQUFWLENBVkosQ0FZSTs7QUFDQSxZQUFNTyxPQUFPLEdBQUduRCxFQUFFLENBQUNvRCxJQUFILENBQVFKLEdBQVIsRUFBYUssQ0FBQyxJQUFJQSxDQUFDLENBQUNDLFdBQXBCLENBQWhCO0FBQ0EsWUFBTUMsT0FBTyxHQUFHdkQsRUFBRSxDQUFDb0QsSUFBSCxDQUFRRixHQUFSLEVBQWFHLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxXQUFwQixDQUFoQjtBQUNBLFVBQUlFLElBQUksR0FBR0MsSUFBSSxDQUFDQyxHQUFMLENBQVNQLE9BQU8sR0FBR0ksT0FBbkIsRUFBNEJJLE9BQTVCLENBQW9DLENBQXBDLENBQVg7QUFDQWhELGNBQVEsQ0FBQ0csYUFBVCxDQUF1QixnQ0FBdkIsRUFBeUQ4QyxTQUF6RCxHQUFzRSxJQUFHSixJQUFLLEdBQTlFLENBaEJKLENBa0JJOztBQUNBLFlBQU1LLEVBQUUsR0FBRzlCLFVBQVUsQ0FBQ2dCLEdBQVgsQ0FBZSxJQUFmLEVBQXFCRSxNQUFyQixDQUE2QlgsQ0FBQyxJQUFJQSxDQUFDLENBQUNDLEtBQUYsR0FBVUssR0FBNUMsQ0FBWDtBQUNBLFlBQU1rQixNQUFNLEdBQUc5RCxFQUFFLENBQUNvRCxJQUFILENBQVFTLEVBQVIsRUFBWVIsQ0FBQyxJQUFJQSxDQUFDLENBQUNDLFdBQW5CLENBQWY7QUFDQSxVQUFJUyxDQUFDLEdBQUlsQixhQUFhLENBQUNJLE1BQWQsQ0FBc0JYLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxLQUFGLEdBQVVLLEdBQVYsSUFBaUJOLENBQUMsQ0FBQ0MsS0FBRixHQUFVTyxHQUF0RCxDQUFUO0FBQ0EsWUFBTWtCLE1BQU0sR0FBR2hFLEVBQUUsQ0FBQ29ELElBQUgsQ0FBUVcsQ0FBUixFQUFXVixDQUFDLElBQUlBLENBQUMsQ0FBQ0MsV0FBbEIsQ0FBZjtBQUNBRSxVQUFJLEdBQUdDLElBQUksQ0FBQ0MsR0FBTCxDQUFTSSxNQUFNLEdBQUdFLE1BQWxCLEVBQTBCTCxPQUExQixDQUFrQyxDQUFsQyxDQUFQLENBdkJKLENBd0JJO0FBRUE7O0FBQ0EsVUFBSU0sR0FBRyxHQUFHakUsRUFBRSxDQUFDa0UsTUFBSCxDQUFVLHVCQUFWLENBQVY7QUFDQSxZQUFNQyxLQUFLLEdBQUcsR0FBZDtBQUNBLFlBQU1DLE1BQU0sR0FBRyxHQUFmO0FBQ0FILFNBQUcsQ0FBQ0ksSUFBSixDQUFTLE9BQVQsRUFBa0JGLEtBQWxCLEVBQXlCRSxJQUF6QixDQUE4QixRQUE5QixFQUF3Q0QsTUFBeEMsRUE5QkosQ0FnQ0k7O0FBQ0EsVUFBSUUsQ0FBQyxHQUFHdEUsRUFBRSxDQUFDdUUsU0FBSCxHQUFlQyxNQUFmLENBQXNCLENBQzFCNUIsR0FEMEIsRUFFMUJFLEdBRjBCLENBQXRCLEVBR0wyQixLQUhLLENBR0MsQ0FBQyxDQUFELEVBQUlOLEtBQUosQ0FIRCxDQUFSO0FBSUFGLFNBQUcsQ0FBQ1MsTUFBSixDQUFXLEdBQVgsRUFDR0wsSUFESCxDQUNRLE9BRFIsRUFDaUIsVUFEakIsRUFFR0EsSUFGSCxDQUVRLFdBRlIsRUFFc0IsZ0JBQWVELE1BQU8sR0FGNUMsRUFHR08sSUFISCxDQUdRM0UsRUFBRSxDQUFDNEUsVUFBSCxDQUFjTixDQUFkLEVBQWlCTyxLQUFqQixDQUF1QixFQUF2QixFQUEyQkMsVUFBM0IsQ0FBc0N6QixDQUFDLElBQUk7QUFDN0MsZUFBT2xELE1BQU0sQ0FBQ2tELENBQUQsQ0FBTixDQUFVMEIsTUFBVixDQUFpQixPQUFqQixDQUFQO0FBQ0gsT0FGSyxDQUhSO0FBTUEsWUFBTUMsQ0FBQyxHQUFHaEYsRUFBRSxDQUFDaUYsV0FBSCxHQUNQVCxNQURPLENBQ0EsQ0FBQyxDQUFELEVBQUksRUFBSixDQURBLEVBQ1NDLEtBRFQsQ0FDZSxDQUFDTCxNQUFELEVBQVMsQ0FBVCxDQURmLENBQVY7QUFFQUgsU0FBRyxDQUFDUyxNQUFKLENBQVcsR0FBWCxFQUNHTCxJQURILENBQ1EsT0FEUixFQUNpQixVQURqQixFQUVHQSxJQUZILENBRVEsV0FGUixFQUVzQixpQkFGdEIsRUFHR00sSUFISCxDQUdRM0UsRUFBRSxDQUFDa0YsUUFBSCxDQUFZRixDQUFaLEVBQWVGLFVBQWYsQ0FBMEJ6QixDQUFDLElBQUlBLENBQUMsR0FBRyxJQUFuQyxDQUhSLEVBSUc4QixTQUpILENBSWEsTUFKYixFQUtHZCxJQUxILENBS1EsR0FMUixFQUthLE9BTGI7QUFNQUosU0FBRyxDQUFDUyxNQUFKLENBQVcsTUFBWCxFQUNHTCxJQURILENBQ1EsT0FEUixFQUNpQiw4QkFEakIsRUFFR2UsS0FGSCxDQUVTcEMsR0FGVCxFQUdHcUIsSUFISCxDQUdRLEdBSFIsRUFHYXJFLEVBQUUsQ0FBQ3FGLElBQUgsR0FDUmYsQ0FEUSxDQUNOakIsQ0FBQyxJQUFJaUIsQ0FBQyxDQUFDakIsQ0FBQyxDQUFDZCxLQUFILENBREEsRUFFUnlDLENBRlEsQ0FFTjNCLENBQUMsSUFBSTJCLENBQUMsQ0FBQzNCLENBQUMsQ0FBQ0MsV0FBSCxDQUZBLENBSGI7QUFPQVcsU0FBRyxDQUFDUyxNQUFKLENBQVcsTUFBWCxFQUNHTCxJQURILENBQ1EsT0FEUixFQUNpQiw4QkFEakIsRUFFR2UsS0FGSCxDQUVTbEMsR0FGVCxFQUdHbUIsSUFISCxDQUdRLEdBSFIsRUFHYXJFLEVBQUUsQ0FBQ3FGLElBQUgsR0FDUmYsQ0FEUSxDQUNOakIsQ0FBQyxJQUFJaUIsQ0FBQyxDQUFDakIsQ0FBQyxDQUFDZCxLQUFILENBREEsRUFFUnlDLENBRlEsQ0FFTjNCLENBQUMsSUFBSTJCLENBQUMsQ0FBQzNCLENBQUMsQ0FBQ0MsV0FBSCxDQUZBLENBSGI7QUFPQVcsU0FBRyxDQUFDUyxNQUFKLENBQVcsTUFBWCxFQUNHTCxJQURILENBQ1EsT0FEUixFQUNpQixtQ0FEakIsRUFFR2UsS0FGSCxDQUVTckIsQ0FGVCxFQUdHTSxJQUhILENBR1EsR0FIUixFQUdhckUsRUFBRSxDQUFDcUYsSUFBSCxHQUNSZixDQURRLENBQ05qQixDQUFDLElBQUlpQixDQUFDLENBQUNqQixDQUFDLENBQUNkLEtBQUgsQ0FEQSxFQUVSeUMsQ0FGUSxDQUVOM0IsQ0FBQyxJQUFJMkIsQ0FBQyxDQUFDM0IsQ0FBQyxDQUFDQyxXQUFILENBRkEsQ0FIYixFQWpFSixDQXlFTTs7QUFDQVcsU0FBRyxHQUFHakUsRUFBRSxDQUFDa0UsTUFBSCxDQUFVLHNCQUFWLENBQU47QUFDQUQsU0FBRyxDQUFDSSxJQUFKLENBQVMsT0FBVCxFQUFrQkYsS0FBbEIsRUFBeUJFLElBQXpCLENBQThCLFFBQTlCLEVBQXdDRCxNQUF4QyxFQTNFTixDQTZFTTs7QUFDQXRCLFNBQUcsR0FBRzNDLE1BQU0sQ0FBQyxVQUFELEVBQWEsVUFBYixDQUFaO0FBQ0F5QyxTQUFHLEdBQUd6QyxNQUFNLENBQUMyQyxHQUFELENBQU4sQ0FBWXdDLFFBQVosQ0FBcUIsQ0FBckIsRUFBd0IsTUFBeEIsQ0FBTjtBQUNBaEIsT0FBQyxHQUFHdEUsRUFBRSxDQUFDdUUsU0FBSCxHQUFlQyxNQUFmLENBQXNCLENBQ3RCNUIsR0FEc0IsRUFFdEJFLEdBRnNCLENBQXRCLEVBR0QyQixLQUhDLENBR0ssQ0FBQyxDQUFELEVBQUlOLEtBQUosQ0FITCxDQUFKO0FBSUFGLFNBQUcsQ0FBQ1MsTUFBSixDQUFXLEdBQVgsRUFDR0wsSUFESCxDQUNRLE9BRFIsRUFDaUIsVUFEakIsRUFFR0EsSUFGSCxDQUVRLFdBRlIsRUFFc0IsZ0JBQWVELE1BQU8sR0FGNUMsRUFHR08sSUFISCxDQUdRM0UsRUFBRSxDQUFDNEUsVUFBSCxDQUFjTixDQUFkLEVBQWlCTyxLQUFqQixDQUF1QixDQUF2QixFQUEwQkMsVUFBMUIsQ0FBcUN6QixDQUFDLElBQUk7QUFDNUMsZUFBT2xELE1BQU0sQ0FBQ2tELENBQUQsQ0FBTixDQUFVMEIsTUFBVixDQUFpQixPQUFqQixDQUFQO0FBQ0gsT0FGSyxDQUhSO0FBTUFkLFNBQUcsQ0FBQ1MsTUFBSixDQUFXLEdBQVgsRUFDR0wsSUFESCxDQUNRLE9BRFIsRUFDaUIsVUFEakIsRUFFR0EsSUFGSCxDQUVRLFdBRlIsRUFFc0IsaUJBRnRCLEVBR0dNLElBSEgsQ0FHUTNFLEVBQUUsQ0FBQ2tGLFFBQUgsQ0FBWUYsQ0FBWixFQUFlRixVQUFmLENBQTBCekIsQ0FBQyxJQUFJQSxDQUFDLEdBQUcsSUFBbkMsQ0FIUixFQUlHOEIsU0FKSCxDQUlhLE1BSmIsRUFLR2QsSUFMSCxDQUtRLEdBTFIsRUFLYSxPQUxiO0FBTUFyQixTQUFHLEdBQUdBLEdBQUcsQ0FBQ0MsTUFBSixDQUFZWCxDQUFDLElBQUlBLENBQUMsQ0FBQ0MsS0FBRixHQUFVSyxHQUFWLElBQWlCTixDQUFDLENBQUNDLEtBQUYsR0FBVU8sR0FBNUMsQ0FBTjtBQUNBSSxTQUFHLEdBQUdBLEdBQUcsQ0FBQ0QsTUFBSixDQUFZWCxDQUFDLElBQUlBLENBQUMsQ0FBQ0MsS0FBRixHQUFVSyxHQUFWLElBQWlCTixDQUFDLENBQUNDLEtBQUYsR0FBVU8sR0FBNUMsQ0FBTjtBQUNBaUIsT0FBQyxHQUFLQSxDQUFDLENBQUNkLE1BQUYsQ0FBVVgsQ0FBQyxJQUFJQSxDQUFDLENBQUNDLEtBQUYsR0FBVUssR0FBVixJQUFpQk4sQ0FBQyxDQUFDQyxLQUFGLEdBQVVPLEdBQTFDLENBQU47QUFDQW1CLFNBQUcsQ0FBQ1MsTUFBSixDQUFXLE1BQVgsRUFDR0wsSUFESCxDQUNRLE9BRFIsRUFDaUIsOEJBRGpCLEVBRUdlLEtBRkgsQ0FFU3BDLEdBRlQsRUFHR3FCLElBSEgsQ0FHUSxHQUhSLEVBR2FyRSxFQUFFLENBQUNxRixJQUFILEdBQ1JmLENBRFEsQ0FDTmpCLENBQUMsSUFBSWlCLENBQUMsQ0FBQ2pCLENBQUMsQ0FBQ2QsS0FBSCxDQURBLEVBRVJ5QyxDQUZRLENBRU4zQixDQUFDLElBQUkyQixDQUFDLENBQUMzQixDQUFDLENBQUNDLFdBQUgsQ0FGQSxDQUhiO0FBT0FXLFNBQUcsQ0FBQ1MsTUFBSixDQUFXLE1BQVgsRUFDR0wsSUFESCxDQUNRLE9BRFIsRUFDaUIsOEJBRGpCLEVBRUdlLEtBRkgsQ0FFU2xDLEdBRlQsRUFHR21CLElBSEgsQ0FHUSxHQUhSLEVBR2FyRSxFQUFFLENBQUNxRixJQUFILEdBQ1JmLENBRFEsQ0FDTmpCLENBQUMsSUFBSWlCLENBQUMsQ0FBQ2pCLENBQUMsQ0FBQ2QsS0FBSCxDQURBLEVBRVJ5QyxDQUZRLENBRU4zQixDQUFDLElBQUkyQixDQUFDLENBQUMzQixDQUFDLENBQUNDLFdBQUgsQ0FGQSxDQUhiO0FBT0FXLFNBQUcsQ0FBQ1MsTUFBSixDQUFXLE1BQVgsRUFDR0wsSUFESCxDQUNRLE9BRFIsRUFDaUIsbUNBRGpCLEVBRUdlLEtBRkgsQ0FFU3JCLENBRlQsRUFHR00sSUFISCxDQUdRLEdBSFIsRUFHYXJFLEVBQUUsQ0FBQ3FGLElBQUgsR0FDUmYsQ0FEUSxDQUNOakIsQ0FBQyxJQUFJaUIsQ0FBQyxDQUFDakIsQ0FBQyxDQUFDZCxLQUFILENBREEsRUFFUnlDLENBRlEsQ0FFTjNCLENBQUMsSUFBSTJCLENBQUMsQ0FBQzNCLENBQUMsQ0FBQ0MsV0FBSCxDQUZBLENBSGIsRUFqSE4sQ0F5SE07O0FBQ0EsVUFBSWlDLFFBQVEsR0FBRyxFQUFmO0FBQ0F2QyxTQUFHLENBQUNYLE9BQUosQ0FBWUMsQ0FBQyxJQUFJO0FBQ2IsY0FBTWtELEdBQUcsR0FBR3JGLE1BQU0sQ0FBQ21DLENBQUMsQ0FBQ0MsS0FBSCxDQUFOLENBQWdCd0MsTUFBaEIsQ0FBdUIsTUFBdkIsQ0FBWjs7QUFDQSxZQUFHLEVBQUVTLEdBQUcsSUFBSUQsUUFBVCxDQUFILEVBQXNCO0FBQ2xCQSxrQkFBUSxDQUFDQyxHQUFELENBQVIsR0FBZ0IsQ0FBRWxELENBQUMsQ0FBQ2dCLFdBQUosQ0FBaEI7QUFDSCxTQUZELE1BRU87QUFDSGlDLGtCQUFRLENBQUNDLEdBQUQsQ0FBUixDQUFjQyxJQUFkLENBQW9CbkQsQ0FBQyxDQUFDZ0IsV0FBdEI7QUFDSDtBQUNKLE9BUEQ7QUFRQSxVQUFJb0MsT0FBTyxHQUFHLEVBQWQ7O0FBQ0EsV0FBSUMsR0FBSixJQUFXSixRQUFYLEVBQW9CO0FBQ2hCRyxlQUFPLENBQUNELElBQVIsQ0FBY3pGLEVBQUUsQ0FBQzRDLEdBQUgsQ0FBTzJDLFFBQVEsQ0FBQ0ksR0FBRCxDQUFmLEVBQXNCckQsQ0FBQyxJQUFFQSxDQUF6QixDQUFkO0FBQ0g7O0FBQ0QsWUFBTXNELE9BQU8sR0FBRzVGLEVBQUUsQ0FBQ29ELElBQUgsQ0FBU3NDLE9BQVQsQ0FBaEI7QUFDQSxZQUFNRyxHQUFHLEdBQUc3RixFQUFFLENBQUNvRCxJQUFILENBQVFKLEdBQVIsRUFBYVYsQ0FBQyxJQUFJQSxDQUFDLENBQUNnQixXQUFwQixDQUFaO0FBQ0FFLFVBQUksR0FBR0MsSUFBSSxDQUFDQyxHQUFMLENBQVNtQyxHQUFHLEdBQUdELE9BQWYsRUFBd0JqQyxPQUF4QixDQUFnQyxDQUFoQyxDQUFQO0FBQ0FoRCxjQUFRLENBQUNHLGFBQVQsQ0FBdUIsZ0NBQXZCLEVBQXlEOEMsU0FBekQsR0FBc0UsSUFBR0osSUFBSyxHQUE5RSxDQTFJTixDQTRJTTs7QUFFQXhCLFdBQUssQ0FBQyw2Q0FBRCxDQUFMLENBQXFEQyxJQUFyRCxDQUEyREssQ0FBQyxJQUFJQSxDQUFDLENBQUNILElBQUYsRUFBaEUsRUFBMkVGLElBQTNFLENBQWlGRyxJQUFJLElBQUk7QUFDckYsWUFBSTBELEdBQUcsR0FBR0MsQ0FBQyxDQUFDRCxHQUFGLENBQU0sS0FBTixFQUFhO0FBQ25CRSxnQkFBTSxFQUFlLENBQUUsRUFBRixFQUFNLENBQU4sQ0FERjtBQUVuQkMsY0FBSSxFQUFpQixDQUZGO0FBR25CQyxpQkFBTyxFQUFjLEVBSEY7QUFJbkJDLDRCQUFrQixFQUFHLEtBSkY7QUFLbkJDLHFCQUFXLEVBQVUsS0FMRjtBQU1uQkMseUJBQWUsRUFBTTtBQU5GLFNBQWIsQ0FBVjtBQVFBTixTQUFDLENBQUNPLE9BQUYsQ0FBVWxFLElBQVYsRUFBZ0IsRUFBaEIsRUFBb0JtRSxLQUFwQixDQUEwQlQsR0FBMUI7QUFDQSxjQUFNVSxHQUFHLEdBQUcsQ0FDUjtBQUFFQyxXQUFDLEVBQUd6RyxFQUFFLENBQUM4QyxHQUFILENBQVFFLEdBQUcsQ0FBQ0MsTUFBSixDQUFZWCxDQUFDLElBQUlBLENBQUMsQ0FBQ0MsS0FBRixJQUFXdkMsRUFBRSxDQUFDOEMsR0FBSCxDQUFPRSxHQUFQLEVBQVlWLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxLQUFuQixDQUE1QixDQUFSLEVBQWdFRCxDQUFDLElBQUlBLENBQUMsQ0FBQ2dCLFdBQXZFLENBQU47QUFBMkZvRCxXQUFDLEVBQUU7QUFBOUYsU0FEUSxFQUVSO0FBQUVELFdBQUMsRUFBRyxFQUFOO0FBQVVDLFdBQUMsRUFBRTtBQUFiLFNBRlEsRUFHUjtBQUFFRCxXQUFDLEVBQUcsRUFBTjtBQUFVQyxXQUFDLEVBQUU7QUFBYixTQUhRLENBQVo7QUFLQTFFLGFBQUssQ0FBQyxvQ0FBRCxDQUFMLENBQTRDQyxJQUE1QyxDQUFrREssQ0FBQyxJQUFJQSxDQUFDLENBQUNILElBQUYsRUFBdkQsRUFBa0VGLElBQWxFLENBQXdFRyxJQUFJLElBQUk7QUFDNUUsZUFBSSxNQUFNLENBQUN1RSxDQUFELEVBQUlDLENBQUosQ0FBVixJQUFvQkMsTUFBTSxDQUFDQyxPQUFQLENBQWUxRSxJQUFmLENBQXBCLEVBQXlDO0FBQ3JDb0UsZUFBRyxDQUFDbkUsT0FBSixDQUFhMEUsR0FBRyxJQUFJO0FBQ2xCQyxxQkFBTyxDQUFDQyxHQUFSLENBQWFDLFFBQVEsQ0FBQ3BFLEdBQUcsQ0FBQ2lDLE1BQUosQ0FBVyxHQUFYLENBQUQsQ0FBckI7QUFDRSxrQkFBSW9DLEdBQUcsR0FBRzFELElBQUksQ0FBQ0MsR0FBTCxDQUFVMEQsVUFBVSxDQUFDUixDQUFDLENBQUMscUJBQUQsQ0FBRCxDQUEwQk0sUUFBUSxDQUFDcEUsR0FBRyxDQUFDaUMsTUFBSixDQUFXLEdBQVgsQ0FBRCxDQUFsQyxDQUFELENBQVYsR0FBb0VnQyxHQUFHLENBQUNOLENBQWxGLENBQVY7O0FBQ0Esa0JBQUdHLENBQUMsQ0FBQ1MsR0FBRixJQUFTVCxDQUFDLENBQUNVLEdBQVgsSUFBa0JILEdBQUcsSUFBSSxDQUE1QixFQUErQjtBQUM3QnBCLGlCQUFDLENBQUN3QixNQUFGLENBQVMsQ0FDUEgsVUFBVSxDQUFDUixDQUFDLENBQUNTLEdBQUgsQ0FESCxFQUVQRCxVQUFVLENBQUNSLENBQUMsQ0FBQ1UsR0FBSCxDQUZILENBQVQsRUFHRztBQUNERSxzQkFBSSxFQUFHekIsQ0FBQyxDQUFDMEIsT0FBRixDQUFVO0FBQ2ZDLDRCQUFRLEVBQUssQ0FBQyxFQUFELEVBQUssRUFBTCxDQURFO0FBRWZDLDhCQUFVLEVBQUcsQ0FBQyxFQUFELEVBQUssRUFBTCxDQUZFO0FBR2ZDLHdCQUFJLEVBQUcscUJBQW9CYixHQUFHLENBQUNMLENBQUU7QUFIbEIsbUJBQVY7QUFETixpQkFISCxFQVNHSCxLQVRILENBU1NULEdBVFQ7QUFVRDtBQUNKLGFBZkQ7QUFnQkg7O0FBQUE7QUFDSixTQW5CRDtBQW9CSCxPQW5DRDtBQW9DSCxLQW5MSDtBQW9MRCxHQTdMRDtBQThMSCxDQTFORCxFIiwiZmlsZSI6InRlbXBlcmF0dXJlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gaW5zdGFsbCBhIEpTT05QIGNhbGxiYWNrIGZvciBjaHVuayBsb2FkaW5nXG4gXHRmdW5jdGlvbiB3ZWJwYWNrSnNvbnBDYWxsYmFjayhkYXRhKSB7XG4gXHRcdHZhciBjaHVua0lkcyA9IGRhdGFbMF07XG4gXHRcdHZhciBtb3JlTW9kdWxlcyA9IGRhdGFbMV07XG4gXHRcdHZhciBleGVjdXRlTW9kdWxlcyA9IGRhdGFbMl07XG5cbiBcdFx0Ly8gYWRkIFwibW9yZU1vZHVsZXNcIiB0byB0aGUgbW9kdWxlcyBvYmplY3QsXG4gXHRcdC8vIHRoZW4gZmxhZyBhbGwgXCJjaHVua0lkc1wiIGFzIGxvYWRlZCBhbmQgZmlyZSBjYWxsYmFja1xuIFx0XHR2YXIgbW9kdWxlSWQsIGNodW5rSWQsIGkgPSAwLCByZXNvbHZlcyA9IFtdO1xuIFx0XHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoaW5zdGFsbGVkQ2h1bmtzLCBjaHVua0lkKSAmJiBpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcbiBcdFx0XHRcdHJlc29sdmVzLnB1c2goaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdWzBdKTtcbiBcdFx0XHR9XG4gXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gMDtcbiBcdFx0fVxuIFx0XHRmb3IobW9kdWxlSWQgaW4gbW9yZU1vZHVsZXMpIHtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobW9yZU1vZHVsZXMsIG1vZHVsZUlkKSkge1xuIFx0XHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0gPSBtb3JlTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdGlmKHBhcmVudEpzb25wRnVuY3Rpb24pIHBhcmVudEpzb25wRnVuY3Rpb24oZGF0YSk7XG5cbiBcdFx0d2hpbGUocmVzb2x2ZXMubGVuZ3RoKSB7XG4gXHRcdFx0cmVzb2x2ZXMuc2hpZnQoKSgpO1xuIFx0XHR9XG5cbiBcdFx0Ly8gYWRkIGVudHJ5IG1vZHVsZXMgZnJvbSBsb2FkZWQgY2h1bmsgdG8gZGVmZXJyZWQgbGlzdFxuIFx0XHRkZWZlcnJlZE1vZHVsZXMucHVzaC5hcHBseShkZWZlcnJlZE1vZHVsZXMsIGV4ZWN1dGVNb2R1bGVzIHx8IFtdKTtcblxuIFx0XHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIGFsbCBjaHVua3MgcmVhZHlcbiBcdFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4gXHR9O1xuIFx0ZnVuY3Rpb24gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKSB7XG4gXHRcdHZhciByZXN1bHQ7XG4gXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBkZWZlcnJlZE1vZHVsZXMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHR2YXIgZGVmZXJyZWRNb2R1bGUgPSBkZWZlcnJlZE1vZHVsZXNbaV07XG4gXHRcdFx0dmFyIGZ1bGZpbGxlZCA9IHRydWU7XG4gXHRcdFx0Zm9yKHZhciBqID0gMTsgaiA8IGRlZmVycmVkTW9kdWxlLmxlbmd0aDsgaisrKSB7XG4gXHRcdFx0XHR2YXIgZGVwSWQgPSBkZWZlcnJlZE1vZHVsZVtqXTtcbiBcdFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tkZXBJZF0gIT09IDApIGZ1bGZpbGxlZCA9IGZhbHNlO1xuIFx0XHRcdH1cbiBcdFx0XHRpZihmdWxmaWxsZWQpIHtcbiBcdFx0XHRcdGRlZmVycmVkTW9kdWxlcy5zcGxpY2UoaS0tLCAxKTtcbiBcdFx0XHRcdHJlc3VsdCA9IF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gZGVmZXJyZWRNb2R1bGVbMF0pO1xuIFx0XHRcdH1cbiBcdFx0fVxuXG4gXHRcdHJldHVybiByZXN1bHQ7XG4gXHR9XG5cbiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIG9iamVjdCB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4gXHQvLyB1bmRlZmluZWQgPSBjaHVuayBub3QgbG9hZGVkLCBudWxsID0gY2h1bmsgcHJlbG9hZGVkL3ByZWZldGNoZWRcbiBcdC8vIFByb21pc2UgPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG4gXHR2YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuIFx0XHRcInRlbXBlcmF0dXJlXCI6IDBcbiBcdH07XG5cbiBcdHZhciBkZWZlcnJlZE1vZHVsZXMgPSBbXTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCIvXCI7XG5cbiBcdHZhciBqc29ucEFycmF5ID0gd2luZG93W1wid2VicGFja0pzb25wXCJdID0gd2luZG93W1wid2VicGFja0pzb25wXCJdIHx8IFtdO1xuIFx0dmFyIG9sZEpzb25wRnVuY3Rpb24gPSBqc29ucEFycmF5LnB1c2guYmluZChqc29ucEFycmF5KTtcbiBcdGpzb25wQXJyYXkucHVzaCA9IHdlYnBhY2tKc29ucENhbGxiYWNrO1xuIFx0anNvbnBBcnJheSA9IGpzb25wQXJyYXkuc2xpY2UoKTtcbiBcdGZvcih2YXIgaSA9IDA7IGkgPCBqc29ucEFycmF5Lmxlbmd0aDsgaSsrKSB3ZWJwYWNrSnNvbnBDYWxsYmFjayhqc29ucEFycmF5W2ldKTtcbiBcdHZhciBwYXJlbnRKc29ucEZ1bmN0aW9uID0gb2xkSnNvbnBGdW5jdGlvbjtcblxuXG4gXHQvLyBhZGQgZW50cnkgbW9kdWxlIHRvIGRlZmVycmVkIGxpc3RcbiBcdGRlZmVycmVkTW9kdWxlcy5wdXNoKFtcIi4vc3JjL2luZGV4LXRlbXBlcmF0dXJlLmpzXCIsXCJ2ZW5kb3JzfnRlbXBlcmF0dXJlXCJdKTtcbiBcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gcmVhZHlcbiBcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIiwidmFyIG1hcCA9IHtcblx0XCIuL2FmXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hZi5qc1wiLFxuXHRcIi4vYWYuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FmLmpzXCIsXG5cdFwiLi9hclwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXIuanNcIixcblx0XCIuL2FyLWR6XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci1kei5qc1wiLFxuXHRcIi4vYXItZHouanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLWR6LmpzXCIsXG5cdFwiLi9hci1rd1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXIta3cuanNcIixcblx0XCIuL2FyLWt3LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci1rdy5qc1wiLFxuXHRcIi4vYXItbHlcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLWx5LmpzXCIsXG5cdFwiLi9hci1seS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXItbHkuanNcIixcblx0XCIuL2FyLW1hXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci1tYS5qc1wiLFxuXHRcIi4vYXItbWEuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLW1hLmpzXCIsXG5cdFwiLi9hci1zYVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXItc2EuanNcIixcblx0XCIuL2FyLXNhLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci1zYS5qc1wiLFxuXHRcIi4vYXItdG5cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLXRuLmpzXCIsXG5cdFwiLi9hci10bi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXItdG4uanNcIixcblx0XCIuL2FyLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci5qc1wiLFxuXHRcIi4vYXpcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2F6LmpzXCIsXG5cdFwiLi9hei5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXouanNcIixcblx0XCIuL2JlXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9iZS5qc1wiLFxuXHRcIi4vYmUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JlLmpzXCIsXG5cdFwiLi9iZ1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYmcuanNcIixcblx0XCIuL2JnLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9iZy5qc1wiLFxuXHRcIi4vYm1cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JtLmpzXCIsXG5cdFwiLi9ibS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYm0uanNcIixcblx0XCIuL2JuXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ibi5qc1wiLFxuXHRcIi4vYm4tYmRcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JuLWJkLmpzXCIsXG5cdFwiLi9ibi1iZC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYm4tYmQuanNcIixcblx0XCIuL2JuLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ibi5qc1wiLFxuXHRcIi4vYm9cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JvLmpzXCIsXG5cdFwiLi9iby5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYm8uanNcIixcblx0XCIuL2JyXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ici5qc1wiLFxuXHRcIi4vYnIuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JyLmpzXCIsXG5cdFwiLi9ic1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYnMuanNcIixcblx0XCIuL2JzLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9icy5qc1wiLFxuXHRcIi4vY2FcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2NhLmpzXCIsXG5cdFwiLi9jYS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvY2EuanNcIixcblx0XCIuL2NzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9jcy5qc1wiLFxuXHRcIi4vY3MuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2NzLmpzXCIsXG5cdFwiLi9jdlwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvY3YuanNcIixcblx0XCIuL2N2LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9jdi5qc1wiLFxuXHRcIi4vY3lcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2N5LmpzXCIsXG5cdFwiLi9jeS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvY3kuanNcIixcblx0XCIuL2RhXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9kYS5qc1wiLFxuXHRcIi4vZGEuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2RhLmpzXCIsXG5cdFwiLi9kZVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZGUuanNcIixcblx0XCIuL2RlLWF0XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9kZS1hdC5qc1wiLFxuXHRcIi4vZGUtYXQuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2RlLWF0LmpzXCIsXG5cdFwiLi9kZS1jaFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZGUtY2guanNcIixcblx0XCIuL2RlLWNoLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9kZS1jaC5qc1wiLFxuXHRcIi4vZGUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2RlLmpzXCIsXG5cdFwiLi9kdlwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZHYuanNcIixcblx0XCIuL2R2LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9kdi5qc1wiLFxuXHRcIi4vZWxcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VsLmpzXCIsXG5cdFwiLi9lbC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZWwuanNcIixcblx0XCIuL2VuLWF1XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1hdS5qc1wiLFxuXHRcIi4vZW4tYXUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWF1LmpzXCIsXG5cdFwiLi9lbi1jYVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4tY2EuanNcIixcblx0XCIuL2VuLWNhLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1jYS5qc1wiLFxuXHRcIi4vZW4tZ2JcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWdiLmpzXCIsXG5cdFwiLi9lbi1nYi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4tZ2IuanNcIixcblx0XCIuL2VuLWllXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1pZS5qc1wiLFxuXHRcIi4vZW4taWUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWllLmpzXCIsXG5cdFwiLi9lbi1pbFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4taWwuanNcIixcblx0XCIuL2VuLWlsLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1pbC5qc1wiLFxuXHRcIi4vZW4taW5cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWluLmpzXCIsXG5cdFwiLi9lbi1pbi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4taW4uanNcIixcblx0XCIuL2VuLW56XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1uei5qc1wiLFxuXHRcIi4vZW4tbnouanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLW56LmpzXCIsXG5cdFwiLi9lbi1zZ1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4tc2cuanNcIixcblx0XCIuL2VuLXNnLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1zZy5qc1wiLFxuXHRcIi4vZW9cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VvLmpzXCIsXG5cdFwiLi9lby5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW8uanNcIixcblx0XCIuL2VzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lcy5qc1wiLFxuXHRcIi4vZXMtZG9cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VzLWRvLmpzXCIsXG5cdFwiLi9lcy1kby5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXMtZG8uanNcIixcblx0XCIuL2VzLW14XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lcy1teC5qc1wiLFxuXHRcIi4vZXMtbXguanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VzLW14LmpzXCIsXG5cdFwiLi9lcy11c1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXMtdXMuanNcIixcblx0XCIuL2VzLXVzLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lcy11cy5qc1wiLFxuXHRcIi4vZXMuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VzLmpzXCIsXG5cdFwiLi9ldFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXQuanNcIixcblx0XCIuL2V0LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ldC5qc1wiLFxuXHRcIi4vZXVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2V1LmpzXCIsXG5cdFwiLi9ldS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXUuanNcIixcblx0XCIuL2ZhXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9mYS5qc1wiLFxuXHRcIi4vZmEuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZhLmpzXCIsXG5cdFwiLi9maVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZmkuanNcIixcblx0XCIuL2ZpLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9maS5qc1wiLFxuXHRcIi4vZmlsXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9maWwuanNcIixcblx0XCIuL2ZpbC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZmlsLmpzXCIsXG5cdFwiLi9mb1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZm8uanNcIixcblx0XCIuL2ZvLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9mby5qc1wiLFxuXHRcIi4vZnJcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZyLmpzXCIsXG5cdFwiLi9mci1jYVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZnItY2EuanNcIixcblx0XCIuL2ZyLWNhLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9mci1jYS5qc1wiLFxuXHRcIi4vZnItY2hcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZyLWNoLmpzXCIsXG5cdFwiLi9mci1jaC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZnItY2guanNcIixcblx0XCIuL2ZyLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9mci5qc1wiLFxuXHRcIi4vZnlcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2Z5LmpzXCIsXG5cdFwiLi9meS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZnkuanNcIixcblx0XCIuL2dhXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9nYS5qc1wiLFxuXHRcIi4vZ2EuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2dhLmpzXCIsXG5cdFwiLi9nZFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ2QuanNcIixcblx0XCIuL2dkLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9nZC5qc1wiLFxuXHRcIi4vZ2xcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2dsLmpzXCIsXG5cdFwiLi9nbC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ2wuanNcIixcblx0XCIuL2dvbS1kZXZhXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9nb20tZGV2YS5qc1wiLFxuXHRcIi4vZ29tLWRldmEuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2dvbS1kZXZhLmpzXCIsXG5cdFwiLi9nb20tbGF0blwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ29tLWxhdG4uanNcIixcblx0XCIuL2dvbS1sYXRuLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9nb20tbGF0bi5qc1wiLFxuXHRcIi4vZ3VcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2d1LmpzXCIsXG5cdFwiLi9ndS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ3UuanNcIixcblx0XCIuL2hlXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9oZS5qc1wiLFxuXHRcIi4vaGUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2hlLmpzXCIsXG5cdFwiLi9oaVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaGkuanNcIixcblx0XCIuL2hpLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9oaS5qc1wiLFxuXHRcIi4vaHJcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2hyLmpzXCIsXG5cdFwiLi9oci5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaHIuanNcIixcblx0XCIuL2h1XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9odS5qc1wiLFxuXHRcIi4vaHUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2h1LmpzXCIsXG5cdFwiLi9oeS1hbVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaHktYW0uanNcIixcblx0XCIuL2h5LWFtLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9oeS1hbS5qc1wiLFxuXHRcIi4vaWRcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2lkLmpzXCIsXG5cdFwiLi9pZC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaWQuanNcIixcblx0XCIuL2lzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9pcy5qc1wiLFxuXHRcIi4vaXMuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2lzLmpzXCIsXG5cdFwiLi9pdFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaXQuanNcIixcblx0XCIuL2l0LWNoXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9pdC1jaC5qc1wiLFxuXHRcIi4vaXQtY2guanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2l0LWNoLmpzXCIsXG5cdFwiLi9pdC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaXQuanNcIixcblx0XCIuL2phXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9qYS5qc1wiLFxuXHRcIi4vamEuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2phLmpzXCIsXG5cdFwiLi9qdlwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvanYuanNcIixcblx0XCIuL2p2LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9qdi5qc1wiLFxuXHRcIi4va2FcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2thLmpzXCIsXG5cdFwiLi9rYS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva2EuanNcIixcblx0XCIuL2trXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ray5qc1wiLFxuXHRcIi4va2suanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2trLmpzXCIsXG5cdFwiLi9rbVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva20uanNcIixcblx0XCIuL2ttLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9rbS5qc1wiLFxuXHRcIi4va25cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2tuLmpzXCIsXG5cdFwiLi9rbi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva24uanNcIixcblx0XCIuL2tvXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9rby5qc1wiLFxuXHRcIi4va28uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2tvLmpzXCIsXG5cdFwiLi9rdVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva3UuanNcIixcblx0XCIuL2t1LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9rdS5qc1wiLFxuXHRcIi4va3lcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2t5LmpzXCIsXG5cdFwiLi9reS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva3kuanNcIixcblx0XCIuL2xiXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9sYi5qc1wiLFxuXHRcIi4vbGIuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2xiLmpzXCIsXG5cdFwiLi9sb1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbG8uanNcIixcblx0XCIuL2xvLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9sby5qc1wiLFxuXHRcIi4vbHRcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2x0LmpzXCIsXG5cdFwiLi9sdC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbHQuanNcIixcblx0XCIuL2x2XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9sdi5qc1wiLFxuXHRcIi4vbHYuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2x2LmpzXCIsXG5cdFwiLi9tZVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbWUuanNcIixcblx0XCIuL21lLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tZS5qc1wiLFxuXHRcIi4vbWlcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21pLmpzXCIsXG5cdFwiLi9taS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbWkuanNcIixcblx0XCIuL21rXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tay5qc1wiLFxuXHRcIi4vbWsuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21rLmpzXCIsXG5cdFwiLi9tbFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbWwuanNcIixcblx0XCIuL21sLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tbC5qc1wiLFxuXHRcIi4vbW5cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21uLmpzXCIsXG5cdFwiLi9tbi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbW4uanNcIixcblx0XCIuL21yXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tci5qc1wiLFxuXHRcIi4vbXIuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21yLmpzXCIsXG5cdFwiLi9tc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbXMuanNcIixcblx0XCIuL21zLW15XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tcy1teS5qc1wiLFxuXHRcIi4vbXMtbXkuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21zLW15LmpzXCIsXG5cdFwiLi9tcy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbXMuanNcIixcblx0XCIuL210XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tdC5qc1wiLFxuXHRcIi4vbXQuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL210LmpzXCIsXG5cdFwiLi9teVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbXkuanNcIixcblx0XCIuL215LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9teS5qc1wiLFxuXHRcIi4vbmJcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL25iLmpzXCIsXG5cdFwiLi9uYi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbmIuanNcIixcblx0XCIuL25lXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9uZS5qc1wiLFxuXHRcIi4vbmUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL25lLmpzXCIsXG5cdFwiLi9ubFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbmwuanNcIixcblx0XCIuL25sLWJlXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ubC1iZS5qc1wiLFxuXHRcIi4vbmwtYmUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL25sLWJlLmpzXCIsXG5cdFwiLi9ubC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbmwuanNcIixcblx0XCIuL25uXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ubi5qc1wiLFxuXHRcIi4vbm4uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL25uLmpzXCIsXG5cdFwiLi9vYy1sbmNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL29jLWxuYy5qc1wiLFxuXHRcIi4vb2MtbG5jLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9vYy1sbmMuanNcIixcblx0XCIuL3BhLWluXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9wYS1pbi5qc1wiLFxuXHRcIi4vcGEtaW4uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3BhLWluLmpzXCIsXG5cdFwiLi9wbFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcGwuanNcIixcblx0XCIuL3BsLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9wbC5qc1wiLFxuXHRcIi4vcHRcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3B0LmpzXCIsXG5cdFwiLi9wdC1iclwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcHQtYnIuanNcIixcblx0XCIuL3B0LWJyLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9wdC1ici5qc1wiLFxuXHRcIi4vcHQuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3B0LmpzXCIsXG5cdFwiLi9yb1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcm8uanNcIixcblx0XCIuL3JvLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9yby5qc1wiLFxuXHRcIi4vcnVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3J1LmpzXCIsXG5cdFwiLi9ydS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcnUuanNcIixcblx0XCIuL3NkXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zZC5qc1wiLFxuXHRcIi4vc2QuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NkLmpzXCIsXG5cdFwiLi9zZVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc2UuanNcIixcblx0XCIuL3NlLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zZS5qc1wiLFxuXHRcIi4vc2lcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NpLmpzXCIsXG5cdFwiLi9zaS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc2kuanNcIixcblx0XCIuL3NrXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zay5qc1wiLFxuXHRcIi4vc2suanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NrLmpzXCIsXG5cdFwiLi9zbFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc2wuanNcIixcblx0XCIuL3NsLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zbC5qc1wiLFxuXHRcIi4vc3FcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NxLmpzXCIsXG5cdFwiLi9zcS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3EuanNcIixcblx0XCIuL3NyXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zci5qc1wiLFxuXHRcIi4vc3ItY3lybFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3ItY3lybC5qc1wiLFxuXHRcIi4vc3ItY3lybC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3ItY3lybC5qc1wiLFxuXHRcIi4vc3IuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NyLmpzXCIsXG5cdFwiLi9zc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3MuanNcIixcblx0XCIuL3NzLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zcy5qc1wiLFxuXHRcIi4vc3ZcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3N2LmpzXCIsXG5cdFwiLi9zdi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3YuanNcIixcblx0XCIuL3N3XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zdy5qc1wiLFxuXHRcIi4vc3cuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3N3LmpzXCIsXG5cdFwiLi90YVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGEuanNcIixcblx0XCIuL3RhLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90YS5qc1wiLFxuXHRcIi4vdGVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RlLmpzXCIsXG5cdFwiLi90ZS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGUuanNcIixcblx0XCIuL3RldFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGV0LmpzXCIsXG5cdFwiLi90ZXQuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RldC5qc1wiLFxuXHRcIi4vdGdcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RnLmpzXCIsXG5cdFwiLi90Zy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGcuanNcIixcblx0XCIuL3RoXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90aC5qc1wiLFxuXHRcIi4vdGguanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RoLmpzXCIsXG5cdFwiLi90a1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGsuanNcIixcblx0XCIuL3RrLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90ay5qc1wiLFxuXHRcIi4vdGwtcGhcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RsLXBoLmpzXCIsXG5cdFwiLi90bC1waC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGwtcGguanNcIixcblx0XCIuL3RsaFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGxoLmpzXCIsXG5cdFwiLi90bGguanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RsaC5qc1wiLFxuXHRcIi4vdHJcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RyLmpzXCIsXG5cdFwiLi90ci5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdHIuanNcIixcblx0XCIuL3R6bFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdHpsLmpzXCIsXG5cdFwiLi90emwuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3R6bC5qc1wiLFxuXHRcIi4vdHptXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90em0uanNcIixcblx0XCIuL3R6bS1sYXRuXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90em0tbGF0bi5qc1wiLFxuXHRcIi4vdHptLWxhdG4uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3R6bS1sYXRuLmpzXCIsXG5cdFwiLi90em0uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3R6bS5qc1wiLFxuXHRcIi4vdWctY25cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3VnLWNuLmpzXCIsXG5cdFwiLi91Zy1jbi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdWctY24uanNcIixcblx0XCIuL3VrXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS91ay5qc1wiLFxuXHRcIi4vdWsuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3VrLmpzXCIsXG5cdFwiLi91clwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdXIuanNcIixcblx0XCIuL3VyLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS91ci5qc1wiLFxuXHRcIi4vdXpcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3V6LmpzXCIsXG5cdFwiLi91ei1sYXRuXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS91ei1sYXRuLmpzXCIsXG5cdFwiLi91ei1sYXRuLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS91ei1sYXRuLmpzXCIsXG5cdFwiLi91ei5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdXouanNcIixcblx0XCIuL3ZpXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS92aS5qc1wiLFxuXHRcIi4vdmkuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3ZpLmpzXCIsXG5cdFwiLi94LXBzZXVkb1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUveC1wc2V1ZG8uanNcIixcblx0XCIuL3gtcHNldWRvLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS94LXBzZXVkby5qc1wiLFxuXHRcIi4veW9cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3lvLmpzXCIsXG5cdFwiLi95by5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUveW8uanNcIixcblx0XCIuL3poLWNuXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS96aC1jbi5qc1wiLFxuXHRcIi4vemgtY24uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3poLWNuLmpzXCIsXG5cdFwiLi96aC1oa1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvemgtaGsuanNcIixcblx0XCIuL3poLWhrLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS96aC1oay5qc1wiLFxuXHRcIi4vemgtbW9cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3poLW1vLmpzXCIsXG5cdFwiLi96aC1tby5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvemgtbW8uanNcIixcblx0XCIuL3poLXR3XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS96aC10dy5qc1wiLFxuXHRcIi4vemgtdHcuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3poLXR3LmpzXCJcbn07XG5cblxuZnVuY3Rpb24gd2VicGFja0NvbnRleHQocmVxKSB7XG5cdHZhciBpZCA9IHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpO1xuXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhpZCk7XG59XG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKSB7XG5cdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8obWFwLCByZXEpKSB7XG5cdFx0dmFyIGUgPSBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiICsgcmVxICsgXCInXCIpO1xuXHRcdGUuY29kZSA9ICdNT0RVTEVfTk9UX0ZPVU5EJztcblx0XHR0aHJvdyBlO1xuXHR9XG5cdHJldHVybiBtYXBbcmVxXTtcbn1cbndlYnBhY2tDb250ZXh0LmtleXMgPSBmdW5jdGlvbiB3ZWJwYWNrQ29udGV4dEtleXMoKSB7XG5cdHJldHVybiBPYmplY3Qua2V5cyhtYXApO1xufTtcbndlYnBhY2tDb250ZXh0LnJlc29sdmUgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmU7XG5tb2R1bGUuZXhwb3J0cyA9IHdlYnBhY2tDb250ZXh0O1xud2VicGFja0NvbnRleHQuaWQgPSBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUgc3luYyByZWN1cnNpdmUgXlxcXFwuXFxcXC8uKiRcIjsiLCIvLyBEZXBlbmRlbmNpZXNcblxuY29uc3QgZDMgPSByZXF1aXJlKCdkMycpO1xuY29uc3QgbGVhZmxldCA9IHJlcXVpcmUoJ2xlYWZsZXQnKTtcbmNvbnN0IG1vbWVudCA9IHJlcXVpcmUoJ21vbWVudCcpO1xuXG4vLyBkZWZhdWx0IG5hbWVzcGFjZSBmb3IgU1ZHc1xuY29uc3QgTlMgPSBcImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCI7XG5jb25zdCB0b2RheSAgPSBuZXcgRGF0ZSgpO1xubGV0IG1vbnRoYWdvID0gbmV3IERhdGUoKTsgXG5tb250aGFnby5zZXRNb250aCggbW9udGhhZ28uZ2V0TW9udGgoKSAtIDEgKTtcblxucGxheWluZyA9IGZhbHNlO1xuXG4vLyBXaGVuIERPTSBpcyByZWFkeVxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZ1bmN0aW9uKClcbntcbiAgXG4gICAgLy8gcGxheWVyIGJ1dHRvbnNcbiAgICB2YXIgcHJlc2VudGF0aW9uID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnByZXNlbnRhdGlvbicpO1xuICAgIGNvbnN0IHNsaWRlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5wcmVzZW50YXRpb25fX3BhZ2UnKTtcbiAgICB2YXIgcGxheWVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnBsYXllcicpO1xuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLWFjdGlvbj1cInBsYXlcIl0nKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCl7XG4gICAgICAgIHBsYXllci5kYXRhc2V0Lm9uID0gXCJwbGF5XCI7XG4gICAgICAgIHBsYXlpbmcgPSB0cnVlO1xuICAgIH0pO1xuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLWFjdGlvbj1cInBhdXNlXCJdJykuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbigpe1xuICAgICAgICBwbGF5ZXIuZGF0YXNldC5vbiA9IFwicGF1c2VcIjtcbiAgICAgICAgcGxheWluZyA9IGZhbHNlO1xuICAgIH0pO1xuICAgIHNldEludGVydmFsKCAoKSA9PiB7IFxuICAgICAgICBpZihwbGF5aW5nKSB7XG4gICAgICAgICAgICBwcmVzZW50YXRpb24uZGF0YXNldC5jdXJyZW50ID0gKCArIHByZXNlbnRhdGlvbi5kYXRhc2V0LmN1cnJlbnQgKyAxICkgJSBzbGlkZXMubGVuZ3RoO1xuICAgICAgICAgICAgdmFyIG5leHQgPSBzbGlkZXMuaXRlbSggK3ByZXNlbnRhdGlvbi5kYXRhc2V0LmN1cnJlbnQgKTtcbiAgICAgICAgICAgIHdpbmRvdy5zY3JvbGxUbyggXG4gICAgICAgICAgICAgICAgMCwgXG4gICAgICAgICAgICAgICAgbmV4dC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS50b3AgKyB3aW5kb3cuc2Nyb2xsWVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSBcbiAgICB9LCA4MDAwKVxuICBcbiAgICBsZXQgZmxvd2RhdGEsIHNlbnNvcmRhdGE7XG5cbiAgICBmZXRjaCgnL2FwaS9zZW5zb3InKS50aGVuKHJlc3BvbnNlID0+IHJlc3BvbnNlLmpzb24oKSkudGhlbihkYXRhID0+IHtcbiAgICAgIC8vIEZvcm1hdCBkYXRhIHByb3Blcmx5XG4gICAgICBkYXRhLmZvckVhY2goaSA9PiB7XG4gICAgICAgIGkuZmRhdGUgPSBkMy50aW1lUGFyc2UoXCIlbS8lZC8lWSAlSDolTTolU1wiKShpLmZkYXRlKTtcbiAgICAgIH0pO1xuICAgICAgc2Vuc29yZGF0YSA9IGQzLmdyb3VwKGRhdGEsIGl0ZW0gPT4gaXRlbS5uYW1lKTtcbiAgICAgIHZhciBtaW5fZGF0ZSA9IGQzLm1pbihkYXRhLCBpdGVtID0+IGl0ZW0uZmRhdGUpO1xuXG4gICAgICAvLyBHcm91cCBkYXRhXG4gICAgICBmZXRjaCgnL2FwaS93ZWF0aGVyJykudGhlbihyZXNwb25zZSA9PiByZXNwb25zZS5qc29uKCkpLnRoZW4oYWVyb3BvcnRfZGF0YSA9PlxuICAgICAge1xuICAgICAgICAgIGFlcm9wb3J0X2RhdGEuZm9yRWFjaChpID0+IHtcbiAgICAgICAgICAgIGkuZmRhdGUgPSBkMy50aW1lUGFyc2UoXCIlbS8lZC8lWSAlSDolTTolU1wiKShpLmZkYXRlKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBcbiAgICAgICAgICBsZXQgbWluID0gZDMubWF4KCBzZW5zb3JkYXRhLmdldCgnYXUyJyksIGkgPT4gaS5mZGF0ZSApO1xuICAgICAgICAgIG1pbi5zZXRNb250aCggbWluLmdldE1vbnRoKCkgLSAxICk7XG4gICAgICAgICAgbGV0IG1heCA9IGQzLm1heCggc2Vuc29yZGF0YS5nZXQoJ2F1MicpLCBpID0+IGkuZmRhdGUgKTtcblxuICAgICAgICAgIGxldCBhdTIgPSBzZW5zb3JkYXRhLmdldCgnYXUyJykuZmlsdGVyKCBpID0+IGkuZmRhdGUgPiBtaW4pO1xuICAgICAgICAgIGxldCBhdTQgPSBzZW5zb3JkYXRhLmdldCgnYXU0JykuZmlsdGVyKCBpID0+IGkuZmRhdGUgPiBtaW4pO1xuXG4gICAgICAgICAgLy8gR2V0IGF2ZXJhZ2UgZGlmZmVyZW5jZSBiZXR3ZWVuIGF1MiBhbmQgYXU0XG4gICAgICAgICAgY29uc3QgYXUyX2F2ZyA9IGQzLm1lYW4oYXUyLCBkID0+IGQudGVtcGVyYXR1cmUpO1xuICAgICAgICAgIGNvbnN0IGF1NF9hdmcgPSBkMy5tZWFuKGF1NCwgZCA9PiBkLnRlbXBlcmF0dXJlKTtcbiAgICAgICAgICBsZXQgZGlmZiA9IE1hdGguYWJzKGF1Ml9hdmcgLSBhdTRfYXZnKS50b0ZpeGVkKDIpO1xuICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNhdTJfYXU0IC5jaGFydF9fY2hhcnQtLW51bWJlcicpLmlubmVySFRNTCA9IGArJHtkaWZmfcKwYDtcblxuICAgICAgICAgIC8vIEdldCBhdmVyYWdlIGRpZmZlcmVuY2UgYmV0d2VlbiBlMCBhbmQgYWVyb3BvcnRcbiAgICAgICAgICBjb25zdCBlMCA9IHNlbnNvcmRhdGEuZ2V0KCdlMCcpLmZpbHRlciggaSA9PiBpLmZkYXRlID4gbWluKTtcbiAgICAgICAgICBjb25zdCBlMF9hdmcgPSBkMy5tZWFuKGUwLCBkID0+IGQudGVtcGVyYXR1cmUpO1xuICAgICAgICAgIGxldCBhICA9IGFlcm9wb3J0X2RhdGEuZmlsdGVyKCBpID0+IGkuZmRhdGUgPiBtaW4gJiYgaS5mZGF0ZSA8IG1heCk7XG4gICAgICAgICAgY29uc3QgYWVfYXZnID0gZDMubWVhbihhLCBkID0+IGQudGVtcGVyYXR1cmUpO1xuICAgICAgICAgIGRpZmYgPSBNYXRoLmFicyhlMF9hdmcgLSBhZV9hdmcpLnRvRml4ZWQoMik7XG4gICAgICAgICAgLy8gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2UwX2FlIC5jaGFydF9fY2hhcnQtLW51bWJlcicpLmlubmVySFRNTCA9IGAke2RpZmZ9wrBgO1xuXG4gICAgICAgICAgLy8gQWRkIGNoYXJ0IHdyYXBwZXJcbiAgICAgICAgICBsZXQgc3ZnID0gZDMuc2VsZWN0KCcjY2hhcnQtYXZlcmFnZS0tbW9udGgnKTtcbiAgICAgICAgICBjb25zdCB3aWR0aCA9IDgwMDtcbiAgICAgICAgICBjb25zdCBoZWlnaHQgPSA2MDA7XG4gICAgICAgICAgc3ZnLmF0dHIoJ3dpZHRoJywgd2lkdGgpLmF0dHIoJ2hlaWdodCcsIGhlaWdodCk7XG5cbiAgICAgICAgICAvLyBDcmVhdGUgY2hhcnQgc2NhbGVzIGFuZCBhZGQgdGhlbSB0byBjaGFydFxuICAgICAgICAgIGxldCB4ID0gZDMuc2NhbGVUaW1lKCkuZG9tYWluKFtcbiAgICAgICAgICAgICAgbWluLFxuICAgICAgICAgICAgICBtYXhcbiAgICAgICAgICBdKS5yYW5nZShbMCwgd2lkdGhdKTtcbiAgICAgICAgICBzdmcuYXBwZW5kKFwiZ1wiKVxuICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X194JylcbiAgICAgICAgICAgIC5hdHRyKFwidHJhbnNmb3JtXCIsIGB0cmFuc2xhdGUoMCwgJHtoZWlnaHR9KWApXG4gICAgICAgICAgICAuY2FsbChkMy5heGlzQm90dG9tKHgpLnRpY2tzKDE1KS50aWNrRm9ybWF0KGQgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBtb21lbnQoZCkuZm9ybWF0KCdERC1NTScpO1xuICAgICAgICAgICAgfSkpO1xuICAgICAgICAgIGNvbnN0IHkgPSBkMy5zY2FsZUxpbmVhcigpXG4gICAgICAgICAgICAuZG9tYWluKFswLCA1MF0pLnJhbmdlKFtoZWlnaHQsIDBdKTtcbiAgICAgICAgICBzdmcuYXBwZW5kKFwiZ1wiKVxuICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X195JylcbiAgICAgICAgICAgIC5hdHRyKFwidHJhbnNmb3JtXCIsIGB0cmFuc2xhdGUoMCwgMClgKVxuICAgICAgICAgICAgLmNhbGwoZDMuYXhpc0xlZnQoeSkudGlja0Zvcm1hdChkID0+IGQgKyBcIsKwIFwiKSlcbiAgICAgICAgICAgIC5zZWxlY3RBbGwoJ3RleHQnKVxuICAgICAgICAgICAgLmF0dHIoJ3gnLCAnLTIwcHgnKTtcbiAgICAgICAgICBzdmcuYXBwZW5kKCdwYXRoJylcbiAgICAgICAgICAgIC5hdHRyKCdjbGFzcycsICdjaGFydF9fbGluZSBjaGFydF9fbGluZS0tYXU0JylcbiAgICAgICAgICAgIC5kYXR1bShhdTIpXG4gICAgICAgICAgICAuYXR0cignZCcsIGQzLmxpbmUoKVxuICAgICAgICAgICAgICAueChkID0+IHgoZC5mZGF0ZSkpXG4gICAgICAgICAgICAgIC55KGQgPT4geShkLnRlbXBlcmF0dXJlKSlcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgc3ZnLmFwcGVuZCgncGF0aCcpXG4gICAgICAgICAgICAuYXR0cignY2xhc3MnLCAnY2hhcnRfX2xpbmUgY2hhcnRfX2xpbmUtLWF1MicpXG4gICAgICAgICAgICAuZGF0dW0oYXU0KVxuICAgICAgICAgICAgLmF0dHIoJ2QnLCBkMy5saW5lKClcbiAgICAgICAgICAgICAgLngoZCA9PiB4KGQuZmRhdGUpKVxuICAgICAgICAgICAgICAueShkID0+IHkoZC50ZW1wZXJhdHVyZSkpXG4gICAgICAgICAgICApO1xuICAgICAgICAgIHN2Zy5hcHBlbmQoJ3BhdGgnKVxuICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X19saW5lIGNoYXJ0X19saW5lLS1hZXJvcG9ydCcpXG4gICAgICAgICAgICAuZGF0dW0oYSlcbiAgICAgICAgICAgIC5hdHRyKCdkJywgZDMubGluZSgpXG4gICAgICAgICAgICAgIC54KGQgPT4geChkLmZkYXRlKSlcbiAgICAgICAgICAgICAgLnkoZCA9PiB5KGQudGVtcGVyYXR1cmUpKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgLy8gQWRkIGNoYXJ0IHdyYXBwZXJcbiAgICAgICAgICAgIHN2ZyA9IGQzLnNlbGVjdCgnI2NoYXJ0LWF2ZXJhZ2UtLXdlZWsnKTtcbiAgICAgICAgICAgIHN2Zy5hdHRyKCd3aWR0aCcsIHdpZHRoKS5hdHRyKCdoZWlnaHQnLCBoZWlnaHQpO1xuXG4gICAgICAgICAgICAvLyBDcmVhdGUgY2hhcnQgc2NhbGVzIGFuZCBhZGQgdGhlbSB0byBjaGFydFxuICAgICAgICAgICAgbWF4ID0gbW9tZW50KCcyMDIxMDgwMicsICdZWVlZTU1ERCcpO1xuICAgICAgICAgICAgbWluID0gbW9tZW50KG1heCkuc3VidHJhY3QoNiwgJ2RheXMnKTtcbiAgICAgICAgICAgIHggPSBkMy5zY2FsZVRpbWUoKS5kb21haW4oW1xuICAgICAgICAgICAgICAgIG1pbixcbiAgICAgICAgICAgICAgICBtYXhcbiAgICAgICAgICAgIF0pLnJhbmdlKFswLCB3aWR0aF0pO1xuICAgICAgICAgICAgc3ZnLmFwcGVuZChcImdcIilcbiAgICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X194JylcbiAgICAgICAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgYHRyYW5zbGF0ZSgwLCAke2hlaWdodH0pYClcbiAgICAgICAgICAgICAgLmNhbGwoZDMuYXhpc0JvdHRvbSh4KS50aWNrcyg3KS50aWNrRm9ybWF0KGQgPT4ge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIG1vbWVudChkKS5mb3JtYXQoJ0RELU1NJyk7XG4gICAgICAgICAgICAgIH0pKTtcbiAgICAgICAgICAgIHN2Zy5hcHBlbmQoXCJnXCIpXG4gICAgICAgICAgICAgIC5hdHRyKCdjbGFzcycsICdjaGFydF9feScpXG4gICAgICAgICAgICAgIC5hdHRyKFwidHJhbnNmb3JtXCIsIGB0cmFuc2xhdGUoMCwgMClgKVxuICAgICAgICAgICAgICAuY2FsbChkMy5heGlzTGVmdCh5KS50aWNrRm9ybWF0KGQgPT4gZCArIFwiwrAgXCIpKVxuICAgICAgICAgICAgICAuc2VsZWN0QWxsKCd0ZXh0JylcbiAgICAgICAgICAgICAgLmF0dHIoJ3gnLCAnLTIwcHgnKTtcbiAgICAgICAgICAgIGF1MiA9IGF1Mi5maWx0ZXIoIGkgPT4gaS5mZGF0ZSA+IG1pbiAmJiBpLmZkYXRlIDwgbWF4KTtcbiAgICAgICAgICAgIGF1NCA9IGF1NC5maWx0ZXIoIGkgPT4gaS5mZGF0ZSA+IG1pbiAmJiBpLmZkYXRlIDwgbWF4KTtcbiAgICAgICAgICAgIGEgICA9IGEuZmlsdGVyKCBpID0+IGkuZmRhdGUgPiBtaW4gJiYgaS5mZGF0ZSA8IG1heCk7XG4gICAgICAgICAgICBzdmcuYXBwZW5kKCdwYXRoJylcbiAgICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X19saW5lIGNoYXJ0X19saW5lLS1hdTQnKVxuICAgICAgICAgICAgICAuZGF0dW0oYXUyKVxuICAgICAgICAgICAgICAuYXR0cignZCcsIGQzLmxpbmUoKVxuICAgICAgICAgICAgICAgIC54KGQgPT4geChkLmZkYXRlKSlcbiAgICAgICAgICAgICAgICAueShkID0+IHkoZC50ZW1wZXJhdHVyZSkpXG4gICAgICAgICAgICAgICk7XG4gICAgICAgICAgICBzdmcuYXBwZW5kKCdwYXRoJylcbiAgICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X19saW5lIGNoYXJ0X19saW5lLS1hdTInKVxuICAgICAgICAgICAgICAuZGF0dW0oYXU0KVxuICAgICAgICAgICAgICAuYXR0cignZCcsIGQzLmxpbmUoKVxuICAgICAgICAgICAgICAgIC54KGQgPT4geChkLmZkYXRlKSlcbiAgICAgICAgICAgICAgICAueShkID0+IHkoZC50ZW1wZXJhdHVyZSkpXG4gICAgICAgICAgICAgICk7XG4gICAgICAgICAgICBzdmcuYXBwZW5kKCdwYXRoJylcbiAgICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X19saW5lIGNoYXJ0X19saW5lLS1hZXJvcG9ydCcpXG4gICAgICAgICAgICAgIC5kYXR1bShhKVxuICAgICAgICAgICAgICAuYXR0cignZCcsIGQzLmxpbmUoKVxuICAgICAgICAgICAgICAgIC54KGQgPT4geChkLmZkYXRlKSlcbiAgICAgICAgICAgICAgICAueShkID0+IHkoZC50ZW1wZXJhdHVyZSkpXG4gICAgICAgICAgICAgICk7XG5cbiAgICAgICAgICAgIC8vIEdldCBhdmVyYWdlIG9mIG1pbmltdW0gdGVtcGFlcmF0dXJlc1xuICAgICAgICAgICAgbGV0IG1lYXN1cmVzID0ge307XG4gICAgICAgICAgICBhdTIuZm9yRWFjaChpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBkYXQgPSBtb21lbnQoaS5mZGF0ZSkuZm9ybWF0KCdERE1NJyk7XG4gICAgICAgICAgICAgICAgaWYoIShkYXQgaW4gbWVhc3VyZXMpKXtcbiAgICAgICAgICAgICAgICAgICAgbWVhc3VyZXNbZGF0XSA9IFsgaS50ZW1wZXJhdHVyZV07XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbWVhc3VyZXNbZGF0XS5wdXNoKCBpLnRlbXBlcmF0dXJlICk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBsZXQgbWluaW11bSA9IFtdO1xuICAgICAgICAgICAgZm9yKGRheSBpbiBtZWFzdXJlcyl7XG4gICAgICAgICAgICAgICAgbWluaW11bS5wdXNoKCBkMy5taW4obWVhc3VyZXNbZGF5XSwgaT0+aSkgKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3QgYXZnX21pbiA9IGQzLm1lYW4oIG1pbmltdW0gKTtcbiAgICAgICAgICAgIGNvbnN0IGF2ZyA9IGQzLm1lYW4oYXUyLCBpID0+IGkudGVtcGVyYXR1cmUpO1xuICAgICAgICAgICAgZGlmZiA9IE1hdGguYWJzKGF2ZyAtIGF2Z19taW4pLnRvRml4ZWQoMik7XG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjYXUyX21pbiAuY2hhcnRfX2NoYXJ0LS1udW1iZXInKS5pbm5lckhUTUwgPSBgKyR7ZGlmZn3CsGA7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIE1hcFxuICAgICAgICAgIFxuICAgICAgICAgICAgZmV0Y2goJy9zdGF0aWMvamFyZGluZXMvanMvZGF0YS9jb3VudHJpZXMuZ2VvLmpzb24nKS50aGVuKCBpID0+IGkuanNvbigpICkudGhlbiggZGF0YSA9PiB7XG4gICAgICAgICAgICAgICAgdmFyIG1hcCA9IEwubWFwKCdtYXAnLCB7XG4gICAgICAgICAgICAgICAgICAgIGNlbnRlciAgICAgICAgICAgICA6IFsgMjAsIDAgXSxcbiAgICAgICAgICAgICAgICAgICAgem9vbSAgICAgICAgICAgICAgIDogMyxcbiAgICAgICAgICAgICAgICAgICAgbWF4Wm9vbSAgICAgICAgICAgIDogMTgsXG4gICAgICAgICAgICAgICAgICAgIGF0dHJpYnV0aW9uQ29udHJvbCA6IGZhbHNlLFxuICAgICAgICAgICAgICAgICAgICB6b29tQ29udHJvbCAgICAgICAgOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgc2Nyb2xsV2hlZWxab29tICAgIDogZmFsc2UsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgTC5nZW9Kc29uKGRhdGEsIHt9KS5hZGRUbyhtYXApO1xuICAgICAgICAgICAgICAgIGNvbnN0IHR0cyA9IFtcbiAgICAgICAgICAgICAgICAgICAgeyB0IDogZDMubWF4KCBhdTIuZmlsdGVyKCBpID0+IGkuZmRhdGUgPT0gZDMubWF4KGF1MiwgaSA9PiBpLmZkYXRlKSksIGkgPT4gaS50ZW1wZXJhdHVyZSksIGM6ICd5ZWxsb3cnIH0sXG4gICAgICAgICAgICAgICAgICAgIHsgdCA6IDIwLCBjOiAnYmx1ZScgfSxcbiAgICAgICAgICAgICAgICAgICAgeyB0IDogMzYsIGM6ICdyZWQnIH1cbiAgICAgICAgICAgICAgICBdO1xuICAgICAgICAgICAgICAgIGZldGNoKCcvc3RhdGljL2phcmRpbmVzL2pzL2RhdGEvZGF0YS5qc29uJykudGhlbiggaSA9PiBpLmpzb24oKSApLnRoZW4oIGRhdGEgPT4ge1xuICAgICAgICAgICAgICAgICAgICBmb3IoY29uc3QgW2ssIHZdIG9mIE9iamVjdC5lbnRyaWVzKGRhdGEpKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIHR0cy5mb3JFYWNoKCByZWYgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyggcGFyc2VJbnQobWF4LmZvcm1hdCgnTScpKSApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciB2YWwgPSBNYXRoLmFicyggcGFyc2VGbG9hdCh2WydhdmVyYWdlX3RlbXBlcmF0dXJlJ11bIHBhcnNlSW50KG1heC5mb3JtYXQoJ00nKSkgXSkgLSByZWYudCApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHYubGF0ICYmIHYubG9uICYmIHZhbCA8PSAxICl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBMLm1hcmtlcihbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhcnNlRmxvYXQodi5sYXQpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJzZUZsb2F0KHYubG9uKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpY29uIDogTC5kaXZJY29uKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpY29uU2l6ZSAgIDogWzIwLCAyMF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWNvbkFuY2hvciA6IFsxMCwgMTBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGh0bWw6IGA8ZGl2IGNsYXNzPVwiaW5uZXIgJHtyZWYuY31cIj48L2Rpdj5gXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS5hZGRUbyhtYXApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfSlcbn0pOyJdLCJzb3VyY2VSb290IjoiIn0=