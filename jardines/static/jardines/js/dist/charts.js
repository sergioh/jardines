/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"charts": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendors~charts"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn-bd": "./node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-mx": "./node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "./node_modules/moment/locale/es-mx.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Dependencies
const d3 = __webpack_require__(/*! d3 */ "./node_modules/d3/src/index.js");

const moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js"); // default namespace for SVGs


const NS = "http://www.w3.org/2000/svg"; // When DOM is ready

document.addEventListener('DOMContentLoaded', function () {
  let flowdata, sensordata;
  document.body.classList.add('loading'); // Get flow data and display it 

  fetch('/api/flow/').then(response => response.json()).then(data => {
    // Format data properly
    data.forEach(i => {
      i.fdate = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
      i.liters = parseFloat(i.liters);
    }); // Group data

    flowdata = d3.group(data, item => item.name);
    add_evolution_charts('#flowcharts', flowdata, 'liters', 0, 1, 'l.'); // Get sensor data and display it

    fetch('/api/sensor').then(response => response.json()).then(data => {
      // Format data properly
      data.forEach(i => {
        i.fdate = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
      }); // Group data

      fetch('/api/weather').then(response => response.json()).then(aeroport_data => {
        aeroport_data.forEach(i => {
          i.fdate = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
        });
        sensordata = d3.group(data, item => item.name);
        add_evolution_charts('#sensorcharts_temp', sensordata, 'temperature', 0, 60, '°', aeroport_data); // add_evolution_charts('#sensorcharts_press', sensordata, 'pressure', 'hPA', 0, 1500);

        add_evolution_charts('#sensorcharts_hum', sensordata, 'mapped_humidity', 0, 100, '%');
        document.querySelectorAll('.chartgroup').forEach(i => {
          i.classList.add('hidden');
        });
        document.querySelector('#flowcharts').parentNode.classList.remove('hidden');
        document.body.classList.remove('loading'); // On orientation change, redraw everything

        window.addEventListener('orientationchange', function () {
          document.querySelector('#flowcharts').innerHTML = '';
          document.querySelector('#sensorcharts_temp').innerHTML = '';
          document.querySelector('#sensorcharts_hum').innerHTML = '';
          add_evolution_charts('#flowcharts', flowdata, 'liters', 0, 1, 'l.');
          add_evolution_charts('#sensorcharts_temp', sensordata, 'temperature', 0, 60, '°', aeroport_data);
          add_evolution_charts('#sensorcharts_hum', sensordata, 'mapped_humidity', 0, 100, '%');
        });
      });
    });
  }); // Add basic interactions

  document.querySelectorAll('.charts-nav__item').forEach(i => {
    var target = i.dataset.show;
    i.addEventListener('click', e => {
      document.querySelectorAll('.chartgroup').forEach(i => {
        i.classList.add('hidden');
      });
      document.querySelector(`#${target}`).parentNode.classList.remove('hidden');
      document.querySelectorAll('.charts-nav__item').forEach(i => {
        i.classList.remove('active');
      });
      i.classList.add('active');
    });
  });
  window.addEventListener('orientationchange', function () {
    document.querySelectorAll('.chartgroup__container').forEach(i => {
      i.innerHTML = '';
    });
  });
});

const whatis = id => {
  const prefixes = {
    'b': 'Línea de riego',
    't': 'Sensor de tierra',
    'aa': 'Aire acondicionado',
    'r': 'Aporte de red',
    'au': 'Aula exterior',
    'e': 'Sensor exterior de fachada'
  };
  let label = '';
  Object.keys(prefixes).forEach(k => {
    if (id.startsWith(k)) {
      label = prefixes[k];
      return;
    }
  });
  return label ? label : id;
};
/**
 *  create_flow_charts
 */


const add_evolution_line = (value, fieldkey, chart, x, y) => {
  chart.select('.chart__line--compared').remove();
  chart.append('path').attr('class', 'chart__line chart__line--compared').datum(value).attr('d', d3.line().x(d => x(d.fdate)).y(d => y(d[fieldkey]))).attr('stroke-dasharray', '4 2');
};

const add_control = (wrapper, label, options, callback) => {
  const control = document.createElement('div');
  control.classList.add('chart-control');
  const control_label = document.createElement('span');
  control_label.classList.add('chart-control__label');
  control_label.innerHTML = label;
  control.appendChild(control_label);
  const control_items = document.createElement('select');
  control_items.classList.add('chart-control__select');
  options.forEach(option => {
    const control_item = document.createElement('option');
    control_item.classList.add('chart-control__option');
    control_item.innerHTML = option.k;
    control_item.value = option.v;
    control_items.appendChild(control_item);
  });
  control_items.addEventListener('change', callback);
  control.appendChild(control_items);
  wrapper.appendChild(control);
};

const add_evolution_charts = (parentnode, data, fieldkey, min, max, suffix, refdata) => {
  const flowcharts = document.querySelector(parentnode);

  for (const [key, value] of data.entries()) {
    // If all values are negative don't render the chart
    if (!value.some(i => i[fieldkey] > 0)) continue; // Add html wrapper to D3 chart and its main elements

    const wrapper = document.createElement('div');
    wrapper.classList.add(['chart'], ['chart--flow']);
    const label = document.createElement('h4');
    label.classList.add('chart__label');
    label.innerHTML = `${whatis(key)} <span class="chart__label-key">${key}</span>`;
    wrapper.appendChild(label);
    flowcharts.appendChild(wrapper); // Add chart wrapper

    const width = wrapper.clientWidth;
    const height = 200;
    const svg_node = document.createElementNS(NS, 'svg');
    svg_node.classList.add('chart__svg');
    svg_node.id = `${fieldkey}_${key}`;
    wrapper.appendChild(svg_node); // Create D3 chart

    const svg = d3.select(`#${fieldkey}_${key}`);
    svg.attr('width', width).attr('height', height);
    /**
     * Create chart scales and add them to chart
     */

    const max_time = value[value.length - 1].fdate;
    let min_time = moment(max_time).subtract(30, 'days'); // Get last month by default

    let x = d3.scaleTime().domain([min_time, max_time]).range([0, width]);
    svg.append("g").attr('class', 'chart__x').attr("transform", `translate(0, ${height})`).call(d3.axisBottom(x).ticks(3).tickFormat(d => {
      return moment(d).format('DD-MM hh:mm');
    }));
    let y = d3.scaleLinear().domain([min, max]).range([height, 0]);
    svg.append("g").attr('class', 'chart__y').attr("transform", `translate(0, 0)`).call(d3.axisLeft(y).tickFormat(d => d + ' ' + suffix)).selectAll('text').attr('x', '-20px');
    /**
     *  Add main evolution line
     */

    let line = svg.append('path').attr('class', 'chart__line').datum(value.filter(i => i.fdate > min_time)).attr('d', d3.line().x(d => x(d.fdate)).y(d => y(d[fieldkey])));
    /**
     *  Add controls
     */

    const controls = document.createElement('div');
    controls.classList.add('chart__controls');
    wrapper.appendChild(controls); // Add comparator

    const comparable = [{
      k: 'No comparar',
      v: 'none'
    }];

    if (refdata) {
      comparable.push({
        k: 'Aeropuerto',
        v: 'aeroport'
      });
    }

    for (const [k, v] of data.entries()) {
      if (k != key) comparable.push({
        k: k,
        v: k
      });
    }

    add_control(controls, 'Comparar con', comparable, function (e) {
      switch (e.target.value) {
        case 'none':
          svg.select('.chart__line--compared').remove();
          break;

        case 'aeroport':
          let refdata_purged = refdata.filter(i => i.fdate >= min_time && i.fdate <= max_time);
          const refline = svg.append('path').attr('class', 'chart__line--compared').datum(refdata_purged).attr('d', d3.line().x(d => x(d.fdate)).y(d => y(d[fieldkey])));
          add_evolution_line(refdata_purged, fieldkey, svg, x, y);
          break;

        default:
          const v = data.get(e.target.value);
          const filtered = v.filter(i => i.fdate >= min_time);
          add_evolution_line(filtered, fieldkey, svg, x, y);
          break;
      }

      ;
    }); //  Add dates control 

    add_control(controls, 'Desde', [{
      k: 'Último mes',
      v: 30
    }, {
      k: 'Última semana',
      v: 7
    }, {
      k: 'Primeras mediciones',
      v: 0
    }], function (e) {
      const val = e.target.value;

      switch (val) {
        case '0':
          var ref = moment(value[0].fdate).unix();
          break;

        default:
          var ref = moment(max_time).subtract(val, 'days').unix();
          break;
      }

      value.every(i => {
        if (moment(i.fdate).unix() >= ref) {
          min_time = i.fdate;
          svg.select('.chart__line').remove();
          svg.select('.chart__line--compared').remove();
          x = d3.scaleTime().domain([min_time, max_time]).range([0, width]);
          svg.select('.chart__x').call(d3.axisBottom(x).ticks(3).tickFormat(d => {
            return moment(d).format('DD-MM HH:mm');
          }));
          const new_values = value.filter(i => i.fdate >= min_time);
          line = svg.append('path').attr('class', 'chart__line').datum(new_values).attr('d', d3.line().x(d => x(d.fdate)).y(d => y(d[fieldkey])));
          return false;
        }

        return true;
      });
    });
    /**
     *    Add events
     */

    var focus = svg.append("g");
    focus.attr('class', 'chart__value');
    focus.append("circle").attr("class", `dot-${fieldkey}_${key}`).style("fill", "transparent").style("stroke", "transparent").attr("r", 4);
    focus.append("text").attr("class", `value-${fieldkey}_${key}`).attr('x', width).attr('y', 20).style('text-anchor', 'end').style("fill", "transparent").style("stroke", "transparent").style('font-weight', 'bold');
    focus.append("text").attr("class", `date-${fieldkey}_${key}`).attr('x', width).attr('y', 0).style('text-anchor', 'end').style("fill", "transparent").style("stroke", "transparent");
    svg.append("rect").attr("width", width).attr("height", height).attr("fill", "transparent").on("mousemove", (e, d) => {
      const date = x.invert(d3.pointer(e)[0]);
      var tmp = value.map(i => Math.abs(i.fdate - date));
      var closest = tmp.indexOf(Math.min(...tmp));
      const val = value[closest][fieldkey];
      d3.select(`.dot-${fieldkey}_${key}`).style("stroke", "red").attr('cx', x(value[closest].fdate)).attr('cy', y(val));
      d3.select(`text.value-${fieldkey}_${key}`).style("fill", "red").html(val.toFixed(2) + ' ' + suffix);
      d3.select(`text.date-${fieldkey}_${key}`).style("fill", "red").html(value[closest].fdate.toLocaleDateString('es-ES', {
        hour: '2-digit',
        minute: '2-digit'
      }));
    }).on("mouseleave", (e, d) => {
      d3.select(`.dot-${fieldkey}_${key}`).style('stroke', 'transparent');
      d3.select(`.value-${fieldkey}_${key}`).style('fill', 'transparent');
      d3.select(`.date-${fieldkey}_${key}`).style('fill', 'transparent');
    });
  }

  ;
};

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUgc3luYyBeXFwuXFwvLiokIiwid2VicGFjazovLy8uL3NyYy9pbmRleC5qcyJdLCJuYW1lcyI6WyJkMyIsInJlcXVpcmUiLCJtb21lbnQiLCJOUyIsImRvY3VtZW50IiwiYWRkRXZlbnRMaXN0ZW5lciIsImZsb3dkYXRhIiwic2Vuc29yZGF0YSIsImJvZHkiLCJjbGFzc0xpc3QiLCJhZGQiLCJmZXRjaCIsInRoZW4iLCJyZXNwb25zZSIsImpzb24iLCJkYXRhIiwiZm9yRWFjaCIsImkiLCJmZGF0ZSIsInRpbWVQYXJzZSIsImxpdGVycyIsInBhcnNlRmxvYXQiLCJncm91cCIsIml0ZW0iLCJuYW1lIiwiYWRkX2V2b2x1dGlvbl9jaGFydHMiLCJhZXJvcG9ydF9kYXRhIiwicXVlcnlTZWxlY3RvckFsbCIsInF1ZXJ5U2VsZWN0b3IiLCJwYXJlbnROb2RlIiwicmVtb3ZlIiwid2luZG93IiwiaW5uZXJIVE1MIiwidGFyZ2V0IiwiZGF0YXNldCIsInNob3ciLCJlIiwid2hhdGlzIiwiaWQiLCJwcmVmaXhlcyIsImxhYmVsIiwiT2JqZWN0Iiwia2V5cyIsImsiLCJzdGFydHNXaXRoIiwiYWRkX2V2b2x1dGlvbl9saW5lIiwidmFsdWUiLCJmaWVsZGtleSIsImNoYXJ0IiwieCIsInkiLCJzZWxlY3QiLCJhcHBlbmQiLCJhdHRyIiwiZGF0dW0iLCJsaW5lIiwiZCIsImFkZF9jb250cm9sIiwid3JhcHBlciIsIm9wdGlvbnMiLCJjYWxsYmFjayIsImNvbnRyb2wiLCJjcmVhdGVFbGVtZW50IiwiY29udHJvbF9sYWJlbCIsImFwcGVuZENoaWxkIiwiY29udHJvbF9pdGVtcyIsIm9wdGlvbiIsImNvbnRyb2xfaXRlbSIsInYiLCJwYXJlbnRub2RlIiwibWluIiwibWF4Iiwic3VmZml4IiwicmVmZGF0YSIsImZsb3djaGFydHMiLCJrZXkiLCJlbnRyaWVzIiwic29tZSIsIndpZHRoIiwiY2xpZW50V2lkdGgiLCJoZWlnaHQiLCJzdmdfbm9kZSIsImNyZWF0ZUVsZW1lbnROUyIsInN2ZyIsIm1heF90aW1lIiwibGVuZ3RoIiwibWluX3RpbWUiLCJzdWJ0cmFjdCIsInNjYWxlVGltZSIsImRvbWFpbiIsInJhbmdlIiwiY2FsbCIsImF4aXNCb3R0b20iLCJ0aWNrcyIsInRpY2tGb3JtYXQiLCJmb3JtYXQiLCJzY2FsZUxpbmVhciIsImF4aXNMZWZ0Iiwic2VsZWN0QWxsIiwiZmlsdGVyIiwiY29udHJvbHMiLCJjb21wYXJhYmxlIiwicHVzaCIsInJlZmRhdGFfcHVyZ2VkIiwicmVmbGluZSIsImdldCIsImZpbHRlcmVkIiwidmFsIiwicmVmIiwidW5peCIsImV2ZXJ5IiwibmV3X3ZhbHVlcyIsImZvY3VzIiwic3R5bGUiLCJvbiIsImRhdGUiLCJpbnZlcnQiLCJwb2ludGVyIiwidG1wIiwibWFwIiwiTWF0aCIsImFicyIsImNsb3Nlc3QiLCJpbmRleE9mIiwiaHRtbCIsInRvRml4ZWQiLCJ0b0xvY2FsZURhdGVTdHJpbmciLCJob3VyIiwibWludXRlIl0sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSxRQUFRLG9CQUFvQjtRQUM1QjtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLGlCQUFpQiw0QkFBNEI7UUFDN0M7UUFDQTtRQUNBLGtCQUFrQiwyQkFBMkI7UUFDN0M7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQSxnQkFBZ0IsdUJBQXVCO1FBQ3ZDOzs7UUFHQTtRQUNBO1FBQ0E7UUFDQTs7Ozs7Ozs7Ozs7O0FDdkpBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2RTs7Ozs7Ozs7Ozs7QUNuU0E7QUFFQSxNQUFNQSxFQUFFLEdBQUdDLG1CQUFPLENBQUMsMENBQUQsQ0FBbEI7O0FBQ0EsTUFBTUMsTUFBTSxHQUFHRCxtQkFBTyxDQUFDLCtDQUFELENBQXRCLEMsQ0FFQTs7O0FBQ0EsTUFBTUUsRUFBRSxHQUFHLDRCQUFYLEMsQ0FFQTs7QUFDQUMsUUFBUSxDQUFDQyxnQkFBVCxDQUEwQixrQkFBMUIsRUFBOEMsWUFBVTtBQUVyRCxNQUFJQyxRQUFKLEVBQWNDLFVBQWQ7QUFDQUgsVUFBUSxDQUFDSSxJQUFULENBQWNDLFNBQWQsQ0FBd0JDLEdBQXhCLENBQTRCLFNBQTVCLEVBSHFELENBS3BEOztBQUNBQyxPQUFLLENBQUMsWUFBRCxDQUFMLENBQW9CQyxJQUFwQixDQUEwQkMsUUFBUSxJQUFJQSxRQUFRLENBQUNDLElBQVQsRUFBdEMsRUFBd0RGLElBQXhELENBQThERyxJQUFJLElBQUk7QUFDbEU7QUFDQUEsUUFBSSxDQUFDQyxPQUFMLENBQWNDLENBQUMsSUFBSTtBQUNmQSxPQUFDLENBQUNDLEtBQUYsR0FBV2xCLEVBQUUsQ0FBQ21CLFNBQUgsQ0FBYSxtQkFBYixFQUFrQ0YsQ0FBQyxDQUFDQyxLQUFwQyxDQUFYO0FBQ0FELE9BQUMsQ0FBQ0csTUFBRixHQUFXQyxVQUFVLENBQUVKLENBQUMsQ0FBQ0csTUFBSixDQUFyQjtBQUNILEtBSEQsRUFGa0UsQ0FNbEU7O0FBQ0FkLFlBQVEsR0FBR04sRUFBRSxDQUFDc0IsS0FBSCxDQUFVUCxJQUFWLEVBQWdCUSxJQUFJLElBQUlBLElBQUksQ0FBQ0MsSUFBN0IsQ0FBWDtBQUNBQyx3QkFBb0IsQ0FBQyxhQUFELEVBQWdCbkIsUUFBaEIsRUFBMEIsUUFBMUIsRUFBb0MsQ0FBcEMsRUFBdUMsQ0FBdkMsRUFBMEMsSUFBMUMsQ0FBcEIsQ0FSa0UsQ0FTbEU7O0FBQ0FLLFNBQUssQ0FBQyxhQUFELENBQUwsQ0FBcUJDLElBQXJCLENBQTJCQyxRQUFRLElBQUlBLFFBQVEsQ0FBQ0MsSUFBVCxFQUF2QyxFQUF5REYsSUFBekQsQ0FBK0RHLElBQUksSUFBSTtBQUNuRTtBQUNBQSxVQUFJLENBQUNDLE9BQUwsQ0FBY0MsQ0FBQyxJQUFJO0FBQ2ZBLFNBQUMsQ0FBQ0MsS0FBRixHQUFXbEIsRUFBRSxDQUFDbUIsU0FBSCxDQUFhLG1CQUFiLEVBQWtDRixDQUFDLENBQUNDLEtBQXBDLENBQVg7QUFDSCxPQUZELEVBRm1FLENBS25FOztBQUNBUCxXQUFLLENBQUMsY0FBRCxDQUFMLENBQXNCQyxJQUF0QixDQUE0QkMsUUFBUSxJQUFJQSxRQUFRLENBQUNDLElBQVQsRUFBeEMsRUFBMERGLElBQTFELENBQWdFYyxhQUFhLElBQUk7QUFDN0VBLHFCQUFhLENBQUNWLE9BQWQsQ0FBdUJDLENBQUMsSUFBSTtBQUN4QkEsV0FBQyxDQUFDQyxLQUFGLEdBQVdsQixFQUFFLENBQUNtQixTQUFILENBQWEsbUJBQWIsRUFBa0NGLENBQUMsQ0FBQ0MsS0FBcEMsQ0FBWDtBQUNILFNBRkQ7QUFHQVgsa0JBQVUsR0FBR1AsRUFBRSxDQUFDc0IsS0FBSCxDQUFVUCxJQUFWLEVBQWdCUSxJQUFJLElBQUlBLElBQUksQ0FBQ0MsSUFBN0IsQ0FBYjtBQUNBQyw0QkFBb0IsQ0FBQyxvQkFBRCxFQUF1QmxCLFVBQXZCLEVBQW1DLGFBQW5DLEVBQWtELENBQWxELEVBQXFELEVBQXJELEVBQXlELEdBQXpELEVBQThEbUIsYUFBOUQsQ0FBcEIsQ0FMNkUsQ0FNN0U7O0FBQ0FELDRCQUFvQixDQUFDLG1CQUFELEVBQXNCbEIsVUFBdEIsRUFBa0MsaUJBQWxDLEVBQXFELENBQXJELEVBQXdELEdBQXhELEVBQTZELEdBQTdELENBQXBCO0FBRUFILGdCQUFRLENBQUN1QixnQkFBVCxDQUEwQixhQUExQixFQUF5Q1gsT0FBekMsQ0FBa0RDLENBQUMsSUFBSTtBQUFFQSxXQUFDLENBQUNSLFNBQUYsQ0FBWUMsR0FBWixDQUFnQixRQUFoQjtBQUE0QixTQUFyRjtBQUNBTixnQkFBUSxDQUFDd0IsYUFBVCxDQUF1QixhQUF2QixFQUFzQ0MsVUFBdEMsQ0FBaURwQixTQUFqRCxDQUEyRHFCLE1BQTNELENBQWtFLFFBQWxFO0FBQ0ExQixnQkFBUSxDQUFDSSxJQUFULENBQWNDLFNBQWQsQ0FBd0JxQixNQUF4QixDQUErQixTQUEvQixFQVg2RSxDQWM3RTs7QUFDQUMsY0FBTSxDQUFDMUIsZ0JBQVAsQ0FBd0IsbUJBQXhCLEVBQTZDLFlBQVU7QUFDbkRELGtCQUFRLENBQUN3QixhQUFULENBQXVCLGFBQXZCLEVBQXNDSSxTQUF0QyxHQUFrRCxFQUFsRDtBQUNBNUIsa0JBQVEsQ0FBQ3dCLGFBQVQsQ0FBdUIsb0JBQXZCLEVBQTZDSSxTQUE3QyxHQUF5RCxFQUF6RDtBQUNBNUIsa0JBQVEsQ0FBQ3dCLGFBQVQsQ0FBdUIsbUJBQXZCLEVBQTRDSSxTQUE1QyxHQUF3RCxFQUF4RDtBQUNBUCw4QkFBb0IsQ0FBQyxhQUFELEVBQWdCbkIsUUFBaEIsRUFBMEIsUUFBMUIsRUFBb0MsQ0FBcEMsRUFBdUMsQ0FBdkMsRUFBMEMsSUFBMUMsQ0FBcEI7QUFDQW1CLDhCQUFvQixDQUFDLG9CQUFELEVBQXVCbEIsVUFBdkIsRUFBbUMsYUFBbkMsRUFBa0QsQ0FBbEQsRUFBcUQsRUFBckQsRUFBeUQsR0FBekQsRUFBOERtQixhQUE5RCxDQUFwQjtBQUNBRCw4QkFBb0IsQ0FBQyxtQkFBRCxFQUFzQmxCLFVBQXRCLEVBQWtDLGlCQUFsQyxFQUFxRCxDQUFyRCxFQUF3RCxHQUF4RCxFQUE2RCxHQUE3RCxDQUFwQjtBQUNILFNBUEQ7QUFRSCxPQXZCRDtBQXdCSCxLQTlCRDtBQStCSCxHQXpDRCxFQU5vRCxDQWlEcEQ7O0FBQ0FILFVBQVEsQ0FBQ3VCLGdCQUFULENBQTBCLG1CQUExQixFQUErQ1gsT0FBL0MsQ0FBd0RDLENBQUMsSUFBSTtBQUN6RCxRQUFJZ0IsTUFBTSxHQUFHaEIsQ0FBQyxDQUFDaUIsT0FBRixDQUFVQyxJQUF2QjtBQUNBbEIsS0FBQyxDQUFDWixnQkFBRixDQUFtQixPQUFuQixFQUE0QitCLENBQUMsSUFBSTtBQUM3QmhDLGNBQVEsQ0FBQ3VCLGdCQUFULENBQTBCLGFBQTFCLEVBQXlDWCxPQUF6QyxDQUFrREMsQ0FBQyxJQUFJO0FBQUVBLFNBQUMsQ0FBQ1IsU0FBRixDQUFZQyxHQUFaLENBQWdCLFFBQWhCO0FBQTRCLE9BQXJGO0FBQ0FOLGNBQVEsQ0FBQ3dCLGFBQVQsQ0FBd0IsSUFBR0ssTUFBTyxFQUFsQyxFQUFxQ0osVUFBckMsQ0FBZ0RwQixTQUFoRCxDQUEwRHFCLE1BQTFELENBQWlFLFFBQWpFO0FBQ0ExQixjQUFRLENBQUN1QixnQkFBVCxDQUEwQixtQkFBMUIsRUFBK0NYLE9BQS9DLENBQXdEQyxDQUFDLElBQUk7QUFBRUEsU0FBQyxDQUFDUixTQUFGLENBQVlxQixNQUFaLENBQW1CLFFBQW5CO0FBQThCLE9BQTdGO0FBQ0FiLE9BQUMsQ0FBQ1IsU0FBRixDQUFZQyxHQUFaLENBQWdCLFFBQWhCO0FBQ0gsS0FMRDtBQU1ILEdBUkQ7QUFVQXFCLFFBQU0sQ0FBQzFCLGdCQUFQLENBQXdCLG1CQUF4QixFQUE2QyxZQUFVO0FBQ25ERCxZQUFRLENBQUN1QixnQkFBVCxDQUEwQix3QkFBMUIsRUFBb0RYLE9BQXBELENBQTZEQyxDQUFDLElBQUk7QUFDOURBLE9BQUMsQ0FBQ2UsU0FBRixHQUFjLEVBQWQ7QUFDSCxLQUZEO0FBR0gsR0FKRDtBQUtILENBakVEOztBQW1FQSxNQUFNSyxNQUFNLEdBQUtDLEVBQUYsSUFBVTtBQUN0QixRQUFNQyxRQUFRLEdBQUc7QUFDZCxTQUFPLGdCQURPO0FBRWQsU0FBTyxrQkFGTztBQUdkLFVBQU8sb0JBSE87QUFJZCxTQUFPLGVBSk87QUFLZCxVQUFPLGVBTE87QUFNZCxTQUFPO0FBTk8sR0FBakI7QUFRQSxNQUFJQyxLQUFLLEdBQUcsRUFBWjtBQUNBQyxRQUFNLENBQUNDLElBQVAsQ0FBWUgsUUFBWixFQUFzQnZCLE9BQXRCLENBQStCMkIsQ0FBQyxJQUFJO0FBQ2pDLFFBQUlMLEVBQUUsQ0FBQ00sVUFBSCxDQUFjRCxDQUFkLENBQUosRUFBc0I7QUFDbEJILFdBQUssR0FBR0QsUUFBUSxDQUFDSSxDQUFELENBQWhCO0FBQ0E7QUFDSDtBQUNILEdBTEQ7QUFNQSxTQUFPSCxLQUFLLEdBQUdBLEtBQUgsR0FBV0YsRUFBdkI7QUFDRixDQWpCRDtBQW1CQTtBQUNBO0FBQ0E7OztBQUVBLE1BQU1PLGtCQUFrQixHQUFHLENBQUVDLEtBQUYsRUFBU0MsUUFBVCxFQUFtQkMsS0FBbkIsRUFBMEJDLENBQTFCLEVBQTZCQyxDQUE3QixLQUFvQztBQUMzREYsT0FBSyxDQUFDRyxNQUFOLENBQWMsd0JBQWQsRUFBeUNyQixNQUF6QztBQUNBa0IsT0FBSyxDQUFDSSxNQUFOLENBQWEsTUFBYixFQUNLQyxJQURMLENBQ1UsT0FEVixFQUNtQixtQ0FEbkIsRUFFS0MsS0FGTCxDQUVZUixLQUZaLEVBR0tPLElBSEwsQ0FHVSxHQUhWLEVBR2VyRCxFQUFFLENBQUN1RCxJQUFILEdBQ05OLENBRE0sQ0FDSE8sQ0FBQyxJQUFJUCxDQUFDLENBQUVPLENBQUMsQ0FBQ3RDLEtBQUosQ0FESCxFQUVOZ0MsQ0FGTSxDQUVITSxDQUFDLElBQUlOLENBQUMsQ0FBRU0sQ0FBQyxDQUFFVCxRQUFGLENBQUgsQ0FGSCxDQUhmLEVBTU1NLElBTk4sQ0FNVyxrQkFOWCxFQU0rQixLQU4vQjtBQU9ILENBVEQ7O0FBV0EsTUFBTUksV0FBVyxHQUFHLENBQUNDLE9BQUQsRUFBVWxCLEtBQVYsRUFBaUJtQixPQUFqQixFQUEwQkMsUUFBMUIsS0FBdUM7QUFDdkQsUUFBTUMsT0FBTyxHQUFHekQsUUFBUSxDQUFDMEQsYUFBVCxDQUF1QixLQUF2QixDQUFoQjtBQUNBRCxTQUFPLENBQUNwRCxTQUFSLENBQWtCQyxHQUFsQixDQUFzQixlQUF0QjtBQUNBLFFBQU1xRCxhQUFhLEdBQUczRCxRQUFRLENBQUMwRCxhQUFULENBQXVCLE1BQXZCLENBQXRCO0FBQ0FDLGVBQWEsQ0FBQ3RELFNBQWQsQ0FBd0JDLEdBQXhCLENBQTRCLHNCQUE1QjtBQUNBcUQsZUFBYSxDQUFDL0IsU0FBZCxHQUEwQlEsS0FBMUI7QUFDQXFCLFNBQU8sQ0FBQ0csV0FBUixDQUFvQkQsYUFBcEI7QUFDQSxRQUFNRSxhQUFhLEdBQUc3RCxRQUFRLENBQUMwRCxhQUFULENBQXVCLFFBQXZCLENBQXRCO0FBQ0FHLGVBQWEsQ0FBQ3hELFNBQWQsQ0FBd0JDLEdBQXhCLENBQTRCLHVCQUE1QjtBQUNBaUQsU0FBTyxDQUFDM0MsT0FBUixDQUFpQmtELE1BQU0sSUFBSTtBQUN2QixVQUFNQyxZQUFZLEdBQUcvRCxRQUFRLENBQUMwRCxhQUFULENBQXVCLFFBQXZCLENBQXJCO0FBQ0FLLGdCQUFZLENBQUMxRCxTQUFiLENBQXVCQyxHQUF2QixDQUEyQix1QkFBM0I7QUFDQXlELGdCQUFZLENBQUNuQyxTQUFiLEdBQXlCa0MsTUFBTSxDQUFDdkIsQ0FBaEM7QUFDQXdCLGdCQUFZLENBQUNyQixLQUFiLEdBQXlCb0IsTUFBTSxDQUFDRSxDQUFoQztBQUNBSCxpQkFBYSxDQUFDRCxXQUFkLENBQTJCRyxZQUEzQjtBQUNILEdBTkQ7QUFPQUYsZUFBYSxDQUFDNUQsZ0JBQWQsQ0FBK0IsUUFBL0IsRUFBeUN1RCxRQUF6QztBQUNBQyxTQUFPLENBQUNHLFdBQVIsQ0FBcUJDLGFBQXJCO0FBQ0FQLFNBQU8sQ0FBQ00sV0FBUixDQUFxQkgsT0FBckI7QUFDSCxDQW5CRDs7QUFxQkEsTUFBTXBDLG9CQUFvQixHQUFHLENBQUU0QyxVQUFGLEVBQWN0RCxJQUFkLEVBQW9CZ0MsUUFBcEIsRUFBOEJ1QixHQUE5QixFQUFtQ0MsR0FBbkMsRUFBd0NDLE1BQXhDLEVBQWdEQyxPQUFoRCxLQUE2RDtBQUN0RixRQUFNQyxVQUFVLEdBQUd0RSxRQUFRLENBQUN3QixhQUFULENBQXVCeUMsVUFBdkIsQ0FBbkI7O0FBRUEsT0FBSyxNQUFNLENBQUNNLEdBQUQsRUFBTTdCLEtBQU4sQ0FBWCxJQUEyQi9CLElBQUksQ0FBQzZELE9BQUwsRUFBM0IsRUFDQTtBQUNJO0FBQ0EsUUFBRyxDQUFDOUIsS0FBSyxDQUFDK0IsSUFBTixDQUFZNUQsQ0FBQyxJQUFJQSxDQUFDLENBQUM4QixRQUFELENBQUQsR0FBYyxDQUEvQixDQUFKLEVBQXVDLFNBRjNDLENBSUk7O0FBQ0EsVUFBTVcsT0FBTyxHQUFHdEQsUUFBUSxDQUFDMEQsYUFBVCxDQUF1QixLQUF2QixDQUFoQjtBQUNBSixXQUFPLENBQUNqRCxTQUFSLENBQWtCQyxHQUFsQixDQUFzQixDQUFDLE9BQUQsQ0FBdEIsRUFBaUMsQ0FBQyxhQUFELENBQWpDO0FBQ0EsVUFBTThCLEtBQUssR0FBS3BDLFFBQVEsQ0FBQzBELGFBQVQsQ0FBdUIsSUFBdkIsQ0FBaEI7QUFDQXRCLFNBQUssQ0FBQy9CLFNBQU4sQ0FBZ0JDLEdBQWhCLENBQW9CLGNBQXBCO0FBQ0E4QixTQUFLLENBQUNSLFNBQU4sR0FBbUIsR0FBR0ssTUFBTSxDQUFDc0MsR0FBRCxDQUFPLG1DQUFrQ0EsR0FBSSxTQUF6RTtBQUNBakIsV0FBTyxDQUFDTSxXQUFSLENBQXFCeEIsS0FBckI7QUFDQWtDLGNBQVUsQ0FBQ1YsV0FBWCxDQUF3Qk4sT0FBeEIsRUFYSixDQWFJOztBQUNBLFVBQU1vQixLQUFLLEdBQUlwQixPQUFPLENBQUNxQixXQUF2QjtBQUNBLFVBQU1DLE1BQU0sR0FBRyxHQUFmO0FBQ0EsVUFBTUMsUUFBUSxHQUFHN0UsUUFBUSxDQUFDOEUsZUFBVCxDQUF5Qi9FLEVBQXpCLEVBQTZCLEtBQTdCLENBQWpCO0FBQ0E4RSxZQUFRLENBQUN4RSxTQUFULENBQW1CQyxHQUFuQixDQUF1QixZQUF2QjtBQUNBdUUsWUFBUSxDQUFDM0MsRUFBVCxHQUFlLEdBQUVTLFFBQVMsSUFBRzRCLEdBQUksRUFBakM7QUFDQWpCLFdBQU8sQ0FBQ00sV0FBUixDQUFxQmlCLFFBQXJCLEVBbkJKLENBcUJJOztBQUNBLFVBQU1FLEdBQUcsR0FBR25GLEVBQUUsQ0FBQ21ELE1BQUgsQ0FBWSxJQUFHSixRQUFTLElBQUc0QixHQUFJLEVBQS9CLENBQVo7QUFDQVEsT0FBRyxDQUFDOUIsSUFBSixDQUFTLE9BQVQsRUFBa0J5QixLQUFsQixFQUF5QnpCLElBQXpCLENBQStCLFFBQS9CLEVBQXlDMkIsTUFBekM7QUFFQTtBQUNSO0FBQ0E7O0FBRVEsVUFBTUksUUFBUSxHQUFHdEMsS0FBSyxDQUFFQSxLQUFLLENBQUN1QyxNQUFOLEdBQWUsQ0FBakIsQ0FBTCxDQUEwQm5FLEtBQTNDO0FBQ0EsUUFBSW9FLFFBQVEsR0FBS3BGLE1BQU0sQ0FBQ2tGLFFBQUQsQ0FBTixDQUFpQkcsUUFBakIsQ0FBMkIsRUFBM0IsRUFBK0IsTUFBL0IsQ0FBakIsQ0E5QkosQ0E4QjhEOztBQUMxRCxRQUFJdEMsQ0FBQyxHQUFHakQsRUFBRSxDQUFDd0YsU0FBSCxHQUFlQyxNQUFmLENBQXNCLENBQzFCSCxRQUQwQixFQUUxQkYsUUFGMEIsQ0FBdEIsRUFHTE0sS0FISyxDQUdDLENBQUUsQ0FBRixFQUFLWixLQUFMLENBSEQsQ0FBUjtBQUlBSyxPQUFHLENBQUMvQixNQUFKLENBQVcsR0FBWCxFQUNLQyxJQURMLENBQ1UsT0FEVixFQUNtQixVQURuQixFQUVLQSxJQUZMLENBRVUsV0FGVixFQUV3QixnQkFBZTJCLE1BQU8sR0FGOUMsRUFHS1csSUFITCxDQUdXM0YsRUFBRSxDQUFDNEYsVUFBSCxDQUFjM0MsQ0FBZCxFQUFpQjRDLEtBQWpCLENBQXVCLENBQXZCLEVBQTBCQyxVQUExQixDQUFzQ3RDLENBQUMsSUFBSTtBQUM5QyxhQUFPdEQsTUFBTSxDQUFDc0QsQ0FBRCxDQUFOLENBQVV1QyxNQUFWLENBQWlCLGFBQWpCLENBQVA7QUFDSCxLQUZNLENBSFg7QUFNQSxRQUFJN0MsQ0FBQyxHQUFHbEQsRUFBRSxDQUFDZ0csV0FBSCxHQUNIUCxNQURHLENBQ0ksQ0FBQ25CLEdBQUQsRUFBTUMsR0FBTixDQURKLEVBQ2dCbUIsS0FEaEIsQ0FDc0IsQ0FBRVYsTUFBRixFQUFVLENBQVYsQ0FEdEIsQ0FBUjtBQUVBRyxPQUFHLENBQUMvQixNQUFKLENBQVcsR0FBWCxFQUNLQyxJQURMLENBQ1UsT0FEVixFQUNtQixVQURuQixFQUVLQSxJQUZMLENBRVUsV0FGVixFQUV3QixpQkFGeEIsRUFHS3NDLElBSEwsQ0FHVzNGLEVBQUUsQ0FBQ2lHLFFBQUgsQ0FBWS9DLENBQVosRUFBZTRDLFVBQWYsQ0FBMkJ0QyxDQUFDLElBQUlBLENBQUMsR0FBRyxHQUFKLEdBQVVnQixNQUExQyxDQUhYLEVBSUswQixTQUpMLENBSWUsTUFKZixFQUtLN0MsSUFMTCxDQUtVLEdBTFYsRUFLZSxPQUxmO0FBT0E7QUFDUjtBQUNBOztBQUVRLFFBQUlFLElBQUksR0FBRzRCLEdBQUcsQ0FBQy9CLE1BQUosQ0FBVyxNQUFYLEVBQ05DLElBRE0sQ0FDRCxPQURDLEVBQ1EsYUFEUixFQUVOQyxLQUZNLENBRUNSLEtBQUssQ0FBQ3FELE1BQU4sQ0FBY2xGLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxLQUFGLEdBQVVvRSxRQUE3QixDQUZELEVBR05qQyxJQUhNLENBR0QsR0FIQyxFQUdJckQsRUFBRSxDQUFDdUQsSUFBSCxHQUNOTixDQURNLENBQ0hPLENBQUMsSUFBSVAsQ0FBQyxDQUFFTyxDQUFDLENBQUN0QyxLQUFKLENBREgsRUFFTmdDLENBRk0sQ0FFSE0sQ0FBQyxJQUFJTixDQUFDLENBQUVNLENBQUMsQ0FBRVQsUUFBRixDQUFILENBRkgsQ0FISixDQUFYO0FBUUE7QUFDUjtBQUNBOztBQUVRLFVBQU1xRCxRQUFRLEdBQUdoRyxRQUFRLENBQUMwRCxhQUFULENBQXVCLEtBQXZCLENBQWpCO0FBQ0FzQyxZQUFRLENBQUMzRixTQUFULENBQW1CQyxHQUFuQixDQUF1QixpQkFBdkI7QUFDQWdELFdBQU8sQ0FBQ00sV0FBUixDQUFxQm9DLFFBQXJCLEVBcEVKLENBdUVJOztBQUVBLFVBQU1DLFVBQVUsR0FBRyxDQUNmO0FBQUUxRCxPQUFDLEVBQUUsYUFBTDtBQUFvQnlCLE9BQUMsRUFBRTtBQUF2QixLQURlLENBQW5COztBQUdBLFFBQUlLLE9BQUosRUFBYTtBQUNUNEIsZ0JBQVUsQ0FBQ0MsSUFBWCxDQUFpQjtBQUFFM0QsU0FBQyxFQUFHLFlBQU47QUFBb0J5QixTQUFDLEVBQUc7QUFBeEIsT0FBakI7QUFDSDs7QUFDRCxTQUFLLE1BQU0sQ0FBQ3pCLENBQUQsRUFBSXlCLENBQUosQ0FBWCxJQUFxQnJELElBQUksQ0FBQzZELE9BQUwsRUFBckIsRUFBcUM7QUFDakMsVUFBSWpDLENBQUMsSUFBSWdDLEdBQVQsRUFBZTBCLFVBQVUsQ0FBQ0MsSUFBWCxDQUFnQjtBQUFFM0QsU0FBQyxFQUFFQSxDQUFMO0FBQVF5QixTQUFDLEVBQUV6QjtBQUFYLE9BQWhCO0FBQ2xCOztBQUVEYyxlQUFXLENBQ1AyQyxRQURPLEVBRVAsY0FGTyxFQUdQQyxVQUhPLEVBSVAsVUFBU2pFLENBQVQsRUFBVztBQUNQLGNBQVFBLENBQUMsQ0FBQ0gsTUFBRixDQUFTYSxLQUFqQjtBQUNJLGFBQUssTUFBTDtBQUNJcUMsYUFBRyxDQUFDaEMsTUFBSixDQUFZLHdCQUFaLEVBQXVDckIsTUFBdkM7QUFDSjs7QUFDQSxhQUFLLFVBQUw7QUFDSSxjQUFJeUUsY0FBYyxHQUFHOUIsT0FBTyxDQUFDMEIsTUFBUixDQUFnQmxGLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxLQUFGLElBQVdvRSxRQUFYLElBQXVCckUsQ0FBQyxDQUFDQyxLQUFGLElBQVdrRSxRQUF2RCxDQUFyQjtBQUNBLGdCQUFNb0IsT0FBTyxHQUFHckIsR0FBRyxDQUFDL0IsTUFBSixDQUFXLE1BQVgsRUFDWEMsSUFEVyxDQUNOLE9BRE0sRUFDRyx1QkFESCxFQUVYQyxLQUZXLENBRUppRCxjQUZJLEVBR1hsRCxJQUhXLENBR04sR0FITSxFQUdEckQsRUFBRSxDQUFDdUQsSUFBSCxHQUNOTixDQURNLENBQ0hPLENBQUMsSUFBSVAsQ0FBQyxDQUFFTyxDQUFDLENBQUN0QyxLQUFKLENBREgsRUFFTmdDLENBRk0sQ0FFSE0sQ0FBQyxJQUFJTixDQUFDLENBQUVNLENBQUMsQ0FBRVQsUUFBRixDQUFILENBRkgsQ0FIQyxDQUFoQjtBQU9FRiw0QkFBa0IsQ0FBRTBELGNBQUYsRUFBa0J4RCxRQUFsQixFQUE0Qm9DLEdBQTVCLEVBQWlDbEMsQ0FBakMsRUFBb0NDLENBQXBDLENBQWxCO0FBQ047O0FBQ0E7QUFDSSxnQkFBTWtCLENBQUMsR0FBR3JELElBQUksQ0FBQzBGLEdBQUwsQ0FBU3JFLENBQUMsQ0FBQ0gsTUFBRixDQUFTYSxLQUFsQixDQUFWO0FBQ0EsZ0JBQU00RCxRQUFRLEdBQUd0QyxDQUFDLENBQUMrQixNQUFGLENBQVVsRixDQUFDLElBQUlBLENBQUMsQ0FBQ0MsS0FBRixJQUFXb0UsUUFBMUIsQ0FBakI7QUFDQXpDLDRCQUFrQixDQUFFNkQsUUFBRixFQUFZM0QsUUFBWixFQUFzQm9DLEdBQXRCLEVBQTJCbEMsQ0FBM0IsRUFBOEJDLENBQTlCLENBQWxCO0FBQ0o7QUFuQko7O0FBb0JDO0FBQ0osS0ExQk0sQ0FBWCxDQW5GSixDQWlISTs7QUFFQU8sZUFBVyxDQUNQMkMsUUFETyxFQUVQLE9BRk8sRUFHUCxDQUNJO0FBQUV6RCxPQUFDLEVBQUUsWUFBTDtBQUFtQnlCLE9BQUMsRUFBRTtBQUF0QixLQURKLEVBRUk7QUFBRXpCLE9BQUMsRUFBRSxlQUFMO0FBQXNCeUIsT0FBQyxFQUFFO0FBQXpCLEtBRkosRUFHSTtBQUFFekIsT0FBQyxFQUFFLHFCQUFMO0FBQTRCeUIsT0FBQyxFQUFFO0FBQS9CLEtBSEosQ0FITyxFQVFQLFVBQVNoQyxDQUFULEVBQVc7QUFDUCxZQUFNdUUsR0FBRyxHQUFHdkUsQ0FBQyxDQUFDSCxNQUFGLENBQVNhLEtBQXJCOztBQUNBLGNBQVE2RCxHQUFSO0FBQ0ksYUFBSyxHQUFMO0FBQ0ksY0FBSUMsR0FBRyxHQUFHMUcsTUFBTSxDQUFFNEMsS0FBSyxDQUFDLENBQUQsQ0FBTCxDQUFTNUIsS0FBWCxDQUFOLENBQXlCMkYsSUFBekIsRUFBVjtBQUNKOztBQUNBO0FBQ0ksY0FBSUQsR0FBRyxHQUFHMUcsTUFBTSxDQUFDa0YsUUFBRCxDQUFOLENBQWlCRyxRQUFqQixDQUEyQm9CLEdBQTNCLEVBQWdDLE1BQWhDLEVBQXlDRSxJQUF6QyxFQUFWO0FBQ0o7QUFOSjs7QUFRQS9ELFdBQUssQ0FBQ2dFLEtBQU4sQ0FBYTdGLENBQUMsSUFBSTtBQUNkLFlBQUlmLE1BQU0sQ0FBQ2UsQ0FBQyxDQUFDQyxLQUFILENBQU4sQ0FBZ0IyRixJQUFoQixNQUEwQkQsR0FBOUIsRUFBbUM7QUFDL0J0QixrQkFBUSxHQUFHckUsQ0FBQyxDQUFDQyxLQUFiO0FBQ0FpRSxhQUFHLENBQUNoQyxNQUFKLENBQVksY0FBWixFQUE2QnJCLE1BQTdCO0FBQ0FxRCxhQUFHLENBQUNoQyxNQUFKLENBQVksd0JBQVosRUFBdUNyQixNQUF2QztBQUNBbUIsV0FBQyxHQUFHakQsRUFBRSxDQUFDd0YsU0FBSCxHQUFlQyxNQUFmLENBQXNCLENBQ3RCSCxRQURzQixFQUV0QkYsUUFGc0IsQ0FBdEIsRUFHRE0sS0FIQyxDQUdLLENBQUUsQ0FBRixFQUFLWixLQUFMLENBSEwsQ0FBSjtBQUlBSyxhQUFHLENBQUNoQyxNQUFKLENBQVksV0FBWixFQUEwQndDLElBQTFCLENBQ0kzRixFQUFFLENBQUM0RixVQUFILENBQWMzQyxDQUFkLEVBQWlCNEMsS0FBakIsQ0FBdUIsQ0FBdkIsRUFBMEJDLFVBQTFCLENBQXNDdEMsQ0FBQyxJQUFJO0FBQ3ZDLG1CQUFPdEQsTUFBTSxDQUFDc0QsQ0FBRCxDQUFOLENBQVV1QyxNQUFWLENBQWlCLGFBQWpCLENBQVA7QUFDSCxXQUZELENBREo7QUFLQSxnQkFBTWdCLFVBQVUsR0FBR2pFLEtBQUssQ0FBQ3FELE1BQU4sQ0FBY2xGLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxLQUFGLElBQVdvRSxRQUE5QixDQUFuQjtBQUNBL0IsY0FBSSxHQUFHNEIsR0FBRyxDQUFDL0IsTUFBSixDQUFXLE1BQVgsRUFDRkMsSUFERSxDQUNHLE9BREgsRUFDWSxhQURaLEVBRUZDLEtBRkUsQ0FFS3lELFVBRkwsRUFHRjFELElBSEUsQ0FHRyxHQUhILEVBR1FyRCxFQUFFLENBQUN1RCxJQUFILEdBQ05OLENBRE0sQ0FDSE8sQ0FBQyxJQUFJUCxDQUFDLENBQUVPLENBQUMsQ0FBQ3RDLEtBQUosQ0FESCxFQUVOZ0MsQ0FGTSxDQUVITSxDQUFDLElBQUlOLENBQUMsQ0FBRU0sQ0FBQyxDQUFFVCxRQUFGLENBQUgsQ0FGSCxDQUhSLENBQVA7QUFPQSxpQkFBTyxLQUFQO0FBQ0g7O0FBQ0QsZUFBTyxJQUFQO0FBQ0gsT0F6QkQ7QUEwQkgsS0E1Q00sQ0FBWDtBQStDQTtBQUNSO0FBQ0E7O0FBQ1EsUUFBSWlFLEtBQUssR0FBRzdCLEdBQUcsQ0FBQy9CLE1BQUosQ0FBVyxHQUFYLENBQVo7QUFDQTRELFNBQUssQ0FBQzNELElBQU4sQ0FBVyxPQUFYLEVBQW9CLGNBQXBCO0FBQ0EyRCxTQUFLLENBQUM1RCxNQUFOLENBQWEsUUFBYixFQUNHQyxJQURILENBQ1EsT0FEUixFQUNrQixPQUFNTixRQUFTLElBQUc0QixHQUFJLEVBRHhDLEVBRUdzQyxLQUZILENBRVMsTUFGVCxFQUVpQixhQUZqQixFQUdHQSxLQUhILENBR1MsUUFIVCxFQUdtQixhQUhuQixFQUlHNUQsSUFKSCxDQUlRLEdBSlIsRUFJYSxDQUpiO0FBS0EyRCxTQUFLLENBQUM1RCxNQUFOLENBQWEsTUFBYixFQUNHQyxJQURILENBQ1EsT0FEUixFQUNrQixTQUFRTixRQUFTLElBQUc0QixHQUFJLEVBRDFDLEVBRUd0QixJQUZILENBRVEsR0FGUixFQUVheUIsS0FGYixFQUdHekIsSUFISCxDQUdRLEdBSFIsRUFHYSxFQUhiLEVBSUc0RCxLQUpILENBSVMsYUFKVCxFQUl3QixLQUp4QixFQUtHQSxLQUxILENBS1MsTUFMVCxFQUtpQixhQUxqQixFQU1HQSxLQU5ILENBTVMsUUFOVCxFQU1tQixhQU5uQixFQU9HQSxLQVBILENBT1MsYUFQVCxFQU93QixNQVB4QjtBQVFBRCxTQUFLLENBQUM1RCxNQUFOLENBQWEsTUFBYixFQUNHQyxJQURILENBQ1EsT0FEUixFQUNrQixRQUFPTixRQUFTLElBQUc0QixHQUFJLEVBRHpDLEVBRUd0QixJQUZILENBRVEsR0FGUixFQUVheUIsS0FGYixFQUdHekIsSUFISCxDQUdRLEdBSFIsRUFHYSxDQUhiLEVBSUc0RCxLQUpILENBSVMsYUFKVCxFQUl3QixLQUp4QixFQUtHQSxLQUxILENBS1MsTUFMVCxFQUtpQixhQUxqQixFQU1HQSxLQU5ILENBTVMsUUFOVCxFQU1tQixhQU5uQjtBQU9BOUIsT0FBRyxDQUFDL0IsTUFBSixDQUFXLE1BQVgsRUFDS0MsSUFETCxDQUNVLE9BRFYsRUFDbUJ5QixLQURuQixFQUVLekIsSUFGTCxDQUVVLFFBRlYsRUFFb0IyQixNQUZwQixFQUdLM0IsSUFITCxDQUdVLE1BSFYsRUFHa0IsYUFIbEIsRUFJSzZELEVBSkwsQ0FJUSxXQUpSLEVBSXFCLENBQUM5RSxDQUFELEVBQUlvQixDQUFKLEtBQVU7QUFDdkIsWUFBTTJELElBQUksR0FBR2xFLENBQUMsQ0FBQ21FLE1BQUYsQ0FBVXBILEVBQUUsQ0FBQ3FILE9BQUgsQ0FBV2pGLENBQVgsRUFBYyxDQUFkLENBQVYsQ0FBYjtBQUNBLFVBQUlrRixHQUFHLEdBQUd4RSxLQUFLLENBQUN5RSxHQUFOLENBQVd0RyxDQUFDLElBQUl1RyxJQUFJLENBQUNDLEdBQUwsQ0FBU3hHLENBQUMsQ0FBQ0MsS0FBRixHQUFVaUcsSUFBbkIsQ0FBaEIsQ0FBVjtBQUNBLFVBQUlPLE9BQU8sR0FBR0osR0FBRyxDQUFDSyxPQUFKLENBQWFILElBQUksQ0FBQ2xELEdBQUwsQ0FBUyxHQUFHZ0QsR0FBWixDQUFiLENBQWQ7QUFDQSxZQUFNWCxHQUFHLEdBQUc3RCxLQUFLLENBQUM0RSxPQUFELENBQUwsQ0FBZTNFLFFBQWYsQ0FBWjtBQUNBL0MsUUFBRSxDQUFDbUQsTUFBSCxDQUFXLFFBQU9KLFFBQVMsSUFBRzRCLEdBQUksRUFBbEMsRUFDR3NDLEtBREgsQ0FDUyxRQURULEVBQ21CLEtBRG5CLEVBRUc1RCxJQUZILENBRVEsSUFGUixFQUVjSixDQUFDLENBQUVILEtBQUssQ0FBQzRFLE9BQUQsQ0FBTCxDQUFleEcsS0FBakIsQ0FGZixFQUdHbUMsSUFISCxDQUdRLElBSFIsRUFHY0gsQ0FBQyxDQUFFeUQsR0FBRixDQUhmO0FBSUEzRyxRQUFFLENBQUNtRCxNQUFILENBQVcsY0FBYUosUUFBUyxJQUFHNEIsR0FBSSxFQUF4QyxFQUNHc0MsS0FESCxDQUNTLE1BRFQsRUFDaUIsS0FEakIsRUFFR1csSUFGSCxDQUVTakIsR0FBRyxDQUFDa0IsT0FBSixDQUFZLENBQVosSUFBaUIsR0FBakIsR0FBdUJyRCxNQUZoQztBQUdBeEUsUUFBRSxDQUFDbUQsTUFBSCxDQUFXLGFBQVlKLFFBQVMsSUFBRzRCLEdBQUksRUFBdkMsRUFDR3NDLEtBREgsQ0FDUyxNQURULEVBQ2lCLEtBRGpCLEVBRUdXLElBRkgsQ0FFVzlFLEtBQUssQ0FBQzRFLE9BQUQsQ0FBTCxDQUFleEcsS0FBZixDQUFxQjRHLGtCQUFyQixDQUF3QyxPQUF4QyxFQUFpRDtBQUNwREMsWUFBSSxFQUFFLFNBRDhDO0FBRXBEQyxjQUFNLEVBQUM7QUFGNkMsT0FBakQsQ0FGWDtBQU1ILEtBdEJMLEVBdUJLZCxFQXZCTCxDQXVCUSxZQXZCUixFQXVCc0IsQ0FBQzlFLENBQUQsRUFBSW9CLENBQUosS0FBVTtBQUN4QnhELFFBQUUsQ0FBQ21ELE1BQUgsQ0FBVyxRQUFPSixRQUFTLElBQUc0QixHQUFJLEVBQWxDLEVBQXFDc0MsS0FBckMsQ0FBMkMsUUFBM0MsRUFBcUQsYUFBckQ7QUFDQWpILFFBQUUsQ0FBQ21ELE1BQUgsQ0FBVyxVQUFTSixRQUFTLElBQUc0QixHQUFJLEVBQXBDLEVBQXVDc0MsS0FBdkMsQ0FBNkMsTUFBN0MsRUFBcUQsYUFBckQ7QUFDQWpILFFBQUUsQ0FBQ21ELE1BQUgsQ0FBVyxTQUFRSixRQUFTLElBQUc0QixHQUFJLEVBQW5DLEVBQXNDc0MsS0FBdEMsQ0FBNEMsTUFBNUMsRUFBb0QsYUFBcEQ7QUFDSCxLQTNCTDtBQTRCSDs7QUFBQTtBQUNKLENBNU5ELEMiLCJmaWxlIjoiY2hhcnRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gaW5zdGFsbCBhIEpTT05QIGNhbGxiYWNrIGZvciBjaHVuayBsb2FkaW5nXG4gXHRmdW5jdGlvbiB3ZWJwYWNrSnNvbnBDYWxsYmFjayhkYXRhKSB7XG4gXHRcdHZhciBjaHVua0lkcyA9IGRhdGFbMF07XG4gXHRcdHZhciBtb3JlTW9kdWxlcyA9IGRhdGFbMV07XG4gXHRcdHZhciBleGVjdXRlTW9kdWxlcyA9IGRhdGFbMl07XG5cbiBcdFx0Ly8gYWRkIFwibW9yZU1vZHVsZXNcIiB0byB0aGUgbW9kdWxlcyBvYmplY3QsXG4gXHRcdC8vIHRoZW4gZmxhZyBhbGwgXCJjaHVua0lkc1wiIGFzIGxvYWRlZCBhbmQgZmlyZSBjYWxsYmFja1xuIFx0XHR2YXIgbW9kdWxlSWQsIGNodW5rSWQsIGkgPSAwLCByZXNvbHZlcyA9IFtdO1xuIFx0XHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoaW5zdGFsbGVkQ2h1bmtzLCBjaHVua0lkKSAmJiBpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcbiBcdFx0XHRcdHJlc29sdmVzLnB1c2goaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdWzBdKTtcbiBcdFx0XHR9XG4gXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gMDtcbiBcdFx0fVxuIFx0XHRmb3IobW9kdWxlSWQgaW4gbW9yZU1vZHVsZXMpIHtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobW9yZU1vZHVsZXMsIG1vZHVsZUlkKSkge1xuIFx0XHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0gPSBtb3JlTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdGlmKHBhcmVudEpzb25wRnVuY3Rpb24pIHBhcmVudEpzb25wRnVuY3Rpb24oZGF0YSk7XG5cbiBcdFx0d2hpbGUocmVzb2x2ZXMubGVuZ3RoKSB7XG4gXHRcdFx0cmVzb2x2ZXMuc2hpZnQoKSgpO1xuIFx0XHR9XG5cbiBcdFx0Ly8gYWRkIGVudHJ5IG1vZHVsZXMgZnJvbSBsb2FkZWQgY2h1bmsgdG8gZGVmZXJyZWQgbGlzdFxuIFx0XHRkZWZlcnJlZE1vZHVsZXMucHVzaC5hcHBseShkZWZlcnJlZE1vZHVsZXMsIGV4ZWN1dGVNb2R1bGVzIHx8IFtdKTtcblxuIFx0XHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIGFsbCBjaHVua3MgcmVhZHlcbiBcdFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4gXHR9O1xuIFx0ZnVuY3Rpb24gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKSB7XG4gXHRcdHZhciByZXN1bHQ7XG4gXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBkZWZlcnJlZE1vZHVsZXMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHR2YXIgZGVmZXJyZWRNb2R1bGUgPSBkZWZlcnJlZE1vZHVsZXNbaV07XG4gXHRcdFx0dmFyIGZ1bGZpbGxlZCA9IHRydWU7XG4gXHRcdFx0Zm9yKHZhciBqID0gMTsgaiA8IGRlZmVycmVkTW9kdWxlLmxlbmd0aDsgaisrKSB7XG4gXHRcdFx0XHR2YXIgZGVwSWQgPSBkZWZlcnJlZE1vZHVsZVtqXTtcbiBcdFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tkZXBJZF0gIT09IDApIGZ1bGZpbGxlZCA9IGZhbHNlO1xuIFx0XHRcdH1cbiBcdFx0XHRpZihmdWxmaWxsZWQpIHtcbiBcdFx0XHRcdGRlZmVycmVkTW9kdWxlcy5zcGxpY2UoaS0tLCAxKTtcbiBcdFx0XHRcdHJlc3VsdCA9IF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gZGVmZXJyZWRNb2R1bGVbMF0pO1xuIFx0XHRcdH1cbiBcdFx0fVxuXG4gXHRcdHJldHVybiByZXN1bHQ7XG4gXHR9XG5cbiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIG9iamVjdCB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4gXHQvLyB1bmRlZmluZWQgPSBjaHVuayBub3QgbG9hZGVkLCBudWxsID0gY2h1bmsgcHJlbG9hZGVkL3ByZWZldGNoZWRcbiBcdC8vIFByb21pc2UgPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG4gXHR2YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuIFx0XHRcImNoYXJ0c1wiOiAwXG4gXHR9O1xuXG4gXHR2YXIgZGVmZXJyZWRNb2R1bGVzID0gW107XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL1wiO1xuXG4gXHR2YXIganNvbnBBcnJheSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSB8fCBbXTtcbiBcdHZhciBvbGRKc29ucEZ1bmN0aW9uID0ganNvbnBBcnJheS5wdXNoLmJpbmQoanNvbnBBcnJheSk7XG4gXHRqc29ucEFycmF5LnB1c2ggPSB3ZWJwYWNrSnNvbnBDYWxsYmFjaztcbiBcdGpzb25wQXJyYXkgPSBqc29ucEFycmF5LnNsaWNlKCk7XG4gXHRmb3IodmFyIGkgPSAwOyBpIDwganNvbnBBcnJheS5sZW5ndGg7IGkrKykgd2VicGFja0pzb25wQ2FsbGJhY2soanNvbnBBcnJheVtpXSk7XG4gXHR2YXIgcGFyZW50SnNvbnBGdW5jdGlvbiA9IG9sZEpzb25wRnVuY3Rpb247XG5cblxuIFx0Ly8gYWRkIGVudHJ5IG1vZHVsZSB0byBkZWZlcnJlZCBsaXN0XG4gXHRkZWZlcnJlZE1vZHVsZXMucHVzaChbXCIuL3NyYy9pbmRleC5qc1wiLFwidmVuZG9yc35jaGFydHNcIl0pO1xuIFx0Ly8gcnVuIGRlZmVycmVkIG1vZHVsZXMgd2hlbiByZWFkeVxuIFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4iLCJ2YXIgbWFwID0ge1xuXHRcIi4vYWZcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FmLmpzXCIsXG5cdFwiLi9hZi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYWYuanNcIixcblx0XCIuL2FyXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci5qc1wiLFxuXHRcIi4vYXItZHpcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLWR6LmpzXCIsXG5cdFwiLi9hci1kei5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXItZHouanNcIixcblx0XCIuL2FyLWt3XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci1rdy5qc1wiLFxuXHRcIi4vYXIta3cuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLWt3LmpzXCIsXG5cdFwiLi9hci1seVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXItbHkuanNcIixcblx0XCIuL2FyLWx5LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci1seS5qc1wiLFxuXHRcIi4vYXItbWFcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLW1hLmpzXCIsXG5cdFwiLi9hci1tYS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXItbWEuanNcIixcblx0XCIuL2FyLXNhXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci1zYS5qc1wiLFxuXHRcIi4vYXItc2EuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLXNhLmpzXCIsXG5cdFwiLi9hci10blwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXItdG4uanNcIixcblx0XCIuL2FyLXRuLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci10bi5qc1wiLFxuXHRcIi4vYXIuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLmpzXCIsXG5cdFwiLi9helwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXouanNcIixcblx0XCIuL2F6LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hei5qc1wiLFxuXHRcIi4vYmVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JlLmpzXCIsXG5cdFwiLi9iZS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYmUuanNcIixcblx0XCIuL2JnXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9iZy5qc1wiLFxuXHRcIi4vYmcuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JnLmpzXCIsXG5cdFwiLi9ibVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYm0uanNcIixcblx0XCIuL2JtLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ibS5qc1wiLFxuXHRcIi4vYm5cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JuLmpzXCIsXG5cdFwiLi9ibi1iZFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYm4tYmQuanNcIixcblx0XCIuL2JuLWJkLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ibi1iZC5qc1wiLFxuXHRcIi4vYm4uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JuLmpzXCIsXG5cdFwiLi9ib1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYm8uanNcIixcblx0XCIuL2JvLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9iby5qc1wiLFxuXHRcIi4vYnJcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JyLmpzXCIsXG5cdFwiLi9ici5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYnIuanNcIixcblx0XCIuL2JzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9icy5qc1wiLFxuXHRcIi4vYnMuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JzLmpzXCIsXG5cdFwiLi9jYVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvY2EuanNcIixcblx0XCIuL2NhLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9jYS5qc1wiLFxuXHRcIi4vY3NcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2NzLmpzXCIsXG5cdFwiLi9jcy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvY3MuanNcIixcblx0XCIuL2N2XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9jdi5qc1wiLFxuXHRcIi4vY3YuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2N2LmpzXCIsXG5cdFwiLi9jeVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvY3kuanNcIixcblx0XCIuL2N5LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9jeS5qc1wiLFxuXHRcIi4vZGFcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2RhLmpzXCIsXG5cdFwiLi9kYS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZGEuanNcIixcblx0XCIuL2RlXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9kZS5qc1wiLFxuXHRcIi4vZGUtYXRcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2RlLWF0LmpzXCIsXG5cdFwiLi9kZS1hdC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZGUtYXQuanNcIixcblx0XCIuL2RlLWNoXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9kZS1jaC5qc1wiLFxuXHRcIi4vZGUtY2guanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2RlLWNoLmpzXCIsXG5cdFwiLi9kZS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZGUuanNcIixcblx0XCIuL2R2XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9kdi5qc1wiLFxuXHRcIi4vZHYuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2R2LmpzXCIsXG5cdFwiLi9lbFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZWwuanNcIixcblx0XCIuL2VsLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbC5qc1wiLFxuXHRcIi4vZW4tYXVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWF1LmpzXCIsXG5cdFwiLi9lbi1hdS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4tYXUuanNcIixcblx0XCIuL2VuLWNhXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1jYS5qc1wiLFxuXHRcIi4vZW4tY2EuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWNhLmpzXCIsXG5cdFwiLi9lbi1nYlwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4tZ2IuanNcIixcblx0XCIuL2VuLWdiLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1nYi5qc1wiLFxuXHRcIi4vZW4taWVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWllLmpzXCIsXG5cdFwiLi9lbi1pZS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4taWUuanNcIixcblx0XCIuL2VuLWlsXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1pbC5qc1wiLFxuXHRcIi4vZW4taWwuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWlsLmpzXCIsXG5cdFwiLi9lbi1pblwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4taW4uanNcIixcblx0XCIuL2VuLWluLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1pbi5qc1wiLFxuXHRcIi4vZW4tbnpcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLW56LmpzXCIsXG5cdFwiLi9lbi1uei5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4tbnouanNcIixcblx0XCIuL2VuLXNnXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1zZy5qc1wiLFxuXHRcIi4vZW4tc2cuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLXNnLmpzXCIsXG5cdFwiLi9lb1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW8uanNcIixcblx0XCIuL2VvLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lby5qc1wiLFxuXHRcIi4vZXNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VzLmpzXCIsXG5cdFwiLi9lcy1kb1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXMtZG8uanNcIixcblx0XCIuL2VzLWRvLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lcy1kby5qc1wiLFxuXHRcIi4vZXMtbXhcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VzLW14LmpzXCIsXG5cdFwiLi9lcy1teC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXMtbXguanNcIixcblx0XCIuL2VzLXVzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lcy11cy5qc1wiLFxuXHRcIi4vZXMtdXMuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VzLXVzLmpzXCIsXG5cdFwiLi9lcy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXMuanNcIixcblx0XCIuL2V0XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ldC5qc1wiLFxuXHRcIi4vZXQuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2V0LmpzXCIsXG5cdFwiLi9ldVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXUuanNcIixcblx0XCIuL2V1LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ldS5qc1wiLFxuXHRcIi4vZmFcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZhLmpzXCIsXG5cdFwiLi9mYS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZmEuanNcIixcblx0XCIuL2ZpXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9maS5qc1wiLFxuXHRcIi4vZmkuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZpLmpzXCIsXG5cdFwiLi9maWxcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZpbC5qc1wiLFxuXHRcIi4vZmlsLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9maWwuanNcIixcblx0XCIuL2ZvXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9mby5qc1wiLFxuXHRcIi4vZm8uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZvLmpzXCIsXG5cdFwiLi9mclwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZnIuanNcIixcblx0XCIuL2ZyLWNhXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9mci1jYS5qc1wiLFxuXHRcIi4vZnItY2EuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZyLWNhLmpzXCIsXG5cdFwiLi9mci1jaFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZnItY2guanNcIixcblx0XCIuL2ZyLWNoLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9mci1jaC5qc1wiLFxuXHRcIi4vZnIuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZyLmpzXCIsXG5cdFwiLi9meVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZnkuanNcIixcblx0XCIuL2Z5LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9meS5qc1wiLFxuXHRcIi4vZ2FcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2dhLmpzXCIsXG5cdFwiLi9nYS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ2EuanNcIixcblx0XCIuL2dkXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9nZC5qc1wiLFxuXHRcIi4vZ2QuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2dkLmpzXCIsXG5cdFwiLi9nbFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ2wuanNcIixcblx0XCIuL2dsLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9nbC5qc1wiLFxuXHRcIi4vZ29tLWRldmFcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2dvbS1kZXZhLmpzXCIsXG5cdFwiLi9nb20tZGV2YS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ29tLWRldmEuanNcIixcblx0XCIuL2dvbS1sYXRuXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9nb20tbGF0bi5qc1wiLFxuXHRcIi4vZ29tLWxhdG4uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2dvbS1sYXRuLmpzXCIsXG5cdFwiLi9ndVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ3UuanNcIixcblx0XCIuL2d1LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ndS5qc1wiLFxuXHRcIi4vaGVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2hlLmpzXCIsXG5cdFwiLi9oZS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaGUuanNcIixcblx0XCIuL2hpXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9oaS5qc1wiLFxuXHRcIi4vaGkuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2hpLmpzXCIsXG5cdFwiLi9oclwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaHIuanNcIixcblx0XCIuL2hyLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9oci5qc1wiLFxuXHRcIi4vaHVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2h1LmpzXCIsXG5cdFwiLi9odS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaHUuanNcIixcblx0XCIuL2h5LWFtXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9oeS1hbS5qc1wiLFxuXHRcIi4vaHktYW0uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2h5LWFtLmpzXCIsXG5cdFwiLi9pZFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaWQuanNcIixcblx0XCIuL2lkLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9pZC5qc1wiLFxuXHRcIi4vaXNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2lzLmpzXCIsXG5cdFwiLi9pcy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaXMuanNcIixcblx0XCIuL2l0XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9pdC5qc1wiLFxuXHRcIi4vaXQtY2hcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2l0LWNoLmpzXCIsXG5cdFwiLi9pdC1jaC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaXQtY2guanNcIixcblx0XCIuL2l0LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9pdC5qc1wiLFxuXHRcIi4vamFcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2phLmpzXCIsXG5cdFwiLi9qYS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvamEuanNcIixcblx0XCIuL2p2XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9qdi5qc1wiLFxuXHRcIi4vanYuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2p2LmpzXCIsXG5cdFwiLi9rYVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva2EuanNcIixcblx0XCIuL2thLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9rYS5qc1wiLFxuXHRcIi4va2tcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2trLmpzXCIsXG5cdFwiLi9ray5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva2suanNcIixcblx0XCIuL2ttXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9rbS5qc1wiLFxuXHRcIi4va20uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ttLmpzXCIsXG5cdFwiLi9rblwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva24uanNcIixcblx0XCIuL2tuLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9rbi5qc1wiLFxuXHRcIi4va29cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2tvLmpzXCIsXG5cdFwiLi9rby5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva28uanNcIixcblx0XCIuL2t1XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9rdS5qc1wiLFxuXHRcIi4va3UuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2t1LmpzXCIsXG5cdFwiLi9reVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva3kuanNcIixcblx0XCIuL2t5LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9reS5qc1wiLFxuXHRcIi4vbGJcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2xiLmpzXCIsXG5cdFwiLi9sYi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbGIuanNcIixcblx0XCIuL2xvXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9sby5qc1wiLFxuXHRcIi4vbG8uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2xvLmpzXCIsXG5cdFwiLi9sdFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbHQuanNcIixcblx0XCIuL2x0LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9sdC5qc1wiLFxuXHRcIi4vbHZcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2x2LmpzXCIsXG5cdFwiLi9sdi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbHYuanNcIixcblx0XCIuL21lXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tZS5qc1wiLFxuXHRcIi4vbWUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21lLmpzXCIsXG5cdFwiLi9taVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbWkuanNcIixcblx0XCIuL21pLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9taS5qc1wiLFxuXHRcIi4vbWtcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21rLmpzXCIsXG5cdFwiLi9tay5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbWsuanNcIixcblx0XCIuL21sXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tbC5qc1wiLFxuXHRcIi4vbWwuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21sLmpzXCIsXG5cdFwiLi9tblwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbW4uanNcIixcblx0XCIuL21uLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tbi5qc1wiLFxuXHRcIi4vbXJcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21yLmpzXCIsXG5cdFwiLi9tci5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbXIuanNcIixcblx0XCIuL21zXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tcy5qc1wiLFxuXHRcIi4vbXMtbXlcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21zLW15LmpzXCIsXG5cdFwiLi9tcy1teS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbXMtbXkuanNcIixcblx0XCIuL21zLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tcy5qc1wiLFxuXHRcIi4vbXRcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL210LmpzXCIsXG5cdFwiLi9tdC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbXQuanNcIixcblx0XCIuL215XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9teS5qc1wiLFxuXHRcIi4vbXkuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL215LmpzXCIsXG5cdFwiLi9uYlwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbmIuanNcIixcblx0XCIuL25iLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9uYi5qc1wiLFxuXHRcIi4vbmVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL25lLmpzXCIsXG5cdFwiLi9uZS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbmUuanNcIixcblx0XCIuL25sXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ubC5qc1wiLFxuXHRcIi4vbmwtYmVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL25sLWJlLmpzXCIsXG5cdFwiLi9ubC1iZS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbmwtYmUuanNcIixcblx0XCIuL25sLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ubC5qc1wiLFxuXHRcIi4vbm5cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL25uLmpzXCIsXG5cdFwiLi9ubi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbm4uanNcIixcblx0XCIuL29jLWxuY1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvb2MtbG5jLmpzXCIsXG5cdFwiLi9vYy1sbmMuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL29jLWxuYy5qc1wiLFxuXHRcIi4vcGEtaW5cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3BhLWluLmpzXCIsXG5cdFwiLi9wYS1pbi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcGEtaW4uanNcIixcblx0XCIuL3BsXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9wbC5qc1wiLFxuXHRcIi4vcGwuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3BsLmpzXCIsXG5cdFwiLi9wdFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcHQuanNcIixcblx0XCIuL3B0LWJyXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9wdC1ici5qc1wiLFxuXHRcIi4vcHQtYnIuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3B0LWJyLmpzXCIsXG5cdFwiLi9wdC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcHQuanNcIixcblx0XCIuL3JvXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9yby5qc1wiLFxuXHRcIi4vcm8uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3JvLmpzXCIsXG5cdFwiLi9ydVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcnUuanNcIixcblx0XCIuL3J1LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ydS5qc1wiLFxuXHRcIi4vc2RcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NkLmpzXCIsXG5cdFwiLi9zZC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc2QuanNcIixcblx0XCIuL3NlXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zZS5qc1wiLFxuXHRcIi4vc2UuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NlLmpzXCIsXG5cdFwiLi9zaVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc2kuanNcIixcblx0XCIuL3NpLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zaS5qc1wiLFxuXHRcIi4vc2tcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NrLmpzXCIsXG5cdFwiLi9zay5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc2suanNcIixcblx0XCIuL3NsXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zbC5qc1wiLFxuXHRcIi4vc2wuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NsLmpzXCIsXG5cdFwiLi9zcVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3EuanNcIixcblx0XCIuL3NxLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zcS5qc1wiLFxuXHRcIi4vc3JcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NyLmpzXCIsXG5cdFwiLi9zci1jeXJsXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zci1jeXJsLmpzXCIsXG5cdFwiLi9zci1jeXJsLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zci1jeXJsLmpzXCIsXG5cdFwiLi9zci5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3IuanNcIixcblx0XCIuL3NzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zcy5qc1wiLFxuXHRcIi4vc3MuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NzLmpzXCIsXG5cdFwiLi9zdlwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3YuanNcIixcblx0XCIuL3N2LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zdi5qc1wiLFxuXHRcIi4vc3dcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3N3LmpzXCIsXG5cdFwiLi9zdy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3cuanNcIixcblx0XCIuL3RhXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90YS5qc1wiLFxuXHRcIi4vdGEuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RhLmpzXCIsXG5cdFwiLi90ZVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGUuanNcIixcblx0XCIuL3RlLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90ZS5qc1wiLFxuXHRcIi4vdGV0XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90ZXQuanNcIixcblx0XCIuL3RldC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGV0LmpzXCIsXG5cdFwiLi90Z1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGcuanNcIixcblx0XCIuL3RnLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90Zy5qc1wiLFxuXHRcIi4vdGhcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RoLmpzXCIsXG5cdFwiLi90aC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGguanNcIixcblx0XCIuL3RrXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90ay5qc1wiLFxuXHRcIi4vdGsuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RrLmpzXCIsXG5cdFwiLi90bC1waFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGwtcGguanNcIixcblx0XCIuL3RsLXBoLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90bC1waC5qc1wiLFxuXHRcIi4vdGxoXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90bGguanNcIixcblx0XCIuL3RsaC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGxoLmpzXCIsXG5cdFwiLi90clwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdHIuanNcIixcblx0XCIuL3RyLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90ci5qc1wiLFxuXHRcIi4vdHpsXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90emwuanNcIixcblx0XCIuL3R6bC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdHpsLmpzXCIsXG5cdFwiLi90em1cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3R6bS5qc1wiLFxuXHRcIi4vdHptLWxhdG5cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3R6bS1sYXRuLmpzXCIsXG5cdFwiLi90em0tbGF0bi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdHptLWxhdG4uanNcIixcblx0XCIuL3R6bS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdHptLmpzXCIsXG5cdFwiLi91Zy1jblwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdWctY24uanNcIixcblx0XCIuL3VnLWNuLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS91Zy1jbi5qc1wiLFxuXHRcIi4vdWtcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3VrLmpzXCIsXG5cdFwiLi91ay5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdWsuanNcIixcblx0XCIuL3VyXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS91ci5qc1wiLFxuXHRcIi4vdXIuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3VyLmpzXCIsXG5cdFwiLi91elwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdXouanNcIixcblx0XCIuL3V6LWxhdG5cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3V6LWxhdG4uanNcIixcblx0XCIuL3V6LWxhdG4uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3V6LWxhdG4uanNcIixcblx0XCIuL3V6LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS91ei5qc1wiLFxuXHRcIi4vdmlcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3ZpLmpzXCIsXG5cdFwiLi92aS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdmkuanNcIixcblx0XCIuL3gtcHNldWRvXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS94LXBzZXVkby5qc1wiLFxuXHRcIi4veC1wc2V1ZG8uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3gtcHNldWRvLmpzXCIsXG5cdFwiLi95b1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUveW8uanNcIixcblx0XCIuL3lvLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS95by5qc1wiLFxuXHRcIi4vemgtY25cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3poLWNuLmpzXCIsXG5cdFwiLi96aC1jbi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvemgtY24uanNcIixcblx0XCIuL3poLWhrXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS96aC1oay5qc1wiLFxuXHRcIi4vemgtaGsuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3poLWhrLmpzXCIsXG5cdFwiLi96aC1tb1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvemgtbW8uanNcIixcblx0XCIuL3poLW1vLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS96aC1tby5qc1wiLFxuXHRcIi4vemgtdHdcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3poLXR3LmpzXCIsXG5cdFwiLi96aC10dy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvemgtdHcuanNcIlxufTtcblxuXG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dChyZXEpIHtcblx0dmFyIGlkID0gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSk7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKGlkKTtcbn1cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpIHtcblx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhtYXAsIHJlcSkpIHtcblx0XHR2YXIgZSA9IG5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIgKyByZXEgKyBcIidcIik7XG5cdFx0ZS5jb2RlID0gJ01PRFVMRV9OT1RfRk9VTkQnO1xuXHRcdHRocm93IGU7XG5cdH1cblx0cmV0dXJuIG1hcFtyZXFdO1xufVxud2VicGFja0NvbnRleHQua2V5cyA9IGZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0S2V5cygpIHtcblx0cmV0dXJuIE9iamVjdC5rZXlzKG1hcCk7XG59O1xud2VicGFja0NvbnRleHQucmVzb2x2ZSA9IHdlYnBhY2tDb250ZXh0UmVzb2x2ZTtcbm1vZHVsZS5leHBvcnRzID0gd2VicGFja0NvbnRleHQ7XG53ZWJwYWNrQ29udGV4dC5pZCA9IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZSBzeW5jIHJlY3Vyc2l2ZSBeXFxcXC5cXFxcLy4qJFwiOyIsIi8vIERlcGVuZGVuY2llc1xuXG5jb25zdCBkMyA9IHJlcXVpcmUoJ2QzJyk7XG5jb25zdCBtb21lbnQgPSByZXF1aXJlKCdtb21lbnQnKTtcblxuLy8gZGVmYXVsdCBuYW1lc3BhY2UgZm9yIFNWR3NcbmNvbnN0IE5TID0gXCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiO1xuXG4vLyBXaGVuIERPTSBpcyByZWFkeVxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZ1bmN0aW9uKCl7XG4gIFxuICAgbGV0IGZsb3dkYXRhLCBzZW5zb3JkYXRhO1xuICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QuYWRkKCdsb2FkaW5nJyk7XG4gICBcbiAgICAvLyBHZXQgZmxvdyBkYXRhIGFuZCBkaXNwbGF5IGl0IFxuICAgIGZldGNoKCcvYXBpL2Zsb3cvJykudGhlbiggcmVzcG9uc2UgPT4gcmVzcG9uc2UuanNvbigpICkudGhlbiggZGF0YSA9PiB7XG4gICAgICAgIC8vIEZvcm1hdCBkYXRhIHByb3Blcmx5XG4gICAgICAgIGRhdGEuZm9yRWFjaCggaSA9PiB7XG4gICAgICAgICAgICBpLmZkYXRlICA9IGQzLnRpbWVQYXJzZShcIiVtLyVkLyVZICVIOiVNOiVTXCIpKGkuZmRhdGUpO1xuICAgICAgICAgICAgaS5saXRlcnMgPSBwYXJzZUZsb2F0KCBpLmxpdGVycyApO1xuICAgICAgICB9KTtcbiAgICAgICAgLy8gR3JvdXAgZGF0YVxuICAgICAgICBmbG93ZGF0YSA9IGQzLmdyb3VwKCBkYXRhLCBpdGVtID0+IGl0ZW0ubmFtZSApO1xuICAgICAgICBhZGRfZXZvbHV0aW9uX2NoYXJ0cygnI2Zsb3djaGFydHMnLCBmbG93ZGF0YSwgJ2xpdGVycycsIDAsIDEsICdsLicpO1xuICAgICAgICAvLyBHZXQgc2Vuc29yIGRhdGEgYW5kIGRpc3BsYXkgaXRcbiAgICAgICAgZmV0Y2goJy9hcGkvc2Vuc29yJykudGhlbiggcmVzcG9uc2UgPT4gcmVzcG9uc2UuanNvbigpICkudGhlbiggZGF0YSA9PiB7XG4gICAgICAgICAgICAvLyBGb3JtYXQgZGF0YSBwcm9wZXJseVxuICAgICAgICAgICAgZGF0YS5mb3JFYWNoKCBpID0+IHtcbiAgICAgICAgICAgICAgICBpLmZkYXRlICA9IGQzLnRpbWVQYXJzZShcIiVtLyVkLyVZICVIOiVNOiVTXCIpKGkuZmRhdGUpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAvLyBHcm91cCBkYXRhXG4gICAgICAgICAgICBmZXRjaCgnL2FwaS93ZWF0aGVyJykudGhlbiggcmVzcG9uc2UgPT4gcmVzcG9uc2UuanNvbigpICkudGhlbiggYWVyb3BvcnRfZGF0YSA9PiB7XG4gICAgICAgICAgICAgICAgYWVyb3BvcnRfZGF0YS5mb3JFYWNoKCBpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaS5mZGF0ZSAgPSBkMy50aW1lUGFyc2UoXCIlbS8lZC8lWSAlSDolTTolU1wiKShpLmZkYXRlKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBzZW5zb3JkYXRhID0gZDMuZ3JvdXAoIGRhdGEsIGl0ZW0gPT4gaXRlbS5uYW1lICk7XG4gICAgICAgICAgICAgICAgYWRkX2V2b2x1dGlvbl9jaGFydHMoJyNzZW5zb3JjaGFydHNfdGVtcCcsIHNlbnNvcmRhdGEsICd0ZW1wZXJhdHVyZScsIDAsIDYwLCAnwrAnLCBhZXJvcG9ydF9kYXRhKTtcbiAgICAgICAgICAgICAgICAvLyBhZGRfZXZvbHV0aW9uX2NoYXJ0cygnI3NlbnNvcmNoYXJ0c19wcmVzcycsIHNlbnNvcmRhdGEsICdwcmVzc3VyZScsICdoUEEnLCAwLCAxNTAwKTtcbiAgICAgICAgICAgICAgICBhZGRfZXZvbHV0aW9uX2NoYXJ0cygnI3NlbnNvcmNoYXJ0c19odW0nLCBzZW5zb3JkYXRhLCAnbWFwcGVkX2h1bWlkaXR5JywgMCwgMTAwLCAnJScpO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jaGFydGdyb3VwJykuZm9yRWFjaCggaSA9PiB7IGkuY2xhc3NMaXN0LmFkZCgnaGlkZGVuJyk7IH0pO1xuICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNmbG93Y2hhcnRzJykucGFyZW50Tm9kZS5jbGFzc0xpc3QucmVtb3ZlKCdoaWRkZW4nKTtcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5yZW1vdmUoJ2xvYWRpbmcnKTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAvLyBPbiBvcmllbnRhdGlvbiBjaGFuZ2UsIHJlZHJhdyBldmVyeXRoaW5nXG4gICAgICAgICAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ29yaWVudGF0aW9uY2hhbmdlJywgZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2Zsb3djaGFydHMnKS5pbm5lckhUTUwgPSAnJztcbiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3NlbnNvcmNoYXJ0c190ZW1wJykuaW5uZXJIVE1MID0gJyc7XG4gICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNzZW5zb3JjaGFydHNfaHVtJykuaW5uZXJIVE1MID0gJyc7XG4gICAgICAgICAgICAgICAgICAgIGFkZF9ldm9sdXRpb25fY2hhcnRzKCcjZmxvd2NoYXJ0cycsIGZsb3dkYXRhLCAnbGl0ZXJzJywgMCwgMSwgJ2wuJyk7XG4gICAgICAgICAgICAgICAgICAgIGFkZF9ldm9sdXRpb25fY2hhcnRzKCcjc2Vuc29yY2hhcnRzX3RlbXAnLCBzZW5zb3JkYXRhLCAndGVtcGVyYXR1cmUnLCAwLCA2MCwgJ8KwJywgYWVyb3BvcnRfZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgIGFkZF9ldm9sdXRpb25fY2hhcnRzKCcjc2Vuc29yY2hhcnRzX2h1bScsIHNlbnNvcmRhdGEsICdtYXBwZWRfaHVtaWRpdHknLCAwLCAxMDAsICclJyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9ICk7XG4gICAgICAgIH0pXG4gICAgfSk7XG4gICAgXG4gICAgLy8gQWRkIGJhc2ljIGludGVyYWN0aW9uc1xuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jaGFydHMtbmF2X19pdGVtJykuZm9yRWFjaCggaSA9PiB7XG4gICAgICAgIHZhciB0YXJnZXQgPSBpLmRhdGFzZXQuc2hvdztcbiAgICAgICAgaS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGUgPT4ge1xuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmNoYXJ0Z3JvdXAnKS5mb3JFYWNoKCBpID0+IHsgaS5jbGFzc0xpc3QuYWRkKCdoaWRkZW4nKTsgfSk7XG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAjJHt0YXJnZXR9YCkucGFyZW50Tm9kZS5jbGFzc0xpc3QucmVtb3ZlKCdoaWRkZW4nKTtcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jaGFydHMtbmF2X19pdGVtJykuZm9yRWFjaCggaSA9PiB7IGkuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJykgfSApO1xuICAgICAgICAgICAgaS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcbiAgICAgICAgfSk7IFxuICAgIH0pO1xuICAgIFxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdvcmllbnRhdGlvbmNoYW5nZScsIGZ1bmN0aW9uKCl7XG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jaGFydGdyb3VwX19jb250YWluZXInKS5mb3JFYWNoKCBpID0+IHtcbiAgICAgICAgICAgIGkuaW5uZXJIVE1MID0gJyc7XG4gICAgICAgIH0gKTtcbiAgICB9KTtcbn0pO1xuXG5jb25zdCB3aGF0aXMgPSAoIGlkICkgPT4ge1xuICAgY29uc3QgcHJlZml4ZXMgPSB7XG4gICAgICAnYicgIDogJ0zDrW5lYSBkZSByaWVnbycsXG4gICAgICAndCcgIDogJ1NlbnNvciBkZSB0aWVycmEnLFxuICAgICAgJ2FhJyA6ICdBaXJlIGFjb25kaWNpb25hZG8nLFxuICAgICAgJ3InICA6ICdBcG9ydGUgZGUgcmVkJyxcbiAgICAgICdhdScgOiAnQXVsYSBleHRlcmlvcicsXG4gICAgICAnZScgIDogJ1NlbnNvciBleHRlcmlvciBkZSBmYWNoYWRhJ1xuICAgfTtcbiAgIGxldCBsYWJlbCA9ICcnO1xuICAgT2JqZWN0LmtleXMocHJlZml4ZXMpLmZvckVhY2goIGsgPT4ge1xuICAgICAgaWYoIGlkLnN0YXJ0c1dpdGgoaykgKXtcbiAgICAgICAgICBsYWJlbCA9IHByZWZpeGVzW2tdO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgIH0pO1xuICAgcmV0dXJuIGxhYmVsID8gbGFiZWwgOiBpZDtcbn1cblxuLyoqXG4gKiAgY3JlYXRlX2Zsb3dfY2hhcnRzXG4gKi8gXG5cbmNvbnN0IGFkZF9ldm9sdXRpb25fbGluZSA9ICggdmFsdWUsIGZpZWxka2V5LCBjaGFydCwgeCwgeSApID0+IHtcbiAgICBjaGFydC5zZWxlY3QoICcuY2hhcnRfX2xpbmUtLWNvbXBhcmVkJyApLnJlbW92ZSgpO1xuICAgIGNoYXJ0LmFwcGVuZCgncGF0aCcpXG4gICAgICAgIC5hdHRyKCdjbGFzcycsICdjaGFydF9fbGluZSBjaGFydF9fbGluZS0tY29tcGFyZWQnKVxuICAgICAgICAuZGF0dW0oIHZhbHVlIClcbiAgICAgICAgLmF0dHIoJ2QnLCBkMy5saW5lKClcbiAgICAgICAgICAgIC54KCBkID0+IHgoIGQuZmRhdGUgKSApXG4gICAgICAgICAgICAueSggZCA9PiB5KCBkWyBmaWVsZGtleSBdICkpXG4gICAgICAgICkuYXR0cignc3Ryb2tlLWRhc2hhcnJheScsICc0IDInKTtcbn07XG5cbmNvbnN0IGFkZF9jb250cm9sID0gKHdyYXBwZXIsIGxhYmVsLCBvcHRpb25zLCBjYWxsYmFjaykgPT4ge1xuICAgIGNvbnN0IGNvbnRyb2wgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICBjb250cm9sLmNsYXNzTGlzdC5hZGQoJ2NoYXJ0LWNvbnRyb2wnKTtcbiAgICBjb25zdCBjb250cm9sX2xhYmVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3BhbicpO1xuICAgIGNvbnRyb2xfbGFiZWwuY2xhc3NMaXN0LmFkZCgnY2hhcnQtY29udHJvbF9fbGFiZWwnKTtcbiAgICBjb250cm9sX2xhYmVsLmlubmVySFRNTCA9IGxhYmVsO1xuICAgIGNvbnRyb2wuYXBwZW5kQ2hpbGQoY29udHJvbF9sYWJlbCk7XG4gICAgY29uc3QgY29udHJvbF9pdGVtcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NlbGVjdCcpO1xuICAgIGNvbnRyb2xfaXRlbXMuY2xhc3NMaXN0LmFkZCgnY2hhcnQtY29udHJvbF9fc2VsZWN0Jyk7XG4gICAgb3B0aW9ucy5mb3JFYWNoKCBvcHRpb24gPT4ge1xuICAgICAgICBjb25zdCBjb250cm9sX2l0ZW0gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdvcHRpb24nKTtcbiAgICAgICAgY29udHJvbF9pdGVtLmNsYXNzTGlzdC5hZGQoJ2NoYXJ0LWNvbnRyb2xfX29wdGlvbicpO1xuICAgICAgICBjb250cm9sX2l0ZW0uaW5uZXJIVE1MID0gb3B0aW9uLms7XG4gICAgICAgIGNvbnRyb2xfaXRlbS52YWx1ZSAgICAgPSBvcHRpb24udjtcbiAgICAgICAgY29udHJvbF9pdGVtcy5hcHBlbmRDaGlsZCggY29udHJvbF9pdGVtICk7ICAgICBcbiAgICB9KTtcbiAgICBjb250cm9sX2l0ZW1zLmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsIGNhbGxiYWNrICk7XG4gICAgY29udHJvbC5hcHBlbmRDaGlsZCggY29udHJvbF9pdGVtcyApO1xuICAgIHdyYXBwZXIuYXBwZW5kQ2hpbGQoIGNvbnRyb2wgKTsgICAgICAgIFxufVxuXG5jb25zdCBhZGRfZXZvbHV0aW9uX2NoYXJ0cyA9ICggcGFyZW50bm9kZSwgZGF0YSwgZmllbGRrZXksIG1pbiwgbWF4LCBzdWZmaXgsIHJlZmRhdGEgKSA9PiB7XG4gICAgY29uc3QgZmxvd2NoYXJ0cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IocGFyZW50bm9kZSk7XG4gICAgXG4gICAgZm9yKCBjb25zdCBba2V5LCB2YWx1ZV0gb2YgZGF0YS5lbnRyaWVzKCkgKVxuICAgIHtcbiAgICAgICAgLy8gSWYgYWxsIHZhbHVlcyBhcmUgbmVnYXRpdmUgZG9uJ3QgcmVuZGVyIHRoZSBjaGFydFxuICAgICAgICBpZighdmFsdWUuc29tZSggaSA9PiBpW2ZpZWxka2V5XSA+IDApKSBjb250aW51ZTtcbiAgICAgICAgXG4gICAgICAgIC8vIEFkZCBodG1sIHdyYXBwZXIgdG8gRDMgY2hhcnQgYW5kIGl0cyBtYWluIGVsZW1lbnRzXG4gICAgICAgIGNvbnN0IHdyYXBwZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgd3JhcHBlci5jbGFzc0xpc3QuYWRkKFsnY2hhcnQnXSwgWydjaGFydC0tZmxvdyddKTtcbiAgICAgICAgY29uc3QgbGFiZWwgICA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2g0Jyk7XG4gICAgICAgIGxhYmVsLmNsYXNzTGlzdC5hZGQoJ2NoYXJ0X19sYWJlbCcpO1xuICAgICAgICBsYWJlbC5pbm5lckhUTUwgPSBgJHsgd2hhdGlzKGtleSkgfSA8c3BhbiBjbGFzcz1cImNoYXJ0X19sYWJlbC1rZXlcIj4ke2tleX08L3NwYW4+YDtcbiAgICAgICAgd3JhcHBlci5hcHBlbmRDaGlsZCggbGFiZWwgKTtcbiAgICAgICAgZmxvd2NoYXJ0cy5hcHBlbmRDaGlsZCggd3JhcHBlciApO1xuICAgICAgICBcbiAgICAgICAgLy8gQWRkIGNoYXJ0IHdyYXBwZXJcbiAgICAgICAgY29uc3Qgd2lkdGggID0gd3JhcHBlci5jbGllbnRXaWR0aDsgXG4gICAgICAgIGNvbnN0IGhlaWdodCA9IDIwMDtcbiAgICAgICAgY29uc3Qgc3ZnX25vZGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMoTlMsICdzdmcnKTtcbiAgICAgICAgc3ZnX25vZGUuY2xhc3NMaXN0LmFkZCgnY2hhcnRfX3N2ZycpO1xuICAgICAgICBzdmdfbm9kZS5pZCA9IGAke2ZpZWxka2V5fV8ke2tleX1gO1xuICAgICAgICB3cmFwcGVyLmFwcGVuZENoaWxkKCBzdmdfbm9kZSApO1xuICAgICAgICBcbiAgICAgICAgLy8gQ3JlYXRlIEQzIGNoYXJ0XG4gICAgICAgIGNvbnN0IHN2ZyA9IGQzLnNlbGVjdCggYCMke2ZpZWxka2V5fV8ke2tleX1gICk7XG4gICAgICAgIHN2Zy5hdHRyKCd3aWR0aCcsIHdpZHRoKS5hdHRyKCAnaGVpZ2h0JywgaGVpZ2h0ICk7XG4gICAgICAgIFxuICAgICAgICAvKipcbiAgICAgICAgICogQ3JlYXRlIGNoYXJ0IHNjYWxlcyBhbmQgYWRkIHRoZW0gdG8gY2hhcnRcbiAgICAgICAgICovXG4gICAgICAgICBcbiAgICAgICAgY29uc3QgbWF4X3RpbWUgPSB2YWx1ZVsgdmFsdWUubGVuZ3RoIC0gMSBdLmZkYXRlO1xuICAgICAgICBsZXQgbWluX3RpbWUgICA9IG1vbWVudChtYXhfdGltZSkuc3VidHJhY3QoIDMwLCAnZGF5cycgKTsgLy8gR2V0IGxhc3QgbW9udGggYnkgZGVmYXVsdFxuICAgICAgICBsZXQgeCA9IGQzLnNjYWxlVGltZSgpLmRvbWFpbihbIFxuICAgICAgICAgICAgbWluX3RpbWUsIFxuICAgICAgICAgICAgbWF4X3RpbWVcbiAgICAgICAgXSkucmFuZ2UoWyAwLCB3aWR0aCBdKTtcbiAgICAgICAgc3ZnLmFwcGVuZChcImdcIilcbiAgICAgICAgICAgIC5hdHRyKCdjbGFzcycsICdjaGFydF9feCcpXG4gICAgICAgICAgICAuYXR0cihcInRyYW5zZm9ybVwiLCBgdHJhbnNsYXRlKDAsICR7aGVpZ2h0fSlgKVxuICAgICAgICAgICAgLmNhbGwoIGQzLmF4aXNCb3R0b20oeCkudGlja3MoMykudGlja0Zvcm1hdCggZCA9PiB7IFxuICAgICAgICAgICAgICAgIHJldHVybiBtb21lbnQoZCkuZm9ybWF0KCdERC1NTSBoaDptbScpOyBcbiAgICAgICAgICAgIH0gKSk7XG4gICAgICAgIGxldCB5ID0gZDMuc2NhbGVMaW5lYXIoKVxuICAgICAgICAgICAgLmRvbWFpbihbbWluLCBtYXhdKS5yYW5nZShbIGhlaWdodCwgMCBdKTsgICAgXG4gICAgICAgIHN2Zy5hcHBlbmQoXCJnXCIpXG4gICAgICAgICAgICAuYXR0cignY2xhc3MnLCAnY2hhcnRfX3knKVxuICAgICAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgYHRyYW5zbGF0ZSgwLCAwKWApXG4gICAgICAgICAgICAuY2FsbCggZDMuYXhpc0xlZnQoeSkudGlja0Zvcm1hdCggZCA9PiBkICsgJyAnICsgc3VmZml4KSApXG4gICAgICAgICAgICAuc2VsZWN0QWxsKCd0ZXh0JylcbiAgICAgICAgICAgIC5hdHRyKCd4JywgJy0yMHB4Jyk7XG4gICAgICAgIFxuICAgICAgICAvKipcbiAgICAgICAgICogIEFkZCBtYWluIGV2b2x1dGlvbiBsaW5lXG4gICAgICAgICAqL1xuICAgICAgICAgXG4gICAgICAgIGxldCBsaW5lID0gc3ZnLmFwcGVuZCgncGF0aCcpXG4gICAgICAgICAgICAuYXR0cignY2xhc3MnLCAnY2hhcnRfX2xpbmUnKVxuICAgICAgICAgICAgLmRhdHVtKCB2YWx1ZS5maWx0ZXIoIGkgPT4gaS5mZGF0ZSA+IG1pbl90aW1lICkgKVxuICAgICAgICAgICAgLmF0dHIoJ2QnLCBkMy5saW5lKClcbiAgICAgICAgICAgICAgICAueCggZCA9PiB4KCBkLmZkYXRlICkgKVxuICAgICAgICAgICAgICAgIC55KCBkID0+IHkoIGRbIGZpZWxka2V5IF0gKSlcbiAgICAgICAgICAgICk7XG4gICAgICAgIFxuICAgICAgICAvKipcbiAgICAgICAgICogIEFkZCBjb250cm9sc1xuICAgICAgICAgKi9cbiAgICAgICAgICAgICAgICBcbiAgICAgICAgY29uc3QgY29udHJvbHMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgY29udHJvbHMuY2xhc3NMaXN0LmFkZCgnY2hhcnRfX2NvbnRyb2xzJyk7XG4gICAgICAgIHdyYXBwZXIuYXBwZW5kQ2hpbGQoIGNvbnRyb2xzICk7XG4gICAgICAgIFxuICAgICAgICBcbiAgICAgICAgLy8gQWRkIGNvbXBhcmF0b3JcbiAgICAgICAgIFxuICAgICAgICBjb25zdCBjb21wYXJhYmxlID0gW1xuICAgICAgICAgICAgeyBrOiAnTm8gY29tcGFyYXInLCB2OiAnbm9uZScgfVxuICAgICAgICBdO1xuICAgICAgICBpZiggcmVmZGF0YSApe1xuICAgICAgICAgICAgY29tcGFyYWJsZS5wdXNoKCB7IGsgOiAnQWVyb3B1ZXJ0bycsIHYgOiAnYWVyb3BvcnQnIH0pXG4gICAgICAgIH1cbiAgICAgICAgZm9yKCBjb25zdCBbaywgdl0gb2YgZGF0YS5lbnRyaWVzKCkgKXtcbiAgICAgICAgICAgIGlmKCBrICE9IGtleSApIGNvbXBhcmFibGUucHVzaCh7IGs6IGssIHY6IGsgfSlcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgYWRkX2NvbnRyb2woXG4gICAgICAgICAgICBjb250cm9scywgXG4gICAgICAgICAgICAnQ29tcGFyYXIgY29uJywgXG4gICAgICAgICAgICBjb21wYXJhYmxlLFxuICAgICAgICAgICAgZnVuY3Rpb24oZSl7IFxuICAgICAgICAgICAgICAgIHN3aXRjaCggZS50YXJnZXQudmFsdWUgKXtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnbm9uZSc6XG4gICAgICAgICAgICAgICAgICAgICAgICBzdmcuc2VsZWN0KCAnLmNoYXJ0X19saW5lLS1jb21wYXJlZCcgKS5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2Flcm9wb3J0JzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCByZWZkYXRhX3B1cmdlZCA9IHJlZmRhdGEuZmlsdGVyKCBpID0+IGkuZmRhdGUgPj0gbWluX3RpbWUgJiYgaS5mZGF0ZSA8PSBtYXhfdGltZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZWZsaW5lID0gc3ZnLmFwcGVuZCgncGF0aCcpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X19saW5lLS1jb21wYXJlZCcpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmRhdHVtKCByZWZkYXRhX3B1cmdlZCApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ2QnLCBkMy5saW5lKClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLngoIGQgPT4geCggZC5mZGF0ZSApKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAueSggZCA9PiB5KCBkWyBmaWVsZGtleSBdICkpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgYWRkX2V2b2x1dGlvbl9saW5lKCByZWZkYXRhX3B1cmdlZCwgZmllbGRrZXksIHN2ZywgeCAseSApO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHYgPSBkYXRhLmdldChlLnRhcmdldC52YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWx0ZXJlZCA9IHYuZmlsdGVyKCBpID0+IGkuZmRhdGUgPj0gbWluX3RpbWUgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFkZF9ldm9sdXRpb25fbGluZSggZmlsdGVyZWQsIGZpZWxka2V5LCBzdmcsIHggLHkgKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgKTsgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgIC8vICBBZGQgZGF0ZXMgY29udHJvbCBcbiAgICAgICAgXG4gICAgICAgIGFkZF9jb250cm9sKFxuICAgICAgICAgICAgY29udHJvbHMsIFxuICAgICAgICAgICAgJ0Rlc2RlJywgXG4gICAgICAgICAgICBbIFxuICAgICAgICAgICAgICAgIHsgazogJ8OabHRpbW8gbWVzJywgdjogMzAgfSxcbiAgICAgICAgICAgICAgICB7IGs6ICfDmmx0aW1hIHNlbWFuYScsIHY6IDcgfSxcbiAgICAgICAgICAgICAgICB7IGs6ICdQcmltZXJhcyBtZWRpY2lvbmVzJywgdjogMCB9LFxuICAgICAgICAgICAgXSwgXG4gICAgICAgICAgICBmdW5jdGlvbihlKXtcbiAgICAgICAgICAgICAgICBjb25zdCB2YWwgPSBlLnRhcmdldC52YWx1ZTsgXG4gICAgICAgICAgICAgICAgc3dpdGNoKCB2YWwgKXtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnMCc6XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgcmVmID0gbW9tZW50KCB2YWx1ZVswXS5mZGF0ZSApLnVuaXgoKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgcmVmID0gbW9tZW50KG1heF90aW1lKS5zdWJ0cmFjdCggdmFsLCAnZGF5cycgKS51bml4KCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB2YWx1ZS5ldmVyeSggaSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmKCBtb21lbnQoaS5mZGF0ZSkudW5peCgpID49IHJlZiApe1xuICAgICAgICAgICAgICAgICAgICAgICAgbWluX3RpbWUgPSBpLmZkYXRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgc3ZnLnNlbGVjdCggJy5jaGFydF9fbGluZScgKS5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN2Zy5zZWxlY3QoICcuY2hhcnRfX2xpbmUtLWNvbXBhcmVkJyApLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgeCA9IGQzLnNjYWxlVGltZSgpLmRvbWFpbihbIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1pbl90aW1lLCBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhfdGltZVxuICAgICAgICAgICAgICAgICAgICAgICAgXSkucmFuZ2UoWyAwLCB3aWR0aCBdKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN2Zy5zZWxlY3QoICcuY2hhcnRfX3gnICkuY2FsbCggXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZDMuYXhpc0JvdHRvbSh4KS50aWNrcygzKS50aWNrRm9ybWF0KCBkID0+IHsgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBtb21lbnQoZCkuZm9ybWF0KCdERC1NTSBISDptbScpOyBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSBcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBuZXdfdmFsdWVzID0gdmFsdWUuZmlsdGVyKCBpID0+IGkuZmRhdGUgPj0gbWluX3RpbWUgKTsgXG4gICAgICAgICAgICAgICAgICAgICAgICBsaW5lID0gc3ZnLmFwcGVuZCgncGF0aCcpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X19saW5lJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZGF0dW0oIG5ld192YWx1ZXMgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdkJywgZDMubGluZSgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC54KCBkID0+IHgoIGQuZmRhdGUgKSApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC55KCBkID0+IHkoIGRbIGZpZWxka2V5IF0gKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICApOyAgXG4gICAgICBcbiAgICAgICAgLyoqXG4gICAgICAgICAqICAgIEFkZCBldmVudHNcbiAgICAgICAgICovICBcbiAgICAgICAgdmFyIGZvY3VzID0gc3ZnLmFwcGVuZChcImdcIik7XG4gICAgICAgIGZvY3VzLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X192YWx1ZScpO1xuICAgICAgICBmb2N1cy5hcHBlbmQoXCJjaXJjbGVcIilcbiAgICAgICAgICAuYXR0cihcImNsYXNzXCIsIGBkb3QtJHtmaWVsZGtleX1fJHtrZXl9YCkgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgIC5zdHlsZShcImZpbGxcIiwgXCJ0cmFuc3BhcmVudFwiKSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgLnN0eWxlKFwic3Ryb2tlXCIsIFwidHJhbnNwYXJlbnRcIikgICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAuYXR0cihcInJcIiwgNCk7ICBcbiAgICAgICAgZm9jdXMuYXBwZW5kKFwidGV4dFwiKVxuICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgYHZhbHVlLSR7ZmllbGRrZXl9XyR7a2V5fWApICAgXG4gICAgICAgICAgLmF0dHIoJ3gnLCB3aWR0aClcbiAgICAgICAgICAuYXR0cigneScsIDIwKSBcbiAgICAgICAgICAuc3R5bGUoJ3RleHQtYW5jaG9yJywgJ2VuZCcpICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgIC5zdHlsZShcImZpbGxcIiwgXCJ0cmFuc3BhcmVudFwiKVxuICAgICAgICAgIC5zdHlsZShcInN0cm9rZVwiLCBcInRyYW5zcGFyZW50XCIpXG4gICAgICAgICAgLnN0eWxlKCdmb250LXdlaWdodCcsICdib2xkJyk7IFxuICAgICAgICBmb2N1cy5hcHBlbmQoXCJ0ZXh0XCIpXG4gICAgICAgICAgLmF0dHIoXCJjbGFzc1wiLCBgZGF0ZS0ke2ZpZWxka2V5fV8ke2tleX1gKVxuICAgICAgICAgIC5hdHRyKCd4Jywgd2lkdGgpXG4gICAgICAgICAgLmF0dHIoJ3knLCAwKSAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgLnN0eWxlKCd0ZXh0LWFuY2hvcicsICdlbmQnKSAgICAgICAgIFxuICAgICAgICAgIC5zdHlsZShcImZpbGxcIiwgXCJ0cmFuc3BhcmVudFwiKVxuICAgICAgICAgIC5zdHlsZShcInN0cm9rZVwiLCBcInRyYW5zcGFyZW50XCIpOyAgICBcbiAgICAgICAgc3ZnLmFwcGVuZChcInJlY3RcIikgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAuYXR0cihcIndpZHRoXCIsIHdpZHRoKSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgLmF0dHIoXCJoZWlnaHRcIiwgaGVpZ2h0KSAgICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgLmF0dHIoXCJmaWxsXCIsIFwidHJhbnNwYXJlbnRcIikgXG4gICAgICAgICAgICAub24oXCJtb3VzZW1vdmVcIiwgKGUsIGQpID0+IHsgXG4gICAgICAgICAgICAgICAgY29uc3QgZGF0ZSA9IHguaW52ZXJ0KCBkMy5wb2ludGVyKGUpWzBdICk7XG4gICAgICAgICAgICAgICAgdmFyIHRtcCA9IHZhbHVlLm1hcCggaSA9PiBNYXRoLmFicyhpLmZkYXRlIC0gZGF0ZSkgKTtcbiAgICAgICAgICAgICAgICB2YXIgY2xvc2VzdCA9IHRtcC5pbmRleE9mKCBNYXRoLm1pbiguLi50bXApICk7IFxuICAgICAgICAgICAgICAgIGNvbnN0IHZhbCA9IHZhbHVlW2Nsb3Nlc3RdW2ZpZWxka2V5XTtcbiAgICAgICAgICAgICAgICBkMy5zZWxlY3QoYC5kb3QtJHtmaWVsZGtleX1fJHtrZXl9YClcbiAgICAgICAgICAgICAgICAgIC5zdHlsZShcInN0cm9rZVwiLCBcInJlZFwiKVxuICAgICAgICAgICAgICAgICAgLmF0dHIoJ2N4JywgeCggdmFsdWVbY2xvc2VzdF0uZmRhdGUgKSlcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCdjeScsIHkoIHZhbCApKTtcbiAgICAgICAgICAgICAgICBkMy5zZWxlY3QoYHRleHQudmFsdWUtJHtmaWVsZGtleX1fJHtrZXl9YClcbiAgICAgICAgICAgICAgICAgIC5zdHlsZShcImZpbGxcIiwgXCJyZWRcIilcbiAgICAgICAgICAgICAgICAgIC5odG1sKCB2YWwudG9GaXhlZCgyKSArICcgJyArIHN1ZmZpeCk7XG4gICAgICAgICAgICAgICAgZDMuc2VsZWN0KGB0ZXh0LmRhdGUtJHtmaWVsZGtleX1fJHtrZXl9YClcbiAgICAgICAgICAgICAgICAgIC5zdHlsZShcImZpbGxcIiwgXCJyZWRcIilcbiAgICAgICAgICAgICAgICAgIC5odG1sKCAgIHZhbHVlW2Nsb3Nlc3RdLmZkYXRlLnRvTG9jYWxlRGF0ZVN0cmluZygnZXMtRVMnLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBob3VyOiAnMi1kaWdpdCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW51dGU6JzItZGlnaXQnXG4gICAgICAgICAgICAgICAgICB9KSk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm9uKFwibW91c2VsZWF2ZVwiLCAoZSwgZCkgPT4geyBcbiAgICAgICAgICAgICAgICBkMy5zZWxlY3QoYC5kb3QtJHtmaWVsZGtleX1fJHtrZXl9YCkuc3R5bGUoJ3N0cm9rZScsICd0cmFuc3BhcmVudCcpO1xuICAgICAgICAgICAgICAgIGQzLnNlbGVjdChgLnZhbHVlLSR7ZmllbGRrZXl9XyR7a2V5fWApLnN0eWxlKCdmaWxsJywgJ3RyYW5zcGFyZW50Jyk7XG4gICAgICAgICAgICAgICAgZDMuc2VsZWN0KGAuZGF0ZS0ke2ZpZWxka2V5fV8ke2tleX1gKS5zdHlsZSgnZmlsbCcsICd0cmFuc3BhcmVudCcpO1xuICAgICAgICAgICAgfSk7ICAgIFxuICAgIH07XG59Il0sInNvdXJjZVJvb3QiOiIifQ==