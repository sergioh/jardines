/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"water": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index-water.js","vendors~water"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn-bd": "./node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-mx": "./node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "./node_modules/moment/locale/es-mx.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/index-water.js":
/*!****************************!*\
  !*** ./src/index-water.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Dependencies
const d3 = __webpack_require__(/*! d3 */ "./node_modules/d3/src/index.js");

const leaflet = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");

const leafletPip = __webpack_require__(/*! @mapbox/leaflet-pip */ "./node_modules/@mapbox/leaflet-pip/index.js");

const moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js"); // default namespace for SVGs


const NS = "http://www.w3.org/2000/svg";
const today = moment('20210827', 'YYYYMMDD');
let monthago = moment(today).subtract(30, 'days');
let playing = false;
let area = 44; // To calculate 

const aeroport_id = 5783;
const api_key = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkZXZAdGVqaWRvLmlvIiwianRpIjoiNDE3YWM4YmUtOWQ1Ni00ZWVkLWJlNWEtMmUxMmFkODdmNDk1IiwiaXNzIjoiQUVNRVQiLCJpYXQiOjE2MjQyOTY3NDksInVzZXJJZCI6IjQxN2FjOGJlLTlkNTYtNGVlZC1iZTVhLTJlMTJhZDg3ZjQ5NSIsInJvbGUiOiIifQ.ceYD31WhlDkF-F1X26NkI7GEcaqSyOJV8dSPIiBKMzA';
const aemet_base = 'https://opendata.aemet.es/opendata/api/';
const date_format = 'YYYY-MM-DDThh:mm:ssUTC';
const width = 800;
const height = 600;
let average_rainfall = 1.23;
let average_aa = 0.19; // When DOM is ready

document.addEventListener('DOMContentLoaded', function () {
  // player buttons
  var presentation = document.querySelector('.presentation');
  const slides = document.querySelectorAll('.presentation__page');
  var player = document.querySelector('.player');
  document.querySelector('[data-action="play"]').addEventListener('click', function () {
    player.dataset.on = "play";
    playing = true;
  });
  document.querySelector('[data-action="pause"]').addEventListener('click', function () {
    player.dataset.on = "pause";
    playing = false;
  });
  setInterval(() => {
    if (playing) {
      presentation.dataset.current = (+presentation.dataset.current + 1) % slides.length;
      var next = slides.item(+presentation.dataset.current);
      window.scrollTo(0, next.getBoundingClientRect().top + window.scrollY);
    }
  }, 8000); // Get rain data in the past month

  fetch('/api/flow').then(i => i.json()).then(data => {
    const supplies = d3.group(data, i => i.name);
    const aa_supply = supplies.get('aa0').filter(i => d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate) >= monthago && d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate) <= today);
    average_aa = d3.mean(aa_supply, i => i.liters) / area;
    let total_aa = aa_supply.reduce((a, b) => a + b.liters, 0);
    let relative_aa = (total_aa / area).toFixed(2);
    const net_supply = supplies.get('r0').filter(i => d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate) >= monthago && d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate) <= today);
    let total_net = net_supply.reduce((a, b) => a + b.liters, 0);
    console.log(total_aa, total_net);
    let weight = (100 * total_aa / (total_aa + total_net)).toFixed(2);
    document.querySelector('#aa-total .chart__chart--number').innerHTML = `${total_aa} l`;
    document.querySelector('#aa0 .chart__chart--number').innerHTML = `${relative_aa} l/m²`;
    document.querySelector('#aa0_r0 .chart__chart--number').innerHTML = `${weight} %`; //Get rain data from Seville aeroport in the last month

    const initial_date = moment(monthago).format(date_format);
    const end_date = moment(today).format(date_format);
    const data_url = `valores/climatologicos/diarios/datos/fechaini/${initial_date}/fechafin/${end_date}/estacion/${aeroport_id}`;
    fetch(`${aemet_base}${data_url}?api_key=${api_key}`).then(i => i.json()).then(response => {
      fetch(response.datos).then(i => i.json()).then(data => {
        data.forEach(i => {
          i.prec = parseFloat(i.prec) ? parseFloat(i.prec) : 0;
          i.fecha = moment(i.fecha, "YYYY-MM-DD");
        });
        average_rainfall = d3.mean(data, i => i.prec);
        let svg = d3.select('#chart--month');
        svg.attr('width', width).attr('height', height); // Create chart scales and add them to chart

        let x = d3.scaleTime().domain([monthago, today]).range([0, width]);
        svg.append("g").attr('class', 'chart__x').attr("transform", `translate(0, ${height})`).call(d3.axisBottom(x).ticks(4).tickFormat(d => {
          return moment(d).format('DD-MM');
        }));
        const y = d3.scaleLinear().domain([0, 40]).range([height, 0]);
        svg.append("g").attr('class', 'chart__y').attr("transform", `translate(0, 0)`).call(d3.axisLeft(y).tickFormat(d => d + " l")).selectAll('text').attr('x', '-20px'); // svg.append('path')
        //   .attr('class', 'chart__line chart__line--aeroport')
        //   .datum(data)
        //   .attr('d', d3.line()
        //     .x(d => x( d.fecha ))
        //     .y(d => y( d.prec ))
        //   );

        let def_aa_supply = [];

        for (var i = 0, date = monthago; i < 30; i++) {
          def_aa_supply.push({
            fdate: date.format('DDMMYYYY'),
            liters: 0
          });
          date.add(1, 'days');
        }

        aa_supply.forEach(i => {
          const k = moment(i.fdate).format('DDMMYYYY');
          let matches = def_aa_supply.filter(i => i.fdate == k);
          if (matches) matches[0].liters += i.liters;
        });
        svg.append('path').attr('class', 'chart__line chart__line--au4').datum(def_aa_supply).attr('d', d3.line().x(d => x(d3.timeParse("%d%m%Y")(d.fdate))).y(d => y(d.liters)));
      });
    });
  }); // Get rain data in the past month

  fetch('/api/sensor').then(i => i.json()).then(data => {
    data.forEach(i => {
      i.fdate = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
    });
    humidity_data = data.filter(i => i.name == 't1');
    const lastday = moment('20210822', 'YYYYMMDD'); //d3.max( humidity_data, i=> i.fdate);

    const weekago = moment(lastday).subtract(7, 'days');
    humidity_data = humidity_data.filter(i => i.fdate > weekago && i.fdate < lastday); // Add chart wrapper

    let svg = d3.select('#chart-average--week');
    const width = 800;
    const height = 600;
    svg.attr('width', width).attr('height', height); // Create chart scales and add them to chart

    let x = d3.scaleTime().domain([weekago, lastday]).range([0, width]);
    svg.append("g").attr('class', 'chart__x').attr("transform", `translate(0, ${height})`).call(d3.axisBottom(x).ticks(7).tickFormat(d => {
      return moment(d).format('DD-MM');
    }));
    const y = d3.scaleLinear().domain([820, 940]).range([height, 0]);
    svg.append("g").attr('class', 'chart__y').attr("transform", `translate(0, 0)`).call(d3.axisLeft(y)).selectAll('text').attr('x', '-20px');
    svg.append('path').attr('class', 'chart__line chart__line--au4').datum(humidity_data).attr('d', d3.line().x(d => x(d.fdate)).y(d => y(d.humidity)));
  });
  let icon = L.divIcon({
    iconSize: [20, 20],
    iconAnchor: [10, 10],
    html: `<div class="inner au2"></div>`
  });
  fetch('/static/jardines/js/data/countries.geo.json').then(i => i.json()).then(data => {
    var map = L.map('map', {
      center: [20, 0],
      zoom: 3,
      maxZoom: 18,
      attributionControl: false,
      zoomControl: false,
      scrollWheelZoom: false
    });
    L.geoJson(data, {}).addTo(map);
    var ref_rainfall = average_rainfall + average_aa;
    var currentmonth = new Date().getMonth();
    fetch('/static/jardines/js/data/data.json').then(i => i.json()).then(data => {
      for (const [k, v] of Object.entries(data)) {
        if ('rainfall' in v) {
          var diff = Math.abs(v.rainfall[currentmonth] - ref_rainfall);

          if (v.lat && v.lon && diff <= .25) {
            L.marker([parseFloat(v.lat), parseFloat(v.lon)], {
              icon: L.divIcon({
                iconSize: [20, 20],
                iconAnchor: [10, 10],
                html: `<div class="inner au2"></div>`
              })
            }).addTo(map);
          }
        }
      }

      ;
    });
  });
  const f = .07;
  icon = L.divIcon({
    iconSize: [3, 3]
  });
  fetch('/static/jardines/js/data/sevilla.geojson').then(i => i.json()).then(data => {
    var c = [37.3828, -5.973];
    const se = L.geoJson(data);
    var mapse = L.map('mapse', {
      center: c,
      zoom: 12,
      maxZoom: 18,
      attributionControl: false,
      zoomControl: false,
      scrollWheelZoom: false
    });
    se.addTo(mapse);
    var n = 1;
    var markers = [];
    L.marker(c, {
      icon: L.divIcon({
        iconSize: [10, 10],
        iconAnchor: [5, 5]
      })
    }).addTo(mapse).bindPopup('AAVV Candelaria').openPopup();
    setInterval(() => {
      var results = [];

      while (results.length == 0) {
        var point = [c[1] - f + Math.random(f * 2), c[0] - f + Math.random(f * 2)];
        results = leafletPip.pointInLayer(point, se);

        if (results.length > 0) {
          L.marker(point.reverse(), {
            icon: icon
          }).addTo(mapse);
          n += 2;
          document.querySelector('#na .chart__chart--number').innerHTML = n;
          document.querySelector('#na--rel .chart__chart--number').innerHTML = (n / 214748).toFixed(3) + "%";
          document.querySelector('#la .chart__chart--number').innerHTML = n * 1.5 + "l";
          document.querySelector('#gi .chart__chart--number').innerHTML = (n * 1.5 * 12 * 30 / 9267412).toFixed(2) + "gi";
        }
      }
    }, 100);
  });
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUgc3luYyBeXFwuXFwvLiokIiwid2VicGFjazovLy8uL3NyYy9pbmRleC13YXRlci5qcyJdLCJuYW1lcyI6WyJkMyIsInJlcXVpcmUiLCJsZWFmbGV0IiwibGVhZmxldFBpcCIsIm1vbWVudCIsIk5TIiwidG9kYXkiLCJtb250aGFnbyIsInN1YnRyYWN0IiwicGxheWluZyIsImFyZWEiLCJhZXJvcG9ydF9pZCIsImFwaV9rZXkiLCJhZW1ldF9iYXNlIiwiZGF0ZV9mb3JtYXQiLCJ3aWR0aCIsImhlaWdodCIsImF2ZXJhZ2VfcmFpbmZhbGwiLCJhdmVyYWdlX2FhIiwiZG9jdW1lbnQiLCJhZGRFdmVudExpc3RlbmVyIiwicHJlc2VudGF0aW9uIiwicXVlcnlTZWxlY3RvciIsInNsaWRlcyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJwbGF5ZXIiLCJkYXRhc2V0Iiwib24iLCJzZXRJbnRlcnZhbCIsImN1cnJlbnQiLCJsZW5ndGgiLCJuZXh0IiwiaXRlbSIsIndpbmRvdyIsInNjcm9sbFRvIiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0IiwidG9wIiwic2Nyb2xsWSIsImZldGNoIiwidGhlbiIsImkiLCJqc29uIiwiZGF0YSIsInN1cHBsaWVzIiwiZ3JvdXAiLCJuYW1lIiwiYWFfc3VwcGx5IiwiZ2V0IiwiZmlsdGVyIiwidGltZVBhcnNlIiwiZmRhdGUiLCJtZWFuIiwibGl0ZXJzIiwidG90YWxfYWEiLCJyZWR1Y2UiLCJhIiwiYiIsInJlbGF0aXZlX2FhIiwidG9GaXhlZCIsIm5ldF9zdXBwbHkiLCJ0b3RhbF9uZXQiLCJjb25zb2xlIiwibG9nIiwid2VpZ2h0IiwiaW5uZXJIVE1MIiwiaW5pdGlhbF9kYXRlIiwiZm9ybWF0IiwiZW5kX2RhdGUiLCJkYXRhX3VybCIsInJlc3BvbnNlIiwiZGF0b3MiLCJmb3JFYWNoIiwicHJlYyIsInBhcnNlRmxvYXQiLCJmZWNoYSIsInN2ZyIsInNlbGVjdCIsImF0dHIiLCJ4Iiwic2NhbGVUaW1lIiwiZG9tYWluIiwicmFuZ2UiLCJhcHBlbmQiLCJjYWxsIiwiYXhpc0JvdHRvbSIsInRpY2tzIiwidGlja0Zvcm1hdCIsImQiLCJ5Iiwic2NhbGVMaW5lYXIiLCJheGlzTGVmdCIsInNlbGVjdEFsbCIsImRlZl9hYV9zdXBwbHkiLCJkYXRlIiwicHVzaCIsImFkZCIsImsiLCJtYXRjaGVzIiwiZGF0dW0iLCJsaW5lIiwiaHVtaWRpdHlfZGF0YSIsImxhc3RkYXkiLCJ3ZWVrYWdvIiwiaHVtaWRpdHkiLCJpY29uIiwiTCIsImRpdkljb24iLCJpY29uU2l6ZSIsImljb25BbmNob3IiLCJodG1sIiwibWFwIiwiY2VudGVyIiwiem9vbSIsIm1heFpvb20iLCJhdHRyaWJ1dGlvbkNvbnRyb2wiLCJ6b29tQ29udHJvbCIsInNjcm9sbFdoZWVsWm9vbSIsImdlb0pzb24iLCJhZGRUbyIsInJlZl9yYWluZmFsbCIsImN1cnJlbnRtb250aCIsIkRhdGUiLCJnZXRNb250aCIsInYiLCJPYmplY3QiLCJlbnRyaWVzIiwiZGlmZiIsIk1hdGgiLCJhYnMiLCJyYWluZmFsbCIsImxhdCIsImxvbiIsIm1hcmtlciIsImYiLCJjIiwic2UiLCJtYXBzZSIsIm4iLCJtYXJrZXJzIiwiYmluZFBvcHVwIiwib3BlblBvcHVwIiwicmVzdWx0cyIsInBvaW50IiwicmFuZG9tIiwicG9pbnRJbkxheWVyIiwicmV2ZXJzZSJdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsUUFBUSxvQkFBb0I7UUFDNUI7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxpQkFBaUIsNEJBQTRCO1FBQzdDO1FBQ0E7UUFDQSxrQkFBa0IsMkJBQTJCO1FBQzdDO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsZ0JBQWdCLHVCQUF1QjtRQUN2Qzs7O1FBR0E7UUFDQTtRQUNBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ3ZKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkU7Ozs7Ozs7Ozs7O0FDblNBO0FBRUEsTUFBTUEsRUFBRSxHQUFHQyxtQkFBTyxDQUFDLDBDQUFELENBQWxCOztBQUNBLE1BQU1DLE9BQU8sR0FBR0QsbUJBQU8sQ0FBQywyREFBRCxDQUF2Qjs7QUFDQSxNQUFNRSxVQUFVLEdBQUdGLG1CQUFPLENBQUMsd0VBQUQsQ0FBMUI7O0FBQ0EsTUFBTUcsTUFBTSxHQUFHSCxtQkFBTyxDQUFDLCtDQUFELENBQXRCLEMsQ0FFQTs7O0FBQ0EsTUFBTUksRUFBRSxHQUFHLDRCQUFYO0FBQ0EsTUFBTUMsS0FBSyxHQUFJRixNQUFNLENBQUMsVUFBRCxFQUFhLFVBQWIsQ0FBckI7QUFDQSxJQUFJRyxRQUFRLEdBQUdILE1BQU0sQ0FBQ0UsS0FBRCxDQUFOLENBQWNFLFFBQWQsQ0FBdUIsRUFBdkIsRUFBMkIsTUFBM0IsQ0FBZjtBQUNBLElBQUlDLE9BQU8sR0FBRyxLQUFkO0FBQ0EsSUFBSUMsSUFBSSxHQUFHLEVBQVgsQyxDQUFlOztBQUNmLE1BQU1DLFdBQVcsR0FBRyxJQUFwQjtBQUNBLE1BQU1DLE9BQU8sR0FBRyxxUkFBaEI7QUFDQSxNQUFNQyxVQUFVLEdBQUcseUNBQW5CO0FBQ0EsTUFBTUMsV0FBVyxHQUFHLHdCQUFwQjtBQUNBLE1BQU1DLEtBQUssR0FBRyxHQUFkO0FBQ0EsTUFBTUMsTUFBTSxHQUFHLEdBQWY7QUFFQSxJQUFJQyxnQkFBZ0IsR0FBRyxJQUF2QjtBQUNBLElBQUlDLFVBQVUsR0FBRyxJQUFqQixDLENBRUE7O0FBQ0FDLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsa0JBQTFCLEVBQThDLFlBQVc7QUFFdkQ7QUFDQSxNQUFJQyxZQUFZLEdBQUdGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixlQUF2QixDQUFuQjtBQUNBLFFBQU1DLE1BQU0sR0FBR0osUUFBUSxDQUFDSyxnQkFBVCxDQUEwQixxQkFBMUIsQ0FBZjtBQUNBLE1BQUlDLE1BQU0sR0FBR04sUUFBUSxDQUFDRyxhQUFULENBQXVCLFNBQXZCLENBQWI7QUFDQUgsVUFBUSxDQUFDRyxhQUFULENBQXVCLHNCQUF2QixFQUErQ0YsZ0JBQS9DLENBQWdFLE9BQWhFLEVBQXlFLFlBQVc7QUFDaEZLLFVBQU0sQ0FBQ0MsT0FBUCxDQUFlQyxFQUFmLEdBQW9CLE1BQXBCO0FBQ0FsQixXQUFPLEdBQUcsSUFBVjtBQUNILEdBSEQ7QUFJQVUsVUFBUSxDQUFDRyxhQUFULENBQXVCLHVCQUF2QixFQUFnREYsZ0JBQWhELENBQWlFLE9BQWpFLEVBQTBFLFlBQVc7QUFDakZLLFVBQU0sQ0FBQ0MsT0FBUCxDQUFlQyxFQUFmLEdBQW9CLE9BQXBCO0FBQ0FsQixXQUFPLEdBQUcsS0FBVjtBQUNILEdBSEQ7QUFJQW1CLGFBQVcsQ0FBQyxNQUFNO0FBQ2QsUUFBR25CLE9BQUgsRUFBWTtBQUNSWSxrQkFBWSxDQUFDSyxPQUFiLENBQXFCRyxPQUFyQixHQUErQixDQUFDLENBQUNSLFlBQVksQ0FBQ0ssT0FBYixDQUFxQkcsT0FBdEIsR0FBZ0MsQ0FBakMsSUFBc0NOLE1BQU0sQ0FBQ08sTUFBNUU7QUFDQSxVQUFJQyxJQUFJLEdBQUdSLE1BQU0sQ0FBQ1MsSUFBUCxDQUFZLENBQUNYLFlBQVksQ0FBQ0ssT0FBYixDQUFxQkcsT0FBbEMsQ0FBWDtBQUNBSSxZQUFNLENBQUNDLFFBQVAsQ0FDRSxDQURGLEVBRUVILElBQUksQ0FBQ0kscUJBQUwsR0FBNkJDLEdBQTdCLEdBQW1DSCxNQUFNLENBQUNJLE9BRjVDO0FBSUg7QUFDSixHQVRVLEVBU1IsSUFUUSxDQUFYLENBZHVELENBeUJ2RDs7QUFDQUMsT0FBSyxDQUFDLFdBQUQsQ0FBTCxDQUFtQkMsSUFBbkIsQ0FBd0JDLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxJQUFGLEVBQTdCLEVBQXVDRixJQUF2QyxDQUE0Q0csSUFBSSxJQUNoRDtBQUNFLFVBQU1DLFFBQVEsR0FBRzNDLEVBQUUsQ0FBQzRDLEtBQUgsQ0FBU0YsSUFBVCxFQUFlRixDQUFDLElBQUlBLENBQUMsQ0FBQ0ssSUFBdEIsQ0FBakI7QUFDQSxVQUFNQyxTQUFTLEdBQUdILFFBQVEsQ0FBQ0ksR0FBVCxDQUFhLEtBQWIsRUFBb0JDLE1BQXBCLENBQ2hCUixDQUFDLElBQUl4QyxFQUFFLENBQUNpRCxTQUFILENBQWEsbUJBQWIsRUFBa0NULENBQUMsQ0FBQ1UsS0FBcEMsS0FBOEMzQyxRQUE5QyxJQUEwRFAsRUFBRSxDQUFDaUQsU0FBSCxDQUFhLG1CQUFiLEVBQWtDVCxDQUFDLENBQUNVLEtBQXBDLEtBQThDNUMsS0FEN0YsQ0FBbEI7QUFHQVksY0FBVSxHQUFHbEIsRUFBRSxDQUFDbUQsSUFBSCxDQUFTTCxTQUFULEVBQW9CTixDQUFDLElBQUlBLENBQUMsQ0FBQ1ksTUFBM0IsSUFBc0MxQyxJQUFuRDtBQUNBLFFBQUkyQyxRQUFRLEdBQUdQLFNBQVMsQ0FBQ1EsTUFBVixDQUFpQixDQUFDQyxDQUFELEVBQUlDLENBQUosS0FBVUQsQ0FBQyxHQUFHQyxDQUFDLENBQUNKLE1BQWpDLEVBQXlDLENBQXpDLENBQWY7QUFDQSxRQUFJSyxXQUFXLEdBQUcsQ0FBQ0osUUFBUSxHQUFHM0MsSUFBWixFQUFrQmdELE9BQWxCLENBQTBCLENBQTFCLENBQWxCO0FBQ0EsVUFBTUMsVUFBVSxHQUFHaEIsUUFBUSxDQUFDSSxHQUFULENBQWEsSUFBYixFQUFtQkMsTUFBbkIsQ0FDZlIsQ0FBQyxJQUFJeEMsRUFBRSxDQUFDaUQsU0FBSCxDQUFhLG1CQUFiLEVBQWtDVCxDQUFDLENBQUNVLEtBQXBDLEtBQThDM0MsUUFBOUMsSUFBMERQLEVBQUUsQ0FBQ2lELFNBQUgsQ0FBYSxtQkFBYixFQUFrQ1QsQ0FBQyxDQUFDVSxLQUFwQyxLQUE4QzVDLEtBRDlGLENBQW5CO0FBR0EsUUFBSXNELFNBQVMsR0FBR0QsVUFBVSxDQUFDTCxNQUFYLENBQWtCLENBQUNDLENBQUQsRUFBSUMsQ0FBSixLQUFVRCxDQUFDLEdBQUdDLENBQUMsQ0FBQ0osTUFBbEMsRUFBMEMsQ0FBMUMsQ0FBaEI7QUFDQVMsV0FBTyxDQUFDQyxHQUFSLENBQWFULFFBQWIsRUFBdUJPLFNBQXZCO0FBQ0EsUUFBSUcsTUFBTSxHQUFHLENBQUMsTUFBTVYsUUFBTixJQUFrQkEsUUFBUSxHQUFHTyxTQUE3QixDQUFELEVBQTBDRixPQUExQyxDQUFrRCxDQUFsRCxDQUFiO0FBQ0F2QyxZQUFRLENBQUNHLGFBQVQsQ0FBdUIsaUNBQXZCLEVBQTBEMEMsU0FBMUQsR0FBdUUsR0FBRVgsUUFBUyxJQUFsRjtBQUNBbEMsWUFBUSxDQUFDRyxhQUFULENBQXVCLDRCQUF2QixFQUFxRDBDLFNBQXJELEdBQWtFLEdBQUVQLFdBQVksT0FBaEY7QUFDQXRDLFlBQVEsQ0FBQ0csYUFBVCxDQUF1QiwrQkFBdkIsRUFBd0QwQyxTQUF4RCxHQUFxRSxHQUFFRCxNQUFPLElBQTlFLENBaEJGLENBa0JFOztBQUNBLFVBQU1FLFlBQVksR0FBRzdELE1BQU0sQ0FBQ0csUUFBRCxDQUFOLENBQWlCMkQsTUFBakIsQ0FBd0JwRCxXQUF4QixDQUFyQjtBQUNBLFVBQU1xRCxRQUFRLEdBQUcvRCxNQUFNLENBQUNFLEtBQUQsQ0FBTixDQUFjNEQsTUFBZCxDQUFxQnBELFdBQXJCLENBQWpCO0FBQ0EsVUFBTXNELFFBQVEsR0FBSSxpREFBZ0RILFlBQWEsYUFBWUUsUUFBUyxhQUFZeEQsV0FBWSxFQUE1SDtBQUNBMkIsU0FBSyxDQUFFLEdBQUV6QixVQUFXLEdBQUV1RCxRQUFTLFlBQVd4RCxPQUFRLEVBQTdDLENBQUwsQ0FBcUQyQixJQUFyRCxDQUNFQyxDQUFDLElBQUlBLENBQUMsQ0FBQ0MsSUFBRixFQURQLEVBRUVGLElBRkYsQ0FFTzhCLFFBQVEsSUFBSTtBQUNqQi9CLFdBQUssQ0FBQytCLFFBQVEsQ0FBQ0MsS0FBVixDQUFMLENBQXNCL0IsSUFBdEIsQ0FBMkJDLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxJQUFGLEVBQWhDLEVBQTBDRixJQUExQyxDQUErQ0csSUFBSSxJQUFJO0FBQ3JEQSxZQUFJLENBQUM2QixPQUFMLENBQWMvQixDQUFDLElBQUk7QUFDZkEsV0FBQyxDQUFDZ0MsSUFBRixHQUFVQyxVQUFVLENBQUNqQyxDQUFDLENBQUNnQyxJQUFILENBQVYsR0FBcUJDLFVBQVUsQ0FBQ2pDLENBQUMsQ0FBQ2dDLElBQUgsQ0FBL0IsR0FBMEMsQ0FBcEQ7QUFDQWhDLFdBQUMsQ0FBQ2tDLEtBQUYsR0FBVXRFLE1BQU0sQ0FBQ29DLENBQUMsQ0FBQ2tDLEtBQUgsRUFBVSxZQUFWLENBQWhCO0FBQ0gsU0FIRDtBQUlBekQsd0JBQWdCLEdBQUdqQixFQUFFLENBQUNtRCxJQUFILENBQVFULElBQVIsRUFBY0YsQ0FBQyxJQUFJQSxDQUFDLENBQUNnQyxJQUFyQixDQUFuQjtBQUNBLFlBQUlHLEdBQUcsR0FBRzNFLEVBQUUsQ0FBQzRFLE1BQUgsQ0FBVSxlQUFWLENBQVY7QUFFQUQsV0FBRyxDQUFDRSxJQUFKLENBQVMsT0FBVCxFQUFrQjlELEtBQWxCLEVBQXlCOEQsSUFBekIsQ0FBOEIsUUFBOUIsRUFBd0M3RCxNQUF4QyxFQVJxRCxDQVVyRDs7QUFDQSxZQUFJOEQsQ0FBQyxHQUFHOUUsRUFBRSxDQUFDK0UsU0FBSCxHQUFlQyxNQUFmLENBQXNCLENBQzVCekUsUUFENEIsRUFFNUJELEtBRjRCLENBQXRCLEVBR0wyRSxLQUhLLENBR0MsQ0FBQyxDQUFELEVBQUlsRSxLQUFKLENBSEQsQ0FBUjtBQUlBNEQsV0FBRyxDQUFDTyxNQUFKLENBQVcsR0FBWCxFQUNHTCxJQURILENBQ1EsT0FEUixFQUNpQixVQURqQixFQUVHQSxJQUZILENBRVEsV0FGUixFQUVzQixnQkFBZTdELE1BQU8sR0FGNUMsRUFHR21FLElBSEgsQ0FHUW5GLEVBQUUsQ0FBQ29GLFVBQUgsQ0FBY04sQ0FBZCxFQUFpQk8sS0FBakIsQ0FBdUIsQ0FBdkIsRUFBMEJDLFVBQTFCLENBQXFDQyxDQUFDLElBQUk7QUFDOUMsaUJBQU9uRixNQUFNLENBQUNtRixDQUFELENBQU4sQ0FBVXJCLE1BQVYsQ0FBaUIsT0FBakIsQ0FBUDtBQUNELFNBRkssQ0FIUjtBQU1BLGNBQU1zQixDQUFDLEdBQUd4RixFQUFFLENBQUN5RixXQUFILEdBQWlCVCxNQUFqQixDQUF3QixDQUFDLENBQUQsRUFBSSxFQUFKLENBQXhCLEVBQWlDQyxLQUFqQyxDQUF1QyxDQUFDakUsTUFBRCxFQUFTLENBQVQsQ0FBdkMsQ0FBVjtBQUVBMkQsV0FBRyxDQUFDTyxNQUFKLENBQVcsR0FBWCxFQUNHTCxJQURILENBQ1EsT0FEUixFQUNpQixVQURqQixFQUVHQSxJQUZILENBRVEsV0FGUixFQUVzQixpQkFGdEIsRUFHR00sSUFISCxDQUdRbkYsRUFBRSxDQUFDMEYsUUFBSCxDQUFZRixDQUFaLEVBQWVGLFVBQWYsQ0FBMEJDLENBQUMsSUFBSUEsQ0FBQyxHQUFHLElBQW5DLENBSFIsRUFJR0ksU0FKSCxDQUlhLE1BSmIsRUFLR2QsSUFMSCxDQUtRLEdBTFIsRUFLYSxPQUxiLEVBdkJxRCxDQTZCckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsWUFBSWUsYUFBYSxHQUFHLEVBQXBCOztBQUNBLGFBQUksSUFBSXBELENBQUMsR0FBQyxDQUFOLEVBQVNxRCxJQUFJLEdBQUN0RixRQUFsQixFQUE0QmlDLENBQUMsR0FBQyxFQUE5QixFQUFrQ0EsQ0FBQyxFQUFuQyxFQUFzQztBQUNsQ29ELHVCQUFhLENBQUNFLElBQWQsQ0FBbUI7QUFDaEI1QyxpQkFBSyxFQUFJMkMsSUFBSSxDQUFDM0IsTUFBTCxDQUFZLFVBQVosQ0FETztBQUVoQmQsa0JBQU0sRUFBRztBQUZPLFdBQW5CO0FBSUF5QyxjQUFJLENBQUNFLEdBQUwsQ0FBUyxDQUFULEVBQVksTUFBWjtBQUNIOztBQUNEakQsaUJBQVMsQ0FBQ3lCLE9BQVYsQ0FBbUIvQixDQUFDLElBQUk7QUFDcEIsZ0JBQU13RCxDQUFDLEdBQUc1RixNQUFNLENBQUNvQyxDQUFDLENBQUNVLEtBQUgsQ0FBTixDQUFnQmdCLE1BQWhCLENBQXVCLFVBQXZCLENBQVY7QUFDQSxjQUFJK0IsT0FBTyxHQUFHTCxhQUFhLENBQUM1QyxNQUFkLENBQXNCUixDQUFDLElBQUlBLENBQUMsQ0FBQ1UsS0FBRixJQUFXOEMsQ0FBdEMsQ0FBZDtBQUNBLGNBQUdDLE9BQUgsRUFBWUEsT0FBTyxDQUFDLENBQUQsQ0FBUCxDQUFXN0MsTUFBWCxJQUFxQlosQ0FBQyxDQUFDWSxNQUF2QjtBQUNmLFNBSkQ7QUFNQXVCLFdBQUcsQ0FBQ08sTUFBSixDQUFXLE1BQVgsRUFDR0wsSUFESCxDQUNRLE9BRFIsRUFDaUIsOEJBRGpCLEVBRUdxQixLQUZILENBRVVOLGFBRlYsRUFHR2YsSUFISCxDQUdRLEdBSFIsRUFHYTdFLEVBQUUsQ0FBQ21HLElBQUgsR0FDUnJCLENBRFEsQ0FDTlMsQ0FBQyxJQUFJVCxDQUFDLENBQUU5RSxFQUFFLENBQUNpRCxTQUFILENBQWEsUUFBYixFQUF1QnNDLENBQUMsQ0FBQ3JDLEtBQXpCLENBQUYsQ0FEQSxFQUVSc0MsQ0FGUSxDQUVORCxDQUFDLElBQUlDLENBQUMsQ0FBQ0QsQ0FBQyxDQUFDbkMsTUFBSCxDQUZBLENBSGI7QUFPRCxPQXpERDtBQTBERCxLQTdERDtBQThERCxHQXJGRCxFQTFCdUQsQ0FpSHZEOztBQUNBZCxPQUFLLENBQUMsYUFBRCxDQUFMLENBQXFCQyxJQUFyQixDQUEwQkMsQ0FBQyxJQUFJQSxDQUFDLENBQUNDLElBQUYsRUFBL0IsRUFBeUNGLElBQXpDLENBQThDRyxJQUFJLElBQUk7QUFDbERBLFFBQUksQ0FBQzZCLE9BQUwsQ0FBYS9CLENBQUMsSUFBSTtBQUNkQSxPQUFDLENBQUNVLEtBQUYsR0FBVWxELEVBQUUsQ0FBQ2lELFNBQUgsQ0FBYSxtQkFBYixFQUFrQ1QsQ0FBQyxDQUFDVSxLQUFwQyxDQUFWO0FBQ0gsS0FGRDtBQUdBa0QsaUJBQWEsR0FBRzFELElBQUksQ0FBQ00sTUFBTCxDQUFhUixDQUFDLElBQUtBLENBQUMsQ0FBQ0ssSUFBRixJQUFVLElBQTdCLENBQWhCO0FBQ0EsVUFBTXdELE9BQU8sR0FBR2pHLE1BQU0sQ0FBQyxVQUFELEVBQWEsVUFBYixDQUF0QixDQUxrRCxDQUtIOztBQUMvQyxVQUFNa0csT0FBTyxHQUFHbEcsTUFBTSxDQUFDaUcsT0FBRCxDQUFOLENBQWdCN0YsUUFBaEIsQ0FBeUIsQ0FBekIsRUFBNEIsTUFBNUIsQ0FBaEI7QUFDQTRGLGlCQUFhLEdBQUdBLGFBQWEsQ0FBQ3BELE1BQWQsQ0FBcUJSLENBQUMsSUFBSUEsQ0FBQyxDQUFDVSxLQUFGLEdBQVVvRCxPQUFWLElBQXFCOUQsQ0FBQyxDQUFDVSxLQUFGLEdBQVVtRCxPQUF6RCxDQUFoQixDQVBrRCxDQVNsRDs7QUFDQSxRQUFJMUIsR0FBRyxHQUFHM0UsRUFBRSxDQUFDNEUsTUFBSCxDQUFVLHNCQUFWLENBQVY7QUFDQSxVQUFNN0QsS0FBSyxHQUFHLEdBQWQ7QUFDQSxVQUFNQyxNQUFNLEdBQUcsR0FBZjtBQUNBMkQsT0FBRyxDQUFDRSxJQUFKLENBQVMsT0FBVCxFQUFrQjlELEtBQWxCLEVBQXlCOEQsSUFBekIsQ0FBOEIsUUFBOUIsRUFBd0M3RCxNQUF4QyxFQWJrRCxDQWVsRDs7QUFDQSxRQUFJOEQsQ0FBQyxHQUFHOUUsRUFBRSxDQUFDK0UsU0FBSCxHQUFlQyxNQUFmLENBQXNCLENBQzFCc0IsT0FEMEIsRUFFMUJELE9BRjBCLENBQXRCLEVBR0xwQixLQUhLLENBR0MsQ0FBQyxDQUFELEVBQUlsRSxLQUFKLENBSEQsQ0FBUjtBQUlBNEQsT0FBRyxDQUFDTyxNQUFKLENBQVcsR0FBWCxFQUNHTCxJQURILENBQ1EsT0FEUixFQUNpQixVQURqQixFQUVHQSxJQUZILENBRVEsV0FGUixFQUVzQixnQkFBZTdELE1BQU8sR0FGNUMsRUFHR21FLElBSEgsQ0FHUW5GLEVBQUUsQ0FBQ29GLFVBQUgsQ0FBY04sQ0FBZCxFQUFpQk8sS0FBakIsQ0FBdUIsQ0FBdkIsRUFBMEJDLFVBQTFCLENBQXFDQyxDQUFDLElBQUk7QUFDNUMsYUFBT25GLE1BQU0sQ0FBQ21GLENBQUQsQ0FBTixDQUFVckIsTUFBVixDQUFpQixPQUFqQixDQUFQO0FBQ0gsS0FGSyxDQUhSO0FBTUEsVUFBTXNCLENBQUMsR0FBR3hGLEVBQUUsQ0FBQ3lGLFdBQUgsR0FDUFQsTUFETyxDQUNBLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FEQSxFQUNZQyxLQURaLENBQ2tCLENBQUNqRSxNQUFELEVBQVMsQ0FBVCxDQURsQixDQUFWO0FBRUEyRCxPQUFHLENBQUNPLE1BQUosQ0FBVyxHQUFYLEVBQ0dMLElBREgsQ0FDUSxPQURSLEVBQ2lCLFVBRGpCLEVBRUdBLElBRkgsQ0FFUSxXQUZSLEVBRXNCLGlCQUZ0QixFQUdHTSxJQUhILENBR1FuRixFQUFFLENBQUMwRixRQUFILENBQVlGLENBQVosQ0FIUixFQUlHRyxTQUpILENBSWEsTUFKYixFQUtHZCxJQUxILENBS1EsR0FMUixFQUthLE9BTGI7QUFNQUYsT0FBRyxDQUFDTyxNQUFKLENBQVcsTUFBWCxFQUNHTCxJQURILENBQ1EsT0FEUixFQUNpQiw4QkFEakIsRUFFR3FCLEtBRkgsQ0FFU0UsYUFGVCxFQUdHdkIsSUFISCxDQUdRLEdBSFIsRUFHYTdFLEVBQUUsQ0FBQ21HLElBQUgsR0FDUnJCLENBRFEsQ0FDTlMsQ0FBQyxJQUFJVCxDQUFDLENBQUNTLENBQUMsQ0FBQ3JDLEtBQUgsQ0FEQSxFQUVSc0MsQ0FGUSxDQUVORCxDQUFDLElBQUlDLENBQUMsQ0FBQ0QsQ0FBQyxDQUFDZ0IsUUFBSCxDQUZBLENBSGI7QUFPSCxHQXpDRDtBQTJDQSxNQUFJQyxJQUFJLEdBQUdDLENBQUMsQ0FBQ0MsT0FBRixDQUFVO0FBQ25CQyxZQUFRLEVBQUssQ0FBQyxFQUFELEVBQUssRUFBTCxDQURNO0FBRW5CQyxjQUFVLEVBQUcsQ0FBQyxFQUFELEVBQUssRUFBTCxDQUZNO0FBR25CQyxRQUFJLEVBQUc7QUFIWSxHQUFWLENBQVg7QUFLQXZFLE9BQUssQ0FBQyw2Q0FBRCxDQUFMLENBQXFEQyxJQUFyRCxDQUEyREMsQ0FBQyxJQUFJQSxDQUFDLENBQUNDLElBQUYsRUFBaEUsRUFBMkVGLElBQTNFLENBQWlGRyxJQUFJLElBQUk7QUFDckYsUUFBSW9FLEdBQUcsR0FBR0wsQ0FBQyxDQUFDSyxHQUFGLENBQU0sS0FBTixFQUFhO0FBQ25CQyxZQUFNLEVBQWUsQ0FBRSxFQUFGLEVBQU0sQ0FBTixDQURGO0FBRW5CQyxVQUFJLEVBQWlCLENBRkY7QUFHbkJDLGFBQU8sRUFBYyxFQUhGO0FBSW5CQyx3QkFBa0IsRUFBRyxLQUpGO0FBS25CQyxpQkFBVyxFQUFVLEtBTEY7QUFNbkJDLHFCQUFlLEVBQU07QUFORixLQUFiLENBQVY7QUFRQVgsS0FBQyxDQUFDWSxPQUFGLENBQVUzRSxJQUFWLEVBQWdCLEVBQWhCLEVBQW9CNEUsS0FBcEIsQ0FBMEJSLEdBQTFCO0FBRUEsUUFBSVMsWUFBWSxHQUFHdEcsZ0JBQWdCLEdBQUdDLFVBQXRDO0FBQ0EsUUFBSXNHLFlBQVksR0FBRyxJQUFJQyxJQUFKLEdBQVdDLFFBQVgsRUFBbkI7QUFDQXBGLFNBQUssQ0FBQyxvQ0FBRCxDQUFMLENBQTRDQyxJQUE1QyxDQUFrREMsQ0FBQyxJQUFJQSxDQUFDLENBQUNDLElBQUYsRUFBdkQsRUFBa0VGLElBQWxFLENBQXdFRyxJQUFJLElBQUk7QUFDNUUsV0FBSSxNQUFNLENBQUNzRCxDQUFELEVBQUkyQixDQUFKLENBQVYsSUFBb0JDLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlbkYsSUFBZixDQUFwQixFQUF5QztBQUNyQyxZQUFHLGNBQWNpRixDQUFqQixFQUFtQjtBQUNmLGNBQUlHLElBQUksR0FBR0MsSUFBSSxDQUFDQyxHQUFMLENBQVVMLENBQUMsQ0FBQ00sUUFBRixDQUFXVCxZQUFYLElBQTJCRCxZQUFyQyxDQUFYOztBQUNBLGNBQUdJLENBQUMsQ0FBQ08sR0FBRixJQUFTUCxDQUFDLENBQUNRLEdBQVgsSUFBa0JMLElBQUksSUFBSSxHQUE3QixFQUFrQztBQUM5QnJCLGFBQUMsQ0FBQzJCLE1BQUYsQ0FBUyxDQUFDM0QsVUFBVSxDQUFDa0QsQ0FBQyxDQUFDTyxHQUFILENBQVgsRUFBb0J6RCxVQUFVLENBQUNrRCxDQUFDLENBQUNRLEdBQUgsQ0FBOUIsQ0FBVCxFQUFpRDtBQUM3QzNCLGtCQUFJLEVBQUdDLENBQUMsQ0FBQ0MsT0FBRixDQUFVO0FBQ2ZDLHdCQUFRLEVBQUssQ0FBQyxFQUFELEVBQUssRUFBTCxDQURFO0FBRWZDLDBCQUFVLEVBQUcsQ0FBQyxFQUFELEVBQUssRUFBTCxDQUZFO0FBR2ZDLG9CQUFJLEVBQUc7QUFIUSxlQUFWO0FBRHNDLGFBQWpELEVBTUdTLEtBTkgsQ0FNU1IsR0FOVDtBQU9IO0FBQ0o7QUFDSjs7QUFBQTtBQUNKLEtBZkQ7QUFnQkgsR0E3QkQ7QUE4QkEsUUFBTXVCLENBQUMsR0FBRyxHQUFWO0FBQ0E3QixNQUFJLEdBQUdDLENBQUMsQ0FBQ0MsT0FBRixDQUFVO0FBQ2ZDLFlBQVEsRUFBRyxDQUFDLENBQUQsRUFBRyxDQUFIO0FBREksR0FBVixDQUFQO0FBR0FyRSxPQUFLLENBQUMsMENBQUQsQ0FBTCxDQUFrREMsSUFBbEQsQ0FBd0RDLENBQUMsSUFBSUEsQ0FBQyxDQUFDQyxJQUFGLEVBQTdELEVBQXdFRixJQUF4RSxDQUE4RUcsSUFBSSxJQUFJO0FBQ2xGLFFBQUk0RixDQUFDLEdBQUcsQ0FBRSxPQUFGLEVBQVcsQ0FBQyxLQUFaLENBQVI7QUFDQSxVQUFNQyxFQUFFLEdBQUc5QixDQUFDLENBQUNZLE9BQUYsQ0FBVTNFLElBQVYsQ0FBWDtBQUNBLFFBQUk4RixLQUFLLEdBQUcvQixDQUFDLENBQUNLLEdBQUYsQ0FBTSxPQUFOLEVBQWU7QUFDdkJDLFlBQU0sRUFBZXVCLENBREU7QUFFdkJ0QixVQUFJLEVBQWlCLEVBRkU7QUFHdkJDLGFBQU8sRUFBYyxFQUhFO0FBSXZCQyx3QkFBa0IsRUFBRyxLQUpFO0FBS3ZCQyxpQkFBVyxFQUFVLEtBTEU7QUFNdkJDLHFCQUFlLEVBQU07QUFORSxLQUFmLENBQVo7QUFRQW1CLE1BQUUsQ0FBQ2pCLEtBQUgsQ0FBU2tCLEtBQVQ7QUFDQSxRQUFJQyxDQUFDLEdBQUcsQ0FBUjtBQUNBLFFBQUlDLE9BQU8sR0FBRyxFQUFkO0FBQ0FqQyxLQUFDLENBQUMyQixNQUFGLENBQVNFLENBQVQsRUFBWTtBQUNSOUIsVUFBSSxFQUFFQyxDQUFDLENBQUNDLE9BQUYsQ0FBVTtBQUNaQyxnQkFBUSxFQUFLLENBQUMsRUFBRCxFQUFLLEVBQUwsQ0FERDtBQUVaQyxrQkFBVSxFQUFHLENBQUMsQ0FBRCxFQUFJLENBQUo7QUFGRCxPQUFWO0FBREUsS0FBWixFQUtHVSxLQUxILENBS1NrQixLQUxULEVBS2dCRyxTQUxoQixDQUswQixpQkFMMUIsRUFLNkNDLFNBTDdDO0FBTUFoSCxlQUFXLENBQUMsTUFBTTtBQUNkLFVBQUlpSCxPQUFPLEdBQUcsRUFBZDs7QUFDQSxhQUFNQSxPQUFPLENBQUMvRyxNQUFSLElBQWtCLENBQXhCLEVBQTBCO0FBQ3RCLFlBQUlnSCxLQUFLLEdBQUcsQ0FBRVIsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFPRCxDQUFQLEdBQVdOLElBQUksQ0FBQ2dCLE1BQUwsQ0FBWVYsQ0FBQyxHQUFDLENBQWQsQ0FBYixFQUFnQ0MsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFPRCxDQUFQLEdBQVdOLElBQUksQ0FBQ2dCLE1BQUwsQ0FBWVYsQ0FBQyxHQUFDLENBQWQsQ0FBM0MsQ0FBWjtBQUNBUSxlQUFPLEdBQUcxSSxVQUFVLENBQUM2SSxZQUFYLENBQXdCRixLQUF4QixFQUErQlAsRUFBL0IsQ0FBVjs7QUFDQSxZQUFHTSxPQUFPLENBQUMvRyxNQUFSLEdBQWUsQ0FBbEIsRUFBb0I7QUFDaEIyRSxXQUFDLENBQUMyQixNQUFGLENBQVNVLEtBQUssQ0FBQ0csT0FBTixFQUFULEVBQTBCO0FBQUV6QyxnQkFBSSxFQUFFQTtBQUFSLFdBQTFCLEVBQTBDYyxLQUExQyxDQUFnRGtCLEtBQWhEO0FBQ0FDLFdBQUMsSUFBRSxDQUFIO0FBQ0F0SCxrQkFBUSxDQUFDRyxhQUFULENBQXVCLDJCQUF2QixFQUFvRDBDLFNBQXBELEdBQWdFeUUsQ0FBaEU7QUFDQXRILGtCQUFRLENBQUNHLGFBQVQsQ0FBdUIsZ0NBQXZCLEVBQXlEMEMsU0FBekQsR0FBcUUsQ0FBQ3lFLENBQUMsR0FBRyxNQUFMLEVBQWEvRSxPQUFiLENBQXFCLENBQXJCLElBQTBCLEdBQS9GO0FBQ0F2QyxrQkFBUSxDQUFDRyxhQUFULENBQXVCLDJCQUF2QixFQUFvRDBDLFNBQXBELEdBQWlFeUUsQ0FBQyxHQUFDLEdBQUgsR0FBVSxHQUExRTtBQUNBdEgsa0JBQVEsQ0FBQ0csYUFBVCxDQUF1QiwyQkFBdkIsRUFBb0QwQyxTQUFwRCxHQUFnRSxDQUFFeUUsQ0FBQyxHQUFDLEdBQUYsR0FBTSxFQUFOLEdBQVMsRUFBVixHQUFjLE9BQWYsRUFBd0IvRSxPQUF4QixDQUFnQyxDQUFoQyxJQUFxQyxJQUFyRztBQUNIO0FBQ0o7QUFDSixLQWRVLEVBY1IsR0FkUSxDQUFYO0FBZUgsR0FuQ0Q7QUFvQ0QsQ0F4T0QsRSIsImZpbGUiOiJ3YXRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIGluc3RhbGwgYSBKU09OUCBjYWxsYmFjayBmb3IgY2h1bmsgbG9hZGluZ1xuIFx0ZnVuY3Rpb24gd2VicGFja0pzb25wQ2FsbGJhY2soZGF0YSkge1xuIFx0XHR2YXIgY2h1bmtJZHMgPSBkYXRhWzBdO1xuIFx0XHR2YXIgbW9yZU1vZHVsZXMgPSBkYXRhWzFdO1xuIFx0XHR2YXIgZXhlY3V0ZU1vZHVsZXMgPSBkYXRhWzJdO1xuXG4gXHRcdC8vIGFkZCBcIm1vcmVNb2R1bGVzXCIgdG8gdGhlIG1vZHVsZXMgb2JqZWN0LFxuIFx0XHQvLyB0aGVuIGZsYWcgYWxsIFwiY2h1bmtJZHNcIiBhcyBsb2FkZWQgYW5kIGZpcmUgY2FsbGJhY2tcbiBcdFx0dmFyIG1vZHVsZUlkLCBjaHVua0lkLCBpID0gMCwgcmVzb2x2ZXMgPSBbXTtcbiBcdFx0Zm9yKDtpIDwgY2h1bmtJZHMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHRjaHVua0lkID0gY2h1bmtJZHNbaV07XG4gXHRcdFx0aWYoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKGluc3RhbGxlZENodW5rcywgY2h1bmtJZCkgJiYgaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdKSB7XG4gXHRcdFx0XHRyZXNvbHZlcy5wdXNoKGluc3RhbGxlZENodW5rc1tjaHVua0lkXVswXSk7XG4gXHRcdFx0fVxuIFx0XHRcdGluc3RhbGxlZENodW5rc1tjaHVua0lkXSA9IDA7XG4gXHRcdH1cbiBcdFx0Zm9yKG1vZHVsZUlkIGluIG1vcmVNb2R1bGVzKSB7XG4gXHRcdFx0aWYoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vcmVNb2R1bGVzLCBtb2R1bGVJZCkpIHtcbiBcdFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdID0gbW9yZU1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHRcdH1cbiBcdFx0fVxuIFx0XHRpZihwYXJlbnRKc29ucEZ1bmN0aW9uKSBwYXJlbnRKc29ucEZ1bmN0aW9uKGRhdGEpO1xuXG4gXHRcdHdoaWxlKHJlc29sdmVzLmxlbmd0aCkge1xuIFx0XHRcdHJlc29sdmVzLnNoaWZ0KCkoKTtcbiBcdFx0fVxuXG4gXHRcdC8vIGFkZCBlbnRyeSBtb2R1bGVzIGZyb20gbG9hZGVkIGNodW5rIHRvIGRlZmVycmVkIGxpc3RcbiBcdFx0ZGVmZXJyZWRNb2R1bGVzLnB1c2guYXBwbHkoZGVmZXJyZWRNb2R1bGVzLCBleGVjdXRlTW9kdWxlcyB8fCBbXSk7XG5cbiBcdFx0Ly8gcnVuIGRlZmVycmVkIG1vZHVsZXMgd2hlbiBhbGwgY2h1bmtzIHJlYWR5XG4gXHRcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIFx0fTtcbiBcdGZ1bmN0aW9uIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCkge1xuIFx0XHR2YXIgcmVzdWx0O1xuIFx0XHRmb3IodmFyIGkgPSAwOyBpIDwgZGVmZXJyZWRNb2R1bGVzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0dmFyIGRlZmVycmVkTW9kdWxlID0gZGVmZXJyZWRNb2R1bGVzW2ldO1xuIFx0XHRcdHZhciBmdWxmaWxsZWQgPSB0cnVlO1xuIFx0XHRcdGZvcih2YXIgaiA9IDE7IGogPCBkZWZlcnJlZE1vZHVsZS5sZW5ndGg7IGorKykge1xuIFx0XHRcdFx0dmFyIGRlcElkID0gZGVmZXJyZWRNb2R1bGVbal07XG4gXHRcdFx0XHRpZihpbnN0YWxsZWRDaHVua3NbZGVwSWRdICE9PSAwKSBmdWxmaWxsZWQgPSBmYWxzZTtcbiBcdFx0XHR9XG4gXHRcdFx0aWYoZnVsZmlsbGVkKSB7XG4gXHRcdFx0XHRkZWZlcnJlZE1vZHVsZXMuc3BsaWNlKGktLSwgMSk7XG4gXHRcdFx0XHRyZXN1bHQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IGRlZmVycmVkTW9kdWxlWzBdKTtcbiBcdFx0XHR9XG4gXHRcdH1cblxuIFx0XHRyZXR1cm4gcmVzdWx0O1xuIFx0fVxuXG4gXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBvYmplY3QgdG8gc3RvcmUgbG9hZGVkIGFuZCBsb2FkaW5nIGNodW5rc1xuIFx0Ly8gdW5kZWZpbmVkID0gY2h1bmsgbm90IGxvYWRlZCwgbnVsbCA9IGNodW5rIHByZWxvYWRlZC9wcmVmZXRjaGVkXG4gXHQvLyBQcm9taXNlID0gY2h1bmsgbG9hZGluZywgMCA9IGNodW5rIGxvYWRlZFxuIFx0dmFyIGluc3RhbGxlZENodW5rcyA9IHtcbiBcdFx0XCJ3YXRlclwiOiAwXG4gXHR9O1xuXG4gXHR2YXIgZGVmZXJyZWRNb2R1bGVzID0gW107XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL1wiO1xuXG4gXHR2YXIganNvbnBBcnJheSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSB8fCBbXTtcbiBcdHZhciBvbGRKc29ucEZ1bmN0aW9uID0ganNvbnBBcnJheS5wdXNoLmJpbmQoanNvbnBBcnJheSk7XG4gXHRqc29ucEFycmF5LnB1c2ggPSB3ZWJwYWNrSnNvbnBDYWxsYmFjaztcbiBcdGpzb25wQXJyYXkgPSBqc29ucEFycmF5LnNsaWNlKCk7XG4gXHRmb3IodmFyIGkgPSAwOyBpIDwganNvbnBBcnJheS5sZW5ndGg7IGkrKykgd2VicGFja0pzb25wQ2FsbGJhY2soanNvbnBBcnJheVtpXSk7XG4gXHR2YXIgcGFyZW50SnNvbnBGdW5jdGlvbiA9IG9sZEpzb25wRnVuY3Rpb247XG5cblxuIFx0Ly8gYWRkIGVudHJ5IG1vZHVsZSB0byBkZWZlcnJlZCBsaXN0XG4gXHRkZWZlcnJlZE1vZHVsZXMucHVzaChbXCIuL3NyYy9pbmRleC13YXRlci5qc1wiLFwidmVuZG9yc353YXRlclwiXSk7XG4gXHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIHJlYWR5XG4gXHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiIsInZhciBtYXAgPSB7XG5cdFwiLi9hZlwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYWYuanNcIixcblx0XCIuL2FmLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hZi5qc1wiLFxuXHRcIi4vYXJcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLmpzXCIsXG5cdFwiLi9hci1kelwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXItZHouanNcIixcblx0XCIuL2FyLWR6LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci1kei5qc1wiLFxuXHRcIi4vYXIta3dcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLWt3LmpzXCIsXG5cdFwiLi9hci1rdy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXIta3cuanNcIixcblx0XCIuL2FyLWx5XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci1seS5qc1wiLFxuXHRcIi4vYXItbHkuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLWx5LmpzXCIsXG5cdFwiLi9hci1tYVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXItbWEuanNcIixcblx0XCIuL2FyLW1hLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci1tYS5qc1wiLFxuXHRcIi4vYXItc2FcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLXNhLmpzXCIsXG5cdFwiLi9hci1zYS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXItc2EuanNcIixcblx0XCIuL2FyLXRuXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hci10bi5qc1wiLFxuXHRcIi4vYXItdG4uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2FyLXRuLmpzXCIsXG5cdFwiLi9hci5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYXIuanNcIixcblx0XCIuL2F6XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9hei5qc1wiLFxuXHRcIi4vYXouanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2F6LmpzXCIsXG5cdFwiLi9iZVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYmUuanNcIixcblx0XCIuL2JlLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9iZS5qc1wiLFxuXHRcIi4vYmdcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JnLmpzXCIsXG5cdFwiLi9iZy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYmcuanNcIixcblx0XCIuL2JtXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ibS5qc1wiLFxuXHRcIi4vYm0uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JtLmpzXCIsXG5cdFwiLi9iblwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYm4uanNcIixcblx0XCIuL2JuLWJkXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ibi1iZC5qc1wiLFxuXHRcIi4vYm4tYmQuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JuLWJkLmpzXCIsXG5cdFwiLi9ibi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYm4uanNcIixcblx0XCIuL2JvXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9iby5qc1wiLFxuXHRcIi4vYm8uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JvLmpzXCIsXG5cdFwiLi9iclwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYnIuanNcIixcblx0XCIuL2JyLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ici5qc1wiLFxuXHRcIi4vYnNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2JzLmpzXCIsXG5cdFwiLi9icy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvYnMuanNcIixcblx0XCIuL2NhXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9jYS5qc1wiLFxuXHRcIi4vY2EuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2NhLmpzXCIsXG5cdFwiLi9jc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvY3MuanNcIixcblx0XCIuL2NzLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9jcy5qc1wiLFxuXHRcIi4vY3ZcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2N2LmpzXCIsXG5cdFwiLi9jdi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvY3YuanNcIixcblx0XCIuL2N5XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9jeS5qc1wiLFxuXHRcIi4vY3kuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2N5LmpzXCIsXG5cdFwiLi9kYVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZGEuanNcIixcblx0XCIuL2RhLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9kYS5qc1wiLFxuXHRcIi4vZGVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2RlLmpzXCIsXG5cdFwiLi9kZS1hdFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZGUtYXQuanNcIixcblx0XCIuL2RlLWF0LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9kZS1hdC5qc1wiLFxuXHRcIi4vZGUtY2hcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2RlLWNoLmpzXCIsXG5cdFwiLi9kZS1jaC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZGUtY2guanNcIixcblx0XCIuL2RlLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9kZS5qc1wiLFxuXHRcIi4vZHZcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2R2LmpzXCIsXG5cdFwiLi9kdi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZHYuanNcIixcblx0XCIuL2VsXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbC5qc1wiLFxuXHRcIi4vZWwuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VsLmpzXCIsXG5cdFwiLi9lbi1hdVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4tYXUuanNcIixcblx0XCIuL2VuLWF1LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1hdS5qc1wiLFxuXHRcIi4vZW4tY2FcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWNhLmpzXCIsXG5cdFwiLi9lbi1jYS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4tY2EuanNcIixcblx0XCIuL2VuLWdiXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1nYi5qc1wiLFxuXHRcIi4vZW4tZ2IuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWdiLmpzXCIsXG5cdFwiLi9lbi1pZVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4taWUuanNcIixcblx0XCIuL2VuLWllLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1pZS5qc1wiLFxuXHRcIi4vZW4taWxcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWlsLmpzXCIsXG5cdFwiLi9lbi1pbC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4taWwuanNcIixcblx0XCIuL2VuLWluXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1pbi5qc1wiLFxuXHRcIi4vZW4taW4uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLWluLmpzXCIsXG5cdFwiLi9lbi1uelwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4tbnouanNcIixcblx0XCIuL2VuLW56LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lbi1uei5qc1wiLFxuXHRcIi4vZW4tc2dcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VuLXNnLmpzXCIsXG5cdFwiLi9lbi1zZy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZW4tc2cuanNcIixcblx0XCIuL2VvXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lby5qc1wiLFxuXHRcIi4vZW8uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VvLmpzXCIsXG5cdFwiLi9lc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXMuanNcIixcblx0XCIuL2VzLWRvXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lcy1kby5qc1wiLFxuXHRcIi4vZXMtZG8uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VzLWRvLmpzXCIsXG5cdFwiLi9lcy1teFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXMtbXguanNcIixcblx0XCIuL2VzLW14LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lcy1teC5qc1wiLFxuXHRcIi4vZXMtdXNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2VzLXVzLmpzXCIsXG5cdFwiLi9lcy11cy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXMtdXMuanNcIixcblx0XCIuL2VzLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9lcy5qc1wiLFxuXHRcIi4vZXRcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2V0LmpzXCIsXG5cdFwiLi9ldC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZXQuanNcIixcblx0XCIuL2V1XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ldS5qc1wiLFxuXHRcIi4vZXUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2V1LmpzXCIsXG5cdFwiLi9mYVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZmEuanNcIixcblx0XCIuL2ZhLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9mYS5qc1wiLFxuXHRcIi4vZmlcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZpLmpzXCIsXG5cdFwiLi9maS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZmkuanNcIixcblx0XCIuL2ZpbFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZmlsLmpzXCIsXG5cdFwiLi9maWwuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZpbC5qc1wiLFxuXHRcIi4vZm9cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZvLmpzXCIsXG5cdFwiLi9mby5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZm8uanNcIixcblx0XCIuL2ZyXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9mci5qc1wiLFxuXHRcIi4vZnItY2FcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZyLWNhLmpzXCIsXG5cdFwiLi9mci1jYS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZnItY2EuanNcIixcblx0XCIuL2ZyLWNoXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9mci1jaC5qc1wiLFxuXHRcIi4vZnItY2guanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ZyLWNoLmpzXCIsXG5cdFwiLi9mci5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZnIuanNcIixcblx0XCIuL2Z5XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9meS5qc1wiLFxuXHRcIi4vZnkuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2Z5LmpzXCIsXG5cdFwiLi9nYVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ2EuanNcIixcblx0XCIuL2dhLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9nYS5qc1wiLFxuXHRcIi4vZ2RcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2dkLmpzXCIsXG5cdFwiLi9nZC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ2QuanNcIixcblx0XCIuL2dsXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9nbC5qc1wiLFxuXHRcIi4vZ2wuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2dsLmpzXCIsXG5cdFwiLi9nb20tZGV2YVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ29tLWRldmEuanNcIixcblx0XCIuL2dvbS1kZXZhLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9nb20tZGV2YS5qc1wiLFxuXHRcIi4vZ29tLWxhdG5cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2dvbS1sYXRuLmpzXCIsXG5cdFwiLi9nb20tbGF0bi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvZ29tLWxhdG4uanNcIixcblx0XCIuL2d1XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ndS5qc1wiLFxuXHRcIi4vZ3UuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2d1LmpzXCIsXG5cdFwiLi9oZVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaGUuanNcIixcblx0XCIuL2hlLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9oZS5qc1wiLFxuXHRcIi4vaGlcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2hpLmpzXCIsXG5cdFwiLi9oaS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaGkuanNcIixcblx0XCIuL2hyXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9oci5qc1wiLFxuXHRcIi4vaHIuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2hyLmpzXCIsXG5cdFwiLi9odVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaHUuanNcIixcblx0XCIuL2h1LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9odS5qc1wiLFxuXHRcIi4vaHktYW1cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2h5LWFtLmpzXCIsXG5cdFwiLi9oeS1hbS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaHktYW0uanNcIixcblx0XCIuL2lkXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9pZC5qc1wiLFxuXHRcIi4vaWQuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2lkLmpzXCIsXG5cdFwiLi9pc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaXMuanNcIixcblx0XCIuL2lzLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9pcy5qc1wiLFxuXHRcIi4vaXRcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2l0LmpzXCIsXG5cdFwiLi9pdC1jaFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvaXQtY2guanNcIixcblx0XCIuL2l0LWNoLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9pdC1jaC5qc1wiLFxuXHRcIi4vaXQuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2l0LmpzXCIsXG5cdFwiLi9qYVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvamEuanNcIixcblx0XCIuL2phLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9qYS5qc1wiLFxuXHRcIi4vanZcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2p2LmpzXCIsXG5cdFwiLi9qdi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvanYuanNcIixcblx0XCIuL2thXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9rYS5qc1wiLFxuXHRcIi4va2EuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2thLmpzXCIsXG5cdFwiLi9ra1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva2suanNcIixcblx0XCIuL2trLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ray5qc1wiLFxuXHRcIi4va21cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2ttLmpzXCIsXG5cdFwiLi9rbS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva20uanNcIixcblx0XCIuL2tuXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9rbi5qc1wiLFxuXHRcIi4va24uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2tuLmpzXCIsXG5cdFwiLi9rb1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva28uanNcIixcblx0XCIuL2tvLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9rby5qc1wiLFxuXHRcIi4va3VcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2t1LmpzXCIsXG5cdFwiLi9rdS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUva3UuanNcIixcblx0XCIuL2t5XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9reS5qc1wiLFxuXHRcIi4va3kuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2t5LmpzXCIsXG5cdFwiLi9sYlwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbGIuanNcIixcblx0XCIuL2xiLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9sYi5qc1wiLFxuXHRcIi4vbG9cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2xvLmpzXCIsXG5cdFwiLi9sby5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbG8uanNcIixcblx0XCIuL2x0XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9sdC5qc1wiLFxuXHRcIi4vbHQuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL2x0LmpzXCIsXG5cdFwiLi9sdlwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbHYuanNcIixcblx0XCIuL2x2LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9sdi5qc1wiLFxuXHRcIi4vbWVcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21lLmpzXCIsXG5cdFwiLi9tZS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbWUuanNcIixcblx0XCIuL21pXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9taS5qc1wiLFxuXHRcIi4vbWkuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21pLmpzXCIsXG5cdFwiLi9ta1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbWsuanNcIixcblx0XCIuL21rLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tay5qc1wiLFxuXHRcIi4vbWxcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21sLmpzXCIsXG5cdFwiLi9tbC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbWwuanNcIixcblx0XCIuL21uXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tbi5qc1wiLFxuXHRcIi4vbW4uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21uLmpzXCIsXG5cdFwiLi9tclwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbXIuanNcIixcblx0XCIuL21yLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tci5qc1wiLFxuXHRcIi4vbXNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21zLmpzXCIsXG5cdFwiLi9tcy1teVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbXMtbXkuanNcIixcblx0XCIuL21zLW15LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tcy1teS5qc1wiLFxuXHRcIi4vbXMuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL21zLmpzXCIsXG5cdFwiLi9tdFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbXQuanNcIixcblx0XCIuL210LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9tdC5qc1wiLFxuXHRcIi4vbXlcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL215LmpzXCIsXG5cdFwiLi9teS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbXkuanNcIixcblx0XCIuL25iXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9uYi5qc1wiLFxuXHRcIi4vbmIuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL25iLmpzXCIsXG5cdFwiLi9uZVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbmUuanNcIixcblx0XCIuL25lLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9uZS5qc1wiLFxuXHRcIi4vbmxcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL25sLmpzXCIsXG5cdFwiLi9ubC1iZVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbmwtYmUuanNcIixcblx0XCIuL25sLWJlLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ubC1iZS5qc1wiLFxuXHRcIi4vbmwuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL25sLmpzXCIsXG5cdFwiLi9ublwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvbm4uanNcIixcblx0XCIuL25uLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ubi5qc1wiLFxuXHRcIi4vb2MtbG5jXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9vYy1sbmMuanNcIixcblx0XCIuL29jLWxuYy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvb2MtbG5jLmpzXCIsXG5cdFwiLi9wYS1pblwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcGEtaW4uanNcIixcblx0XCIuL3BhLWluLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9wYS1pbi5qc1wiLFxuXHRcIi4vcGxcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3BsLmpzXCIsXG5cdFwiLi9wbC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcGwuanNcIixcblx0XCIuL3B0XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9wdC5qc1wiLFxuXHRcIi4vcHQtYnJcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3B0LWJyLmpzXCIsXG5cdFwiLi9wdC1ici5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcHQtYnIuanNcIixcblx0XCIuL3B0LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9wdC5qc1wiLFxuXHRcIi4vcm9cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3JvLmpzXCIsXG5cdFwiLi9yby5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvcm8uanNcIixcblx0XCIuL3J1XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9ydS5qc1wiLFxuXHRcIi4vcnUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3J1LmpzXCIsXG5cdFwiLi9zZFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc2QuanNcIixcblx0XCIuL3NkLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zZC5qc1wiLFxuXHRcIi4vc2VcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NlLmpzXCIsXG5cdFwiLi9zZS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc2UuanNcIixcblx0XCIuL3NpXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zaS5qc1wiLFxuXHRcIi4vc2kuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NpLmpzXCIsXG5cdFwiLi9za1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc2suanNcIixcblx0XCIuL3NrLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zay5qc1wiLFxuXHRcIi4vc2xcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NsLmpzXCIsXG5cdFwiLi9zbC5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc2wuanNcIixcblx0XCIuL3NxXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zcS5qc1wiLFxuXHRcIi4vc3EuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NxLmpzXCIsXG5cdFwiLi9zclwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3IuanNcIixcblx0XCIuL3NyLWN5cmxcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NyLWN5cmwuanNcIixcblx0XCIuL3NyLWN5cmwuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NyLWN5cmwuanNcIixcblx0XCIuL3NyLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zci5qc1wiLFxuXHRcIi4vc3NcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3NzLmpzXCIsXG5cdFwiLi9zcy5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3MuanNcIixcblx0XCIuL3N2XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zdi5qc1wiLFxuXHRcIi4vc3YuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3N2LmpzXCIsXG5cdFwiLi9zd1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvc3cuanNcIixcblx0XCIuL3N3LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS9zdy5qc1wiLFxuXHRcIi4vdGFcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RhLmpzXCIsXG5cdFwiLi90YS5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGEuanNcIixcblx0XCIuL3RlXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90ZS5qc1wiLFxuXHRcIi4vdGUuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RlLmpzXCIsXG5cdFwiLi90ZXRcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RldC5qc1wiLFxuXHRcIi4vdGV0LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90ZXQuanNcIixcblx0XCIuL3RnXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90Zy5qc1wiLFxuXHRcIi4vdGcuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RnLmpzXCIsXG5cdFwiLi90aFwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGguanNcIixcblx0XCIuL3RoLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90aC5qc1wiLFxuXHRcIi4vdGtcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RrLmpzXCIsXG5cdFwiLi90ay5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdGsuanNcIixcblx0XCIuL3RsLXBoXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90bC1waC5qc1wiLFxuXHRcIi4vdGwtcGguanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RsLXBoLmpzXCIsXG5cdFwiLi90bGhcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RsaC5qc1wiLFxuXHRcIi4vdGxoLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90bGguanNcIixcblx0XCIuL3RyXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90ci5qc1wiLFxuXHRcIi4vdHIuanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3RyLmpzXCIsXG5cdFwiLi90emxcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3R6bC5qc1wiLFxuXHRcIi4vdHpsLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90emwuanNcIixcblx0XCIuL3R6bVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdHptLmpzXCIsXG5cdFwiLi90em0tbGF0blwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdHptLWxhdG4uanNcIixcblx0XCIuL3R6bS1sYXRuLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90em0tbGF0bi5qc1wiLFxuXHRcIi4vdHptLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS90em0uanNcIixcblx0XCIuL3VnLWNuXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS91Zy1jbi5qc1wiLFxuXHRcIi4vdWctY24uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3VnLWNuLmpzXCIsXG5cdFwiLi91a1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdWsuanNcIixcblx0XCIuL3VrLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS91ay5qc1wiLFxuXHRcIi4vdXJcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3VyLmpzXCIsXG5cdFwiLi91ci5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdXIuanNcIixcblx0XCIuL3V6XCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS91ei5qc1wiLFxuXHRcIi4vdXotbGF0blwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdXotbGF0bi5qc1wiLFxuXHRcIi4vdXotbGF0bi5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdXotbGF0bi5qc1wiLFxuXHRcIi4vdXouanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3V6LmpzXCIsXG5cdFwiLi92aVwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvdmkuanNcIixcblx0XCIuL3ZpLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS92aS5qc1wiLFxuXHRcIi4veC1wc2V1ZG9cIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3gtcHNldWRvLmpzXCIsXG5cdFwiLi94LXBzZXVkby5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUveC1wc2V1ZG8uanNcIixcblx0XCIuL3lvXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS95by5qc1wiLFxuXHRcIi4veW8uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3lvLmpzXCIsXG5cdFwiLi96aC1jblwiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvemgtY24uanNcIixcblx0XCIuL3poLWNuLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS96aC1jbi5qc1wiLFxuXHRcIi4vemgtaGtcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3poLWhrLmpzXCIsXG5cdFwiLi96aC1oay5qc1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvemgtaGsuanNcIixcblx0XCIuL3poLW1vXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS96aC1tby5qc1wiLFxuXHRcIi4vemgtbW8uanNcIjogXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlL3poLW1vLmpzXCIsXG5cdFwiLi96aC10d1wiOiBcIi4vbm9kZV9tb2R1bGVzL21vbWVudC9sb2NhbGUvemgtdHcuanNcIixcblx0XCIuL3poLXR3LmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvbW9tZW50L2xvY2FsZS96aC10dy5qc1wiXG59O1xuXG5cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0KHJlcSkge1xuXHR2YXIgaWQgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKTtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oaWQpO1xufVxuZnVuY3Rpb24gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkge1xuXHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKG1hcCwgcmVxKSkge1xuXHRcdHZhciBlID0gbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJ1wiKTtcblx0XHRlLmNvZGUgPSAnTU9EVUxFX05PVF9GT1VORCc7XG5cdFx0dGhyb3cgZTtcblx0fVxuXHRyZXR1cm4gbWFwW3JlcV07XG59XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gXCIuL25vZGVfbW9kdWxlcy9tb21lbnQvbG9jYWxlIHN5bmMgcmVjdXJzaXZlIF5cXFxcLlxcXFwvLiokXCI7IiwiLy8gRGVwZW5kZW5jaWVzXG5cbmNvbnN0IGQzID0gcmVxdWlyZSgnZDMnKTtcbmNvbnN0IGxlYWZsZXQgPSByZXF1aXJlKCdsZWFmbGV0Jyk7XG5jb25zdCBsZWFmbGV0UGlwID0gcmVxdWlyZSgnQG1hcGJveC9sZWFmbGV0LXBpcCcpO1xuY29uc3QgbW9tZW50ID0gcmVxdWlyZSgnbW9tZW50Jyk7XG5cbi8vIGRlZmF1bHQgbmFtZXNwYWNlIGZvciBTVkdzXG5jb25zdCBOUyA9IFwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIjtcbmNvbnN0IHRvZGF5ICA9IG1vbWVudCgnMjAyMTA4MjcnLCAnWVlZWU1NREQnKTtcbmxldCBtb250aGFnbyA9IG1vbWVudCh0b2RheSkuc3VidHJhY3QoMzAsICdkYXlzJyk7XG5sZXQgcGxheWluZyA9IGZhbHNlO1xubGV0IGFyZWEgPSA0NDsgLy8gVG8gY2FsY3VsYXRlIFxuY29uc3QgYWVyb3BvcnRfaWQgPSA1NzgzO1xuY29uc3QgYXBpX2tleSA9ICdleUpoYkdjaU9pSklVekkxTmlKOS5leUp6ZFdJaU9pSmtaWFpBZEdWcWFXUnZMbWx2SWl3aWFuUnBJam9pTkRFM1lXTTRZbVV0T1dRMU5pMDBaV1ZrTFdKbE5XRXRNbVV4TW1Ga09EZG1ORGsxSWl3aWFYTnpJam9pUVVWTlJWUWlMQ0pwWVhRaU9qRTJNalF5T1RZM05Ea3NJblZ6WlhKSlpDSTZJalF4TjJGak9HSmxMVGxrTlRZdE5HVmxaQzFpWlRWaExUSmxNVEpoWkRnM1pqUTVOU0lzSW5KdmJHVWlPaUlpZlEuY2VZRDMxV2hsRGtGLUYxWDI2TmtJN0dFY2FxU3lPSlY4ZFNQSWlCS016QSc7XG5jb25zdCBhZW1ldF9iYXNlID0gJ2h0dHBzOi8vb3BlbmRhdGEuYWVtZXQuZXMvb3BlbmRhdGEvYXBpLyc7XG5jb25zdCBkYXRlX2Zvcm1hdCA9ICdZWVlZLU1NLUREVGhoOm1tOnNzVVRDJztcbmNvbnN0IHdpZHRoID0gODAwO1xuY29uc3QgaGVpZ2h0ID0gNjAwO1xuXG5sZXQgYXZlcmFnZV9yYWluZmFsbCA9IDEuMjM7XG5sZXQgYXZlcmFnZV9hYSA9IDAuMTk7XG5cbi8vIFdoZW4gRE9NIGlzIHJlYWR5XG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZnVuY3Rpb24oKSB7XG5cbiAgLy8gcGxheWVyIGJ1dHRvbnNcbiAgdmFyIHByZXNlbnRhdGlvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5wcmVzZW50YXRpb24nKTtcbiAgY29uc3Qgc2xpZGVzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLnByZXNlbnRhdGlvbl9fcGFnZScpO1xuICB2YXIgcGxheWVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnBsYXllcicpO1xuICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdbZGF0YS1hY3Rpb249XCJwbGF5XCJdJykuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbigpIHtcbiAgICAgIHBsYXllci5kYXRhc2V0Lm9uID0gXCJwbGF5XCI7XG4gICAgICBwbGF5aW5nID0gdHJ1ZTtcbiAgfSk7XG4gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLWFjdGlvbj1cInBhdXNlXCJdJykuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbigpIHtcbiAgICAgIHBsYXllci5kYXRhc2V0Lm9uID0gXCJwYXVzZVwiO1xuICAgICAgcGxheWluZyA9IGZhbHNlO1xuICB9KTtcbiAgc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgaWYocGxheWluZykge1xuICAgICAgICAgIHByZXNlbnRhdGlvbi5kYXRhc2V0LmN1cnJlbnQgPSAoK3ByZXNlbnRhdGlvbi5kYXRhc2V0LmN1cnJlbnQgKyAxKSAlIHNsaWRlcy5sZW5ndGg7XG4gICAgICAgICAgdmFyIG5leHQgPSBzbGlkZXMuaXRlbSgrcHJlc2VudGF0aW9uLmRhdGFzZXQuY3VycmVudCk7XG4gICAgICAgICAgd2luZG93LnNjcm9sbFRvKFxuICAgICAgICAgICAgMCxcbiAgICAgICAgICAgIG5leHQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wICsgd2luZG93LnNjcm9sbFlcbiAgICAgICAgICApO1xuICAgICAgfVxuICB9LCA4MDAwKTtcblxuICAvLyBHZXQgcmFpbiBkYXRhIGluIHRoZSBwYXN0IG1vbnRoXG4gIGZldGNoKCcvYXBpL2Zsb3cnKS50aGVuKGkgPT4gaS5qc29uKCkpLnRoZW4oZGF0YSA9PiBcbiAge1xuICAgIGNvbnN0IHN1cHBsaWVzID0gZDMuZ3JvdXAoZGF0YSwgaSA9PiBpLm5hbWUpO1xuICAgIGNvbnN0IGFhX3N1cHBseSA9IHN1cHBsaWVzLmdldCgnYWEwJykuZmlsdGVyKFxuICAgICAgaSA9PiBkMy50aW1lUGFyc2UoXCIlbS8lZC8lWSAlSDolTTolU1wiKShpLmZkYXRlKSA+PSBtb250aGFnbyAmJiBkMy50aW1lUGFyc2UoXCIlbS8lZC8lWSAlSDolTTolU1wiKShpLmZkYXRlKSA8PSB0b2RheVxuICAgICk7XG4gICAgYXZlcmFnZV9hYSA9IGQzLm1lYW4oIGFhX3N1cHBseSwgaSA9PiBpLmxpdGVycyApIC8gYXJlYTtcbiAgICBsZXQgdG90YWxfYWEgPSBhYV9zdXBwbHkucmVkdWNlKChhLCBiKSA9PiBhICsgYi5saXRlcnMsIDApO1xuICAgIGxldCByZWxhdGl2ZV9hYSA9ICh0b3RhbF9hYSAvIGFyZWEpLnRvRml4ZWQoMik7XG4gICAgY29uc3QgbmV0X3N1cHBseSA9IHN1cHBsaWVzLmdldCgncjAnKS5maWx0ZXIoXG4gICAgICAgIGkgPT4gZDMudGltZVBhcnNlKFwiJW0vJWQvJVkgJUg6JU06JVNcIikoaS5mZGF0ZSkgPj0gbW9udGhhZ28gJiYgZDMudGltZVBhcnNlKFwiJW0vJWQvJVkgJUg6JU06JVNcIikoaS5mZGF0ZSkgPD0gdG9kYXlcbiAgICApO1xuICAgIGxldCB0b3RhbF9uZXQgPSBuZXRfc3VwcGx5LnJlZHVjZSgoYSwgYikgPT4gYSArIGIubGl0ZXJzLCAwKTtcbiAgICBjb25zb2xlLmxvZyggdG90YWxfYWEsIHRvdGFsX25ldCApO1xuICAgIGxldCB3ZWlnaHQgPSAoMTAwICogdG90YWxfYWEgLyAodG90YWxfYWEgKyB0b3RhbF9uZXQpKS50b0ZpeGVkKDIpO1xuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNhYS10b3RhbCAuY2hhcnRfX2NoYXJ0LS1udW1iZXInKS5pbm5lckhUTUwgPSBgJHt0b3RhbF9hYX0gbGA7XG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2FhMCAuY2hhcnRfX2NoYXJ0LS1udW1iZXInKS5pbm5lckhUTUwgPSBgJHtyZWxhdGl2ZV9hYX0gbC9twrJgO1xuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNhYTBfcjAgLmNoYXJ0X19jaGFydC0tbnVtYmVyJykuaW5uZXJIVE1MID0gYCR7d2VpZ2h0fSAlYDtcblxuICAgIC8vR2V0IHJhaW4gZGF0YSBmcm9tIFNldmlsbGUgYWVyb3BvcnQgaW4gdGhlIGxhc3QgbW9udGhcbiAgICBjb25zdCBpbml0aWFsX2RhdGUgPSBtb21lbnQobW9udGhhZ28pLmZvcm1hdChkYXRlX2Zvcm1hdCk7XG4gICAgY29uc3QgZW5kX2RhdGUgPSBtb21lbnQodG9kYXkpLmZvcm1hdChkYXRlX2Zvcm1hdCk7XG4gICAgY29uc3QgZGF0YV91cmwgPSBgdmFsb3Jlcy9jbGltYXRvbG9naWNvcy9kaWFyaW9zL2RhdG9zL2ZlY2hhaW5pLyR7aW5pdGlhbF9kYXRlfS9mZWNoYWZpbi8ke2VuZF9kYXRlfS9lc3RhY2lvbi8ke2Flcm9wb3J0X2lkfWA7XG4gICAgZmV0Y2goYCR7YWVtZXRfYmFzZX0ke2RhdGFfdXJsfT9hcGlfa2V5PSR7YXBpX2tleX1gKS50aGVuKFxuICAgICAgaSA9PiBpLmpzb24oKVxuICAgICkudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICBmZXRjaChyZXNwb25zZS5kYXRvcykudGhlbihpID0+IGkuanNvbigpKS50aGVuKGRhdGEgPT4ge1xuICAgICAgICBkYXRhLmZvckVhY2goIGkgPT4geyBcbiAgICAgICAgICAgIGkucHJlYyAgPSBwYXJzZUZsb2F0KGkucHJlYykgPyBwYXJzZUZsb2F0KGkucHJlYykgOiAwO1xuICAgICAgICAgICAgaS5mZWNoYSA9IG1vbWVudChpLmZlY2hhLCBcIllZWVktTU0tRERcIik7IFxuICAgICAgICB9KTtcbiAgICAgICAgYXZlcmFnZV9yYWluZmFsbCA9IGQzLm1lYW4oZGF0YSwgaSA9PiBpLnByZWMpO1xuICAgICAgICBsZXQgc3ZnID0gZDMuc2VsZWN0KCcjY2hhcnQtLW1vbnRoJyk7XG4gICAgICAgIFxuICAgICAgICBzdmcuYXR0cignd2lkdGgnLCB3aWR0aCkuYXR0cignaGVpZ2h0JywgaGVpZ2h0KTtcblxuICAgICAgICAvLyBDcmVhdGUgY2hhcnQgc2NhbGVzIGFuZCBhZGQgdGhlbSB0byBjaGFydFxuICAgICAgICBsZXQgeCA9IGQzLnNjYWxlVGltZSgpLmRvbWFpbihbXG4gICAgICAgICAgbW9udGhhZ28sXG4gICAgICAgICAgdG9kYXlcbiAgICAgICAgXSkucmFuZ2UoWzAsIHdpZHRoXSk7XG4gICAgICAgIHN2Zy5hcHBlbmQoXCJnXCIpXG4gICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X194JylcbiAgICAgICAgICAuYXR0cihcInRyYW5zZm9ybVwiLCBgdHJhbnNsYXRlKDAsICR7aGVpZ2h0fSlgKVxuICAgICAgICAgIC5jYWxsKGQzLmF4aXNCb3R0b20oeCkudGlja3MoNCkudGlja0Zvcm1hdChkID0+IHtcbiAgICAgICAgICAgIHJldHVybiBtb21lbnQoZCkuZm9ybWF0KCdERC1NTScpO1xuICAgICAgICAgIH0pKTtcbiAgICAgICAgY29uc3QgeSA9IGQzLnNjYWxlTGluZWFyKCkuZG9tYWluKFswLCA0MF0pLnJhbmdlKFtoZWlnaHQsIDBdKTtcblxuICAgICAgICBzdmcuYXBwZW5kKFwiZ1wiKVxuICAgICAgICAgIC5hdHRyKCdjbGFzcycsICdjaGFydF9feScpXG4gICAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgYHRyYW5zbGF0ZSgwLCAwKWApXG4gICAgICAgICAgLmNhbGwoZDMuYXhpc0xlZnQoeSkudGlja0Zvcm1hdChkID0+IGQgKyBcIiBsXCIpKVxuICAgICAgICAgIC5zZWxlY3RBbGwoJ3RleHQnKVxuICAgICAgICAgIC5hdHRyKCd4JywgJy0yMHB4Jyk7XG4gICAgICAgIC8vIHN2Zy5hcHBlbmQoJ3BhdGgnKVxuICAgICAgICAvLyAgIC5hdHRyKCdjbGFzcycsICdjaGFydF9fbGluZSBjaGFydF9fbGluZS0tYWVyb3BvcnQnKVxuICAgICAgICAvLyAgIC5kYXR1bShkYXRhKVxuICAgICAgICAvLyAgIC5hdHRyKCdkJywgZDMubGluZSgpXG4gICAgICAgIC8vICAgICAueChkID0+IHgoIGQuZmVjaGEgKSlcbiAgICAgICAgLy8gICAgIC55KGQgPT4geSggZC5wcmVjICkpXG4gICAgICAgIC8vICAgKTtcbiAgICAgICAgbGV0IGRlZl9hYV9zdXBwbHkgPSBbXTtcbiAgICAgICAgZm9yKHZhciBpPTAsIGRhdGU9bW9udGhhZ287IGk8MzA7IGkrKyl7XG4gICAgICAgICAgICBkZWZfYWFfc3VwcGx5LnB1c2goe1xuICAgICAgICAgICAgICAgZmRhdGUgIDogZGF0ZS5mb3JtYXQoJ0RETU1ZWVlZJyksXG4gICAgICAgICAgICAgICBsaXRlcnMgOiAwXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGRhdGUuYWRkKDEsICdkYXlzJyk7XG4gICAgICAgIH1cbiAgICAgICAgYWFfc3VwcGx5LmZvckVhY2goIGkgPT4ge1xuICAgICAgICAgICAgY29uc3QgayA9IG1vbWVudChpLmZkYXRlKS5mb3JtYXQoJ0RETU1ZWVlZJyk7XG4gICAgICAgICAgICBsZXQgbWF0Y2hlcyA9IGRlZl9hYV9zdXBwbHkuZmlsdGVyKCBpID0+IGkuZmRhdGUgPT0gayk7XG4gICAgICAgICAgICBpZihtYXRjaGVzKSBtYXRjaGVzWzBdLmxpdGVycyArPSBpLmxpdGVycztcbiAgICAgICAgfSk7XG5cbiAgICAgICAgc3ZnLmFwcGVuZCgncGF0aCcpXG4gICAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X19saW5lIGNoYXJ0X19saW5lLS1hdTQnKVxuICAgICAgICAgIC5kYXR1bSggZGVmX2FhX3N1cHBseSApXG4gICAgICAgICAgLmF0dHIoJ2QnLCBkMy5saW5lKClcbiAgICAgICAgICAgIC54KGQgPT4geCggZDMudGltZVBhcnNlKFwiJWQlbSVZXCIpKGQuZmRhdGUpICkpXG4gICAgICAgICAgICAueShkID0+IHkoZC5saXRlcnMpKVxuICAgICAgICAgICk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfSk7XG4gIFxuICAvLyBHZXQgcmFpbiBkYXRhIGluIHRoZSBwYXN0IG1vbnRoXG4gIGZldGNoKCcvYXBpL3NlbnNvcicpLnRoZW4oaSA9PiBpLmpzb24oKSkudGhlbihkYXRhID0+IHtcbiAgICAgIGRhdGEuZm9yRWFjaChpID0+IHtcbiAgICAgICAgICBpLmZkYXRlID0gZDMudGltZVBhcnNlKFwiJW0vJWQvJVkgJUg6JU06JVNcIikoaS5mZGF0ZSk7XG4gICAgICB9KTtcbiAgICAgIGh1bWlkaXR5X2RhdGEgPSBkYXRhLmZpbHRlciggaSA9PiAoaS5uYW1lID09ICd0MScpICk7XG4gICAgICBjb25zdCBsYXN0ZGF5ID0gbW9tZW50KCcyMDIxMDgyMicsICdZWVlZTU1ERCcpOy8vZDMubWF4KCBodW1pZGl0eV9kYXRhLCBpPT4gaS5mZGF0ZSk7XG4gICAgICBjb25zdCB3ZWVrYWdvID0gbW9tZW50KGxhc3RkYXkpLnN1YnRyYWN0KDcsICdkYXlzJyk7XG4gICAgICBodW1pZGl0eV9kYXRhID0gaHVtaWRpdHlfZGF0YS5maWx0ZXIoaSA9PiBpLmZkYXRlID4gd2Vla2FnbyAmJiBpLmZkYXRlIDwgbGFzdGRheSk7XG4gICAgICBcbiAgICAgIC8vIEFkZCBjaGFydCB3cmFwcGVyXG4gICAgICBsZXQgc3ZnID0gZDMuc2VsZWN0KCcjY2hhcnQtYXZlcmFnZS0td2VlaycpO1xuICAgICAgY29uc3Qgd2lkdGggPSA4MDA7XG4gICAgICBjb25zdCBoZWlnaHQgPSA2MDA7XG4gICAgICBzdmcuYXR0cignd2lkdGgnLCB3aWR0aCkuYXR0cignaGVpZ2h0JywgaGVpZ2h0KTtcblxuICAgICAgLy8gQ3JlYXRlIGNoYXJ0IHNjYWxlcyBhbmQgYWRkIHRoZW0gdG8gY2hhcnRcbiAgICAgIGxldCB4ID0gZDMuc2NhbGVUaW1lKCkuZG9tYWluKFtcbiAgICAgICAgICB3ZWVrYWdvLFxuICAgICAgICAgIGxhc3RkYXlcbiAgICAgIF0pLnJhbmdlKFswLCB3aWR0aF0pO1xuICAgICAgc3ZnLmFwcGVuZChcImdcIilcbiAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X194JylcbiAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgYHRyYW5zbGF0ZSgwLCAke2hlaWdodH0pYClcbiAgICAgICAgLmNhbGwoZDMuYXhpc0JvdHRvbSh4KS50aWNrcyg3KS50aWNrRm9ybWF0KGQgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIG1vbWVudChkKS5mb3JtYXQoJ0RELU1NJyk7XG4gICAgICAgIH0pKTtcbiAgICAgIGNvbnN0IHkgPSBkMy5zY2FsZUxpbmVhcigpXG4gICAgICAgIC5kb21haW4oWzgyMCwgOTQwXSkucmFuZ2UoW2hlaWdodCwgMF0pO1xuICAgICAgc3ZnLmFwcGVuZChcImdcIilcbiAgICAgICAgLmF0dHIoJ2NsYXNzJywgJ2NoYXJ0X195JylcbiAgICAgICAgLmF0dHIoXCJ0cmFuc2Zvcm1cIiwgYHRyYW5zbGF0ZSgwLCAwKWApXG4gICAgICAgIC5jYWxsKGQzLmF4aXNMZWZ0KHkpKVxuICAgICAgICAuc2VsZWN0QWxsKCd0ZXh0JylcbiAgICAgICAgLmF0dHIoJ3gnLCAnLTIwcHgnKTtcbiAgICAgIHN2Zy5hcHBlbmQoJ3BhdGgnKVxuICAgICAgICAuYXR0cignY2xhc3MnLCAnY2hhcnRfX2xpbmUgY2hhcnRfX2xpbmUtLWF1NCcpXG4gICAgICAgIC5kYXR1bShodW1pZGl0eV9kYXRhKVxuICAgICAgICAuYXR0cignZCcsIGQzLmxpbmUoKVxuICAgICAgICAgIC54KGQgPT4geChkLmZkYXRlKSlcbiAgICAgICAgICAueShkID0+IHkoZC5odW1pZGl0eSkpXG4gICAgICAgICk7XG4gIH0pO1xuXG4gIGxldCBpY29uID0gTC5kaXZJY29uKHtcbiAgICBpY29uU2l6ZSAgIDogWzIwLCAyMF0sXG4gICAgaWNvbkFuY2hvciA6IFsxMCwgMTBdLFxuICAgIGh0bWw6IGA8ZGl2IGNsYXNzPVwiaW5uZXIgYXUyXCI+PC9kaXY+YFxuICB9KTtcbiAgZmV0Y2goJy9zdGF0aWMvamFyZGluZXMvanMvZGF0YS9jb3VudHJpZXMuZ2VvLmpzb24nKS50aGVuKCBpID0+IGkuanNvbigpICkudGhlbiggZGF0YSA9PiB7XG4gICAgICB2YXIgbWFwID0gTC5tYXAoJ21hcCcsIHtcbiAgICAgICAgICBjZW50ZXIgICAgICAgICAgICAgOiBbIDIwLCAwIF0sXG4gICAgICAgICAgem9vbSAgICAgICAgICAgICAgIDogMyxcbiAgICAgICAgICBtYXhab29tICAgICAgICAgICAgOiAxOCxcbiAgICAgICAgICBhdHRyaWJ1dGlvbkNvbnRyb2wgOiBmYWxzZSxcbiAgICAgICAgICB6b29tQ29udHJvbCAgICAgICAgOiBmYWxzZSxcbiAgICAgICAgICBzY3JvbGxXaGVlbFpvb20gICAgOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgICAgTC5nZW9Kc29uKGRhdGEsIHt9KS5hZGRUbyhtYXApO1xuICAgICAgXG4gICAgICB2YXIgcmVmX3JhaW5mYWxsID0gYXZlcmFnZV9yYWluZmFsbCArIGF2ZXJhZ2VfYWE7XG4gICAgICB2YXIgY3VycmVudG1vbnRoID0gbmV3IERhdGUoKS5nZXRNb250aCgpO1xuICAgICAgZmV0Y2goJy9zdGF0aWMvamFyZGluZXMvanMvZGF0YS9kYXRhLmpzb24nKS50aGVuKCBpID0+IGkuanNvbigpICkudGhlbiggZGF0YSA9PiB7XG4gICAgICAgICAgZm9yKGNvbnN0IFtrLCB2XSBvZiBPYmplY3QuZW50cmllcyhkYXRhKSl7XG4gICAgICAgICAgICAgIGlmKCdyYWluZmFsbCcgaW4gdil7XG4gICAgICAgICAgICAgICAgICB2YXIgZGlmZiA9IE1hdGguYWJzKCB2LnJhaW5mYWxsW2N1cnJlbnRtb250aF0gLSByZWZfcmFpbmZhbGwgKTtcbiAgICAgICAgICAgICAgICAgIGlmKHYubGF0ICYmIHYubG9uICYmIGRpZmYgPD0gLjI1ICl7XG4gICAgICAgICAgICAgICAgICAgICAgTC5tYXJrZXIoW3BhcnNlRmxvYXQodi5sYXQpLCBwYXJzZUZsb2F0KHYubG9uKV0sIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgaWNvbiA6IEwuZGl2SWNvbih7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWNvblNpemUgICA6IFsyMCwgMjBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGljb25BbmNob3IgOiBbMTAsIDEwXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBodG1sOiBgPGRpdiBjbGFzcz1cImlubmVyIGF1MlwiPjwvZGl2PmBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICB9KS5hZGRUbyhtYXApO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfTtcbiAgICAgIH0pO1xuICB9KTtcbiAgY29uc3QgZiA9IC4wNztcbiAgaWNvbiA9IEwuZGl2SWNvbih7XG4gICAgaWNvblNpemUgOiBbMywzXVxuICB9KTtcbiAgZmV0Y2goJy9zdGF0aWMvamFyZGluZXMvanMvZGF0YS9zZXZpbGxhLmdlb2pzb24nKS50aGVuKCBpID0+IGkuanNvbigpICkudGhlbiggZGF0YSA9PiB7XG4gICAgICB2YXIgYyA9IFsgMzcuMzgyOCwgLTUuOTczIF07XG4gICAgICBjb25zdCBzZSA9IEwuZ2VvSnNvbihkYXRhKTtcbiAgICAgIHZhciBtYXBzZSA9IEwubWFwKCdtYXBzZScsIHtcbiAgICAgICAgICBjZW50ZXIgICAgICAgICAgICAgOiBjLFxuICAgICAgICAgIHpvb20gICAgICAgICAgICAgICA6IDEyLFxuICAgICAgICAgIG1heFpvb20gICAgICAgICAgICA6IDE4LFxuICAgICAgICAgIGF0dHJpYnV0aW9uQ29udHJvbCA6IGZhbHNlLFxuICAgICAgICAgIHpvb21Db250cm9sICAgICAgICA6IGZhbHNlLFxuICAgICAgICAgIHNjcm9sbFdoZWVsWm9vbSAgICA6IGZhbHNlLFxuICAgICAgfSk7XG4gICAgICBzZS5hZGRUbyhtYXBzZSk7IFxuICAgICAgdmFyIG4gPSAxO1xuICAgICAgdmFyIG1hcmtlcnMgPSBbXTtcbiAgICAgIEwubWFya2VyKGMsIHsgXG4gICAgICAgICAgaWNvbjogTC5kaXZJY29uKHtcbiAgICAgICAgICAgICAgaWNvblNpemUgICA6IFsxMCwgMTBdLFxuICAgICAgICAgICAgICBpY29uQW5jaG9yIDogWzUsIDVdLFxuICAgICAgICAgIH0pXG4gICAgICB9KS5hZGRUbyhtYXBzZSkuYmluZFBvcHVwKCdBQVZWIENhbmRlbGFyaWEnKS5vcGVuUG9wdXAoKTtcbiAgICAgIHNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICB2YXIgcmVzdWx0cyA9IFtdO1xuICAgICAgICAgIHdoaWxlKHJlc3VsdHMubGVuZ3RoID09IDApe1xuICAgICAgICAgICAgICB2YXIgcG9pbnQgPSBbIGNbMV0gLSBmICsgTWF0aC5yYW5kb20oZioyKSAsIGNbMF0gLSBmICsgTWF0aC5yYW5kb20oZioyKSBdO1xuICAgICAgICAgICAgICByZXN1bHRzID0gbGVhZmxldFBpcC5wb2ludEluTGF5ZXIocG9pbnQsIHNlKTtcbiAgICAgICAgICAgICAgaWYocmVzdWx0cy5sZW5ndGg+MCl7XG4gICAgICAgICAgICAgICAgICBMLm1hcmtlcihwb2ludC5yZXZlcnNlKCksIHsgaWNvbjogaWNvbiB9KS5hZGRUbyhtYXBzZSk7XG4gICAgICAgICAgICAgICAgICBuKz0yO1xuICAgICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI25hIC5jaGFydF9fY2hhcnQtLW51bWJlcicpLmlubmVySFRNTCA9IG47XG4gICAgICAgICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjbmEtLXJlbCAuY2hhcnRfX2NoYXJ0LS1udW1iZXInKS5pbm5lckhUTUwgPSAobiAvIDIxNDc0OCkudG9GaXhlZCgzKSArIFwiJVwiO1xuICAgICAgICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2xhIC5jaGFydF9fY2hhcnQtLW51bWJlcicpLmlubmVySFRNTCA9IChuKjEuNSkgKyBcImxcIjtcbiAgICAgICAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNnaSAuY2hhcnRfX2NoYXJ0LS1udW1iZXInKS5pbm5lckhUTUwgPSAoKG4qMS41KjEyKjMwKS85MjY3NDEyKS50b0ZpeGVkKDIpICsgXCJnaVwiO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgfSwgMTAwKTtcbiAgfSk7XG59KTsiXSwic291cmNlUm9vdCI6IiJ9