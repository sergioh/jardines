// Dependencies

const d3 = require('d3');
const leaflet = require('leaflet');
const leafletPip = require('@mapbox/leaflet-pip');
const moment = require('moment');

// default namespace for SVGs
const NS = "http://www.w3.org/2000/svg";
const today  = moment('20210827', 'YYYYMMDD');
let monthago = moment(today).subtract(30, 'days');
let playing = false;
let area = 44; // To calculate 
const aeroport_id = 5783;
const api_key = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkZXZAdGVqaWRvLmlvIiwianRpIjoiNDE3YWM4YmUtOWQ1Ni00ZWVkLWJlNWEtMmUxMmFkODdmNDk1IiwiaXNzIjoiQUVNRVQiLCJpYXQiOjE2MjQyOTY3NDksInVzZXJJZCI6IjQxN2FjOGJlLTlkNTYtNGVlZC1iZTVhLTJlMTJhZDg3ZjQ5NSIsInJvbGUiOiIifQ.ceYD31WhlDkF-F1X26NkI7GEcaqSyOJV8dSPIiBKMzA';
const aemet_base = 'https://opendata.aemet.es/opendata/api/';
const date_format = 'YYYY-MM-DDThh:mm:ssUTC';
const width = 800;
const height = 600;

let average_rainfall = 1.23;
let average_aa = 0.19;

// When DOM is ready
document.addEventListener('DOMContentLoaded', function() {

  // player buttons
  var presentation = document.querySelector('.presentation');
  const slides = document.querySelectorAll('.presentation__page');
  var player = document.querySelector('.player');
  document.querySelector('[data-action="play"]').addEventListener('click', function() {
      player.dataset.on = "play";
      playing = true;
  });
  document.querySelector('[data-action="pause"]').addEventListener('click', function() {
      player.dataset.on = "pause";
      playing = false;
  });
  setInterval(() => {
      if(playing) {
          presentation.dataset.current = (+presentation.dataset.current + 1) % slides.length;
          var next = slides.item(+presentation.dataset.current);
          window.scrollTo(
            0,
            next.getBoundingClientRect().top + window.scrollY
          );
      }
  }, 8000);

  // Get rain data in the past month
  fetch('/api/flow').then(i => i.json()).then(data => 
  {
    const supplies = d3.group(data, i => i.name);
    const aa_supply = supplies.get('aa0').filter(
      i => d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate) >= monthago && d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate) <= today
    );
    average_aa = d3.mean( aa_supply, i => i.liters ) / area;
    let total_aa = aa_supply.reduce((a, b) => a + b.liters, 0);
    let relative_aa = (total_aa / area).toFixed(2);
    const net_supply = supplies.get('r0').filter(
        i => d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate) >= monthago && d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate) <= today
    );
    let total_net = net_supply.reduce((a, b) => a + b.liters, 0);
    console.log( total_aa, total_net );
    let weight = (100 * total_aa / (total_aa + total_net)).toFixed(2);
    document.querySelector('#aa-total .chart__chart--number').innerHTML = `${total_aa} l`;
    document.querySelector('#aa0 .chart__chart--number').innerHTML = `${relative_aa} l/m²`;
    document.querySelector('#aa0_r0 .chart__chart--number').innerHTML = `${weight} %`;

    //Get rain data from Seville aeroport in the last month
    const initial_date = moment(monthago).format(date_format);
    const end_date = moment(today).format(date_format);
    const data_url = `valores/climatologicos/diarios/datos/fechaini/${initial_date}/fechafin/${end_date}/estacion/${aeroport_id}`;
    fetch(`${aemet_base}${data_url}?api_key=${api_key}`).then(
      i => i.json()
    ).then(response => {
      fetch(response.datos).then(i => i.json()).then(data => {
        data.forEach( i => { 
            i.prec  = parseFloat(i.prec) ? parseFloat(i.prec) : 0;
            i.fecha = moment(i.fecha, "YYYY-MM-DD"); 
        });
        average_rainfall = d3.mean(data, i => i.prec);
        let svg = d3.select('#chart--month');
        
        svg.attr('width', width).attr('height', height);

        // Create chart scales and add them to chart
        let x = d3.scaleTime().domain([
          monthago,
          today
        ]).range([0, width]);
        svg.append("g")
          .attr('class', 'chart__x')
          .attr("transform", `translate(0, ${height})`)
          .call(d3.axisBottom(x).ticks(4).tickFormat(d => {
            return moment(d).format('DD-MM');
          }));
        const y = d3.scaleLinear().domain([0, 40]).range([height, 0]);

        svg.append("g")
          .attr('class', 'chart__y')
          .attr("transform", `translate(0, 0)`)
          .call(d3.axisLeft(y).tickFormat(d => d + " l"))
          .selectAll('text')
          .attr('x', '-20px');
        // svg.append('path')
        //   .attr('class', 'chart__line chart__line--aeroport')
        //   .datum(data)
        //   .attr('d', d3.line()
        //     .x(d => x( d.fecha ))
        //     .y(d => y( d.prec ))
        //   );
        let def_aa_supply = [];
        for(var i=0, date=monthago; i<30; i++){
            def_aa_supply.push({
               fdate  : date.format('DDMMYYYY'),
               liters : 0
            });
            date.add(1, 'days');
        }
        aa_supply.forEach( i => {
            const k = moment(i.fdate).format('DDMMYYYY');
            let matches = def_aa_supply.filter( i => i.fdate == k);
            if(matches) matches[0].liters += i.liters;
        });

        svg.append('path')
          .attr('class', 'chart__line chart__line--au4')
          .datum( def_aa_supply )
          .attr('d', d3.line()
            .x(d => x( d3.timeParse("%d%m%Y")(d.fdate) ))
            .y(d => y(d.liters))
          );
      });
    });
  });
  
  // Get rain data in the past month
  fetch('/api/sensor').then(i => i.json()).then(data => {
      data.forEach(i => {
          i.fdate = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
      });
      humidity_data = data.filter( i => (i.name == 't1') );
      const lastday = moment('20210822', 'YYYYMMDD');//d3.max( humidity_data, i=> i.fdate);
      const weekago = moment(lastday).subtract(7, 'days');
      humidity_data = humidity_data.filter(i => i.fdate > weekago && i.fdate < lastday);
      
      // Add chart wrapper
      let svg = d3.select('#chart-average--week');
      const width = 800;
      const height = 600;
      svg.attr('width', width).attr('height', height);

      // Create chart scales and add them to chart
      let x = d3.scaleTime().domain([
          weekago,
          lastday
      ]).range([0, width]);
      svg.append("g")
        .attr('class', 'chart__x')
        .attr("transform", `translate(0, ${height})`)
        .call(d3.axisBottom(x).ticks(7).tickFormat(d => {
            return moment(d).format('DD-MM');
        }));
      const y = d3.scaleLinear()
        .domain([820, 940]).range([height, 0]);
      svg.append("g")
        .attr('class', 'chart__y')
        .attr("transform", `translate(0, 0)`)
        .call(d3.axisLeft(y))
        .selectAll('text')
        .attr('x', '-20px');
      svg.append('path')
        .attr('class', 'chart__line chart__line--au4')
        .datum(humidity_data)
        .attr('d', d3.line()
          .x(d => x(d.fdate))
          .y(d => y(d.humidity))
        );
  });

  let icon = L.divIcon({
    iconSize   : [20, 20],
    iconAnchor : [10, 10],
    html: `<div class="inner au2"></div>`
  });
  fetch('/static/jardines/js/data/countries.geo.json').then( i => i.json() ).then( data => {
      var map = L.map('map', {
          center             : [ 20, 0 ],
          zoom               : 3,
          maxZoom            : 18,
          attributionControl : false,
          zoomControl        : false,
          scrollWheelZoom    : false,
      });
      L.geoJson(data, {}).addTo(map);
      
      var ref_rainfall = average_rainfall + average_aa;
      var currentmonth = new Date().getMonth();
      fetch('/static/jardines/js/data/data.json').then( i => i.json() ).then( data => {
          for(const [k, v] of Object.entries(data)){
              if('rainfall' in v){
                  var diff = Math.abs( v.rainfall[currentmonth] - ref_rainfall );
                  if(v.lat && v.lon && diff <= .25 ){
                      L.marker([parseFloat(v.lat), parseFloat(v.lon)], {
                          icon : L.divIcon({
                            iconSize   : [20, 20],
                            iconAnchor : [10, 10],
                            html: `<div class="inner au2"></div>`
                          })
                      }).addTo(map);
                  }
              }
          };
      });
  });
  const f = .07;
  icon = L.divIcon({
    iconSize : [3,3]
  });
  fetch('/static/jardines/js/data/sevilla.geojson').then( i => i.json() ).then( data => {
      var c = [ 37.3828, -5.973 ];
      const se = L.geoJson(data);
      var mapse = L.map('mapse', {
          center             : c,
          zoom               : 12,
          maxZoom            : 18,
          attributionControl : false,
          zoomControl        : false,
          scrollWheelZoom    : false,
      });
      se.addTo(mapse); 
      var n = 1;
      var markers = [];
      L.marker(c, { 
          icon: L.divIcon({
              iconSize   : [10, 10],
              iconAnchor : [5, 5],
          })
      }).addTo(mapse).bindPopup('AAVV Candelaria').openPopup();
      setInterval(() => {
          var results = [];
          while(results.length == 0){
              var point = [ c[1] - f + Math.random(f*2) , c[0] - f + Math.random(f*2) ];
              results = leafletPip.pointInLayer(point, se);
              if(results.length>0){
                  L.marker(point.reverse(), { icon: icon }).addTo(mapse);
                  n+=2;
                  document.querySelector('#na .chart__chart--number').innerHTML = n;
                  document.querySelector('#na--rel .chart__chart--number').innerHTML = (n / 214748).toFixed(3) + "%";
                  document.querySelector('#la .chart__chart--number').innerHTML = (n*1.5) + "l";
                  document.querySelector('#gi .chart__chart--number').innerHTML = ((n*1.5*12*30)/9267412).toFixed(2) + "gi";
              }
          }
      }, 100);
  });
});