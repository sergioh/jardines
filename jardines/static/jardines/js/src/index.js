// Dependencies

const d3 = require('d3');
const moment = require('moment');

// default namespace for SVGs
const NS = "http://www.w3.org/2000/svg";

// When DOM is ready
document.addEventListener('DOMContentLoaded', function(){
  
   let flowdata, sensordata;
   document.body.classList.add('loading');
   
    // Get flow data and display it 
    fetch('/api/flow/').then( response => response.json() ).then( data => {
        // Format data properly
        data.forEach( i => {
            i.fdate  = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
            i.liters = parseFloat( i.liters );
        });
        // Group data
        flowdata = d3.group( data, item => item.name );
        add_evolution_charts('#flowcharts', flowdata, 'liters', 0, 1, 'l.');
        // Get sensor data and display it
        fetch('/api/sensor').then( response => response.json() ).then( data => {
            // Format data properly
            data.forEach( i => {
                i.fdate  = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
            });
            // Group data
            fetch('/api/weather').then( response => response.json() ).then( aeroport_data => {
                aeroport_data.forEach( i => {
                    i.fdate  = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
                });
                sensordata = d3.group( data, item => item.name );
                add_evolution_charts('#sensorcharts_temp', sensordata, 'temperature', 0, 60, '°', aeroport_data);
                // add_evolution_charts('#sensorcharts_press', sensordata, 'pressure', 'hPA', 0, 1500);
                add_evolution_charts('#sensorcharts_hum', sensordata, 'mapped_humidity', 0, 100, '%');
                
                document.querySelectorAll('.chartgroup').forEach( i => { i.classList.add('hidden'); });
                document.querySelector('#flowcharts').parentNode.classList.remove('hidden');
                document.body.classList.remove('loading');
                
                
                // On orientation change, redraw everything
                window.addEventListener('orientationchange', function(){
                    document.querySelector('#flowcharts').innerHTML = '';
                    document.querySelector('#sensorcharts_temp').innerHTML = '';
                    document.querySelector('#sensorcharts_hum').innerHTML = '';
                    add_evolution_charts('#flowcharts', flowdata, 'liters', 0, 1, 'l.');
                    add_evolution_charts('#sensorcharts_temp', sensordata, 'temperature', 0, 60, '°', aeroport_data);
                    add_evolution_charts('#sensorcharts_hum', sensordata, 'mapped_humidity', 0, 100, '%');
                });
            } );
        })
    });
    
    // Add basic interactions
    document.querySelectorAll('.charts-nav__item').forEach( i => {
        var target = i.dataset.show;
        i.addEventListener('click', e => {
            document.querySelectorAll('.chartgroup').forEach( i => { i.classList.add('hidden'); });
            document.querySelector(`#${target}`).parentNode.classList.remove('hidden');
            document.querySelectorAll('.charts-nav__item').forEach( i => { i.classList.remove('active') } );
            i.classList.add('active');
        }); 
    });
    
    window.addEventListener('orientationchange', function(){
        document.querySelectorAll('.chartgroup__container').forEach( i => {
            i.innerHTML = '';
        } );
    });
});

const whatis = ( id ) => {
   const prefixes = {
      'b'  : 'Línea de riego',
      't'  : 'Sensor de tierra',
      'aa' : 'Aire acondicionado',
      'r'  : 'Aporte de red',
      'au' : 'Aula exterior',
      'e'  : 'Sensor exterior de fachada'
   };
   let label = '';
   Object.keys(prefixes).forEach( k => {
      if( id.startsWith(k) ){
          label = prefixes[k];
          return;
      }
   });
   return label ? label : id;
}

/**
 *  create_flow_charts
 */ 

const add_evolution_line = ( value, fieldkey, chart, x, y ) => {
    chart.select( '.chart__line--compared' ).remove();
    chart.append('path')
        .attr('class', 'chart__line chart__line--compared')
        .datum( value )
        .attr('d', d3.line()
            .x( d => x( d.fdate ) )
            .y( d => y( d[ fieldkey ] ))
        ).attr('stroke-dasharray', '4 2');
};

const add_control = (wrapper, label, options, callback) => {
    const control = document.createElement('div');
    control.classList.add('chart-control');
    const control_label = document.createElement('span');
    control_label.classList.add('chart-control__label');
    control_label.innerHTML = label;
    control.appendChild(control_label);
    const control_items = document.createElement('select');
    control_items.classList.add('chart-control__select');
    options.forEach( option => {
        const control_item = document.createElement('option');
        control_item.classList.add('chart-control__option');
        control_item.innerHTML = option.k;
        control_item.value     = option.v;
        control_items.appendChild( control_item );     
    });
    control_items.addEventListener('change', callback );
    control.appendChild( control_items );
    wrapper.appendChild( control );        
}

const add_evolution_charts = ( parentnode, data, fieldkey, min, max, suffix, refdata ) => {
    const flowcharts = document.querySelector(parentnode);
    
    for( const [key, value] of data.entries() )
    {
        // If all values are negative don't render the chart
        if(!value.some( i => i[fieldkey] > 0)) continue;
        
        // Add html wrapper to D3 chart and its main elements
        const wrapper = document.createElement('div');
        wrapper.classList.add(['chart'], ['chart--flow']);
        const label   = document.createElement('h4');
        label.classList.add('chart__label');
        label.innerHTML = `${ whatis(key) } <span class="chart__label-key">${key}</span>`;
        wrapper.appendChild( label );
        flowcharts.appendChild( wrapper );
        
        // Add chart wrapper
        const width  = wrapper.clientWidth; 
        const height = 200;
        const svg_node = document.createElementNS(NS, 'svg');
        svg_node.classList.add('chart__svg');
        svg_node.id = `${fieldkey}_${key}`;
        wrapper.appendChild( svg_node );
        
        // Create D3 chart
        const svg = d3.select( `#${fieldkey}_${key}` );
        svg.attr('width', width).attr( 'height', height );
        
        /**
         * Create chart scales and add them to chart
         */
         
        const max_time = value[ value.length - 1 ].fdate;
        let min_time   = moment(max_time).subtract( 30, 'days' ); // Get last month by default
        let x = d3.scaleTime().domain([ 
            min_time, 
            max_time
        ]).range([ 0, width ]);
        svg.append("g")
            .attr('class', 'chart__x')
            .attr("transform", `translate(0, ${height})`)
            .call( d3.axisBottom(x).ticks(3).tickFormat( d => { 
                return moment(d).format('DD-MM hh:mm'); 
            } ));
        let y = d3.scaleLinear()
            .domain([min, max]).range([ height, 0 ]);    
        svg.append("g")
            .attr('class', 'chart__y')
            .attr("transform", `translate(0, 0)`)
            .call( d3.axisLeft(y).tickFormat( d => d + ' ' + suffix) )
            .selectAll('text')
            .attr('x', '-20px');
        
        /**
         *  Add main evolution line
         */
         
        let line = svg.append('path')
            .attr('class', 'chart__line')
            .datum( value.filter( i => i.fdate > min_time ) )
            .attr('d', d3.line()
                .x( d => x( d.fdate ) )
                .y( d => y( d[ fieldkey ] ))
            );
        
        /**
         *  Add controls
         */
                
        const controls = document.createElement('div');
        controls.classList.add('chart__controls');
        wrapper.appendChild( controls );
        
        
        // Add comparator
         
        const comparable = [
            { k: 'No comparar', v: 'none' }
        ];
        if( refdata ){
            comparable.push( { k : 'Aeropuerto', v : 'aeroport' })
        }
        for( const [k, v] of data.entries() ){
            if( k != key ) comparable.push({ k: k, v: k })
        }
        
        add_control(
            controls, 
            'Comparar con', 
            comparable,
            function(e){ 
                switch( e.target.value ){
                    case 'none':
                        svg.select( '.chart__line--compared' ).remove();
                    break;
                    case 'aeroport':
                        let refdata_purged = refdata.filter( i => i.fdate >= min_time && i.fdate <= max_time);
                        const refline = svg.append('path')
                            .attr('class', 'chart__line--compared')
                            .datum( refdata_purged )
                            .attr('d', d3.line()
                                .x( d => x( d.fdate ))
                                .y( d => y( d[ fieldkey ] ))
                            );
                          add_evolution_line( refdata_purged, fieldkey, svg, x ,y );
                    break;
                    default:
                        const v = data.get(e.target.value);
                        const filtered = v.filter( i => i.fdate >= min_time );
                        add_evolution_line( filtered, fieldkey, svg, x ,y );
                    break;
                };
            }
        );  
        
        
        //  Add dates control 
        
        add_control(
            controls, 
            'Desde', 
            [ 
                { k: 'Último mes', v: 30 },
                { k: 'Última semana', v: 7 },
                { k: 'Primeras mediciones', v: 0 },
            ], 
            function(e){
                const val = e.target.value; 
                switch( val ){
                    case '0':
                        var ref = moment( value[0].fdate ).unix();
                    break;
                    default:
                        var ref = moment(max_time).subtract( val, 'days' ).unix();
                    break;
                }
                value.every( i => {
                    if( moment(i.fdate).unix() >= ref ){
                        min_time = i.fdate;
                        svg.select( '.chart__line' ).remove();
                        svg.select( '.chart__line--compared' ).remove();
                        x = d3.scaleTime().domain([ 
                            min_time, 
                            max_time
                        ]).range([ 0, width ]);
                        svg.select( '.chart__x' ).call( 
                            d3.axisBottom(x).ticks(3).tickFormat( d => { 
                                return moment(d).format('DD-MM HH:mm'); 
                            }) 
                        );
                        const new_values = value.filter( i => i.fdate >= min_time ); 
                        line = svg.append('path')
                            .attr('class', 'chart__line')
                            .datum( new_values )
                            .attr('d', d3.line()
                                .x( d => x( d.fdate ) )
                                .y( d => y( d[ fieldkey ] ))
                            );
                        return false;
                    }
                    return true;
                });
            }
        );  
      
        /**
         *    Add events
         */  
        var focus = svg.append("g");
        focus.attr('class', 'chart__value');
        focus.append("circle")
          .attr("class", `dot-${fieldkey}_${key}`)                                
          .style("fill", "transparent")                             
          .style("stroke", "transparent")                           
          .attr("r", 4);  
        focus.append("text")
          .attr("class", `value-${fieldkey}_${key}`)   
          .attr('x', width)
          .attr('y', 20) 
          .style('text-anchor', 'end')                      
          .style("fill", "transparent")
          .style("stroke", "transparent")
          .style('font-weight', 'bold'); 
        focus.append("text")
          .attr("class", `date-${fieldkey}_${key}`)
          .attr('x', width)
          .attr('y', 0)                 
          .style('text-anchor', 'end')         
          .style("fill", "transparent")
          .style("stroke", "transparent");    
        svg.append("rect")                                     
            .attr("width", width)                              
            .attr("height", height)                           
            .attr("fill", "transparent") 
            .on("mousemove", (e, d) => { 
                const date = x.invert( d3.pointer(e)[0] );
                var tmp = value.map( i => Math.abs(i.fdate - date) );
                var closest = tmp.indexOf( Math.min(...tmp) ); 
                const val = value[closest][fieldkey];
                d3.select(`.dot-${fieldkey}_${key}`)
                  .style("stroke", "red")
                  .attr('cx', x( value[closest].fdate ))
                  .attr('cy', y( val ));
                d3.select(`text.value-${fieldkey}_${key}`)
                  .style("fill", "red")
                  .html( val.toFixed(2) + ' ' + suffix);
                d3.select(`text.date-${fieldkey}_${key}`)
                  .style("fill", "red")
                  .html(   value[closest].fdate.toLocaleDateString('es-ES', {
                        hour: '2-digit',
                        minute:'2-digit'
                  }));
            })
            .on("mouseleave", (e, d) => { 
                d3.select(`.dot-${fieldkey}_${key}`).style('stroke', 'transparent');
                d3.select(`.value-${fieldkey}_${key}`).style('fill', 'transparent');
                d3.select(`.date-${fieldkey}_${key}`).style('fill', 'transparent');
            });    
    };
}