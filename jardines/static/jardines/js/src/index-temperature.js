// Dependencies

const d3 = require('d3');
const leaflet = require('leaflet');
const moment = require('moment');

// default namespace for SVGs
const NS = "http://www.w3.org/2000/svg";
const today  = new Date();
let monthago = new Date(); 
monthago.setMonth( monthago.getMonth() - 1 );

playing = false;

// When DOM is ready
document.addEventListener('DOMContentLoaded', function()
{
  
    // player buttons
    var presentation = document.querySelector('.presentation');
    const slides = document.querySelectorAll('.presentation__page');
    var player = document.querySelector('.player');
    document.querySelector('[data-action="play"]').addEventListener('click', function(){
        player.dataset.on = "play";
        playing = true;
    });
    document.querySelector('[data-action="pause"]').addEventListener('click', function(){
        player.dataset.on = "pause";
        playing = false;
    });
    setInterval( () => { 
        if(playing) {
            presentation.dataset.current = ( + presentation.dataset.current + 1 ) % slides.length;
            var next = slides.item( +presentation.dataset.current );
            window.scrollTo( 
                0, 
                next.getBoundingClientRect().top + window.scrollY
            );
        } 
    }, 8000)
  
    let flowdata, sensordata;

    fetch('/api/sensor').then(response => response.json()).then(data => {
      // Format data properly
      data.forEach(i => {
        i.fdate = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
      });
      sensordata = d3.group(data, item => item.name);
      var min_date = d3.min(data, item => item.fdate);

      // Group data
      fetch('/api/weather').then(response => response.json()).then(aeroport_data =>
      {
          aeroport_data.forEach(i => {
            i.fdate = d3.timeParse("%m/%d/%Y %H:%M:%S")(i.fdate);
          });
          
          let min = d3.max( sensordata.get('au2'), i => i.fdate );
          min.setMonth( min.getMonth() - 1 );
          let max = d3.max( sensordata.get('au2'), i => i.fdate );

          let au2 = sensordata.get('au2').filter( i => i.fdate > min);
          let au4 = sensordata.get('au4').filter( i => i.fdate > min);

          // Get average difference between au2 and au4
          const au2_avg = d3.mean(au2, d => d.temperature);
          const au4_avg = d3.mean(au4, d => d.temperature);
          let diff = Math.abs(au2_avg - au4_avg).toFixed(2);
          document.querySelector('#au2_au4 .chart__chart--number').innerHTML = `+${diff}°`;

          // Get average difference between e0 and aeroport
          const e0 = sensordata.get('e0').filter( i => i.fdate > min);
          const e0_avg = d3.mean(e0, d => d.temperature);
          let a  = aeroport_data.filter( i => i.fdate > min && i.fdate < max);
          const ae_avg = d3.mean(a, d => d.temperature);
          diff = Math.abs(e0_avg - ae_avg).toFixed(2);
          // document.querySelector('#e0_ae .chart__chart--number').innerHTML = `${diff}°`;

          // Add chart wrapper
          let svg = d3.select('#chart-average--month');
          const width = 800;
          const height = 600;
          svg.attr('width', width).attr('height', height);

          // Create chart scales and add them to chart
          let x = d3.scaleTime().domain([
              min,
              max
          ]).range([0, width]);
          svg.append("g")
            .attr('class', 'chart__x')
            .attr("transform", `translate(0, ${height})`)
            .call(d3.axisBottom(x).ticks(15).tickFormat(d => {
                return moment(d).format('DD-MM');
            }));
          const y = d3.scaleLinear()
            .domain([0, 50]).range([height, 0]);
          svg.append("g")
            .attr('class', 'chart__y')
            .attr("transform", `translate(0, 0)`)
            .call(d3.axisLeft(y).tickFormat(d => d + "° "))
            .selectAll('text')
            .attr('x', '-20px');
          svg.append('path')
            .attr('class', 'chart__line chart__line--au4')
            .datum(au2)
            .attr('d', d3.line()
              .x(d => x(d.fdate))
              .y(d => y(d.temperature))
            );
          svg.append('path')
            .attr('class', 'chart__line chart__line--au2')
            .datum(au4)
            .attr('d', d3.line()
              .x(d => x(d.fdate))
              .y(d => y(d.temperature))
            );
          svg.append('path')
            .attr('class', 'chart__line chart__line--aeroport')
            .datum(a)
            .attr('d', d3.line()
              .x(d => x(d.fdate))
              .y(d => y(d.temperature))
            );
            
            // Add chart wrapper
            svg = d3.select('#chart-average--week');
            svg.attr('width', width).attr('height', height);

            // Create chart scales and add them to chart
            max = moment('20210802', 'YYYYMMDD');
            min = moment(max).subtract(6, 'days');
            x = d3.scaleTime().domain([
                min,
                max
            ]).range([0, width]);
            svg.append("g")
              .attr('class', 'chart__x')
              .attr("transform", `translate(0, ${height})`)
              .call(d3.axisBottom(x).ticks(7).tickFormat(d => {
                  return moment(d).format('DD-MM');
              }));
            svg.append("g")
              .attr('class', 'chart__y')
              .attr("transform", `translate(0, 0)`)
              .call(d3.axisLeft(y).tickFormat(d => d + "° "))
              .selectAll('text')
              .attr('x', '-20px');
            au2 = au2.filter( i => i.fdate > min && i.fdate < max);
            au4 = au4.filter( i => i.fdate > min && i.fdate < max);
            a   = a.filter( i => i.fdate > min && i.fdate < max);
            svg.append('path')
              .attr('class', 'chart__line chart__line--au4')
              .datum(au2)
              .attr('d', d3.line()
                .x(d => x(d.fdate))
                .y(d => y(d.temperature))
              );
            svg.append('path')
              .attr('class', 'chart__line chart__line--au2')
              .datum(au4)
              .attr('d', d3.line()
                .x(d => x(d.fdate))
                .y(d => y(d.temperature))
              );
            svg.append('path')
              .attr('class', 'chart__line chart__line--aeroport')
              .datum(a)
              .attr('d', d3.line()
                .x(d => x(d.fdate))
                .y(d => y(d.temperature))
              );

            // Get average of minimum tempaeratures
            let measures = {};
            au2.forEach(i => {
                const dat = moment(i.fdate).format('DDMM');
                if(!(dat in measures)){
                    measures[dat] = [ i.temperature];
                } else {
                    measures[dat].push( i.temperature );
                }
            });
            let minimum = [];
            for(day in measures){
                minimum.push( d3.min(measures[day], i=>i) )
            }
            const avg_min = d3.mean( minimum );
            const avg = d3.mean(au2, i => i.temperature);
            diff = Math.abs(avg - avg_min).toFixed(2);
            document.querySelector('#au2_min .chart__chart--number').innerHTML = `+${diff}°`;
            
            // Map
          
            fetch('/static/jardines/js/data/countries.geo.json').then( i => i.json() ).then( data => {
                var map = L.map('map', {
                    center             : [ 20, 0 ],
                    zoom               : 3,
                    maxZoom            : 18,
                    attributionControl : false,
                    zoomControl        : false,
                    scrollWheelZoom    : false,
                });
                L.geoJson(data, {}).addTo(map);
                const tts = [
                    { t : d3.max( au2.filter( i => i.fdate == d3.max(au2, i => i.fdate)), i => i.temperature), c: 'yellow' },
                    { t : 20, c: 'blue' },
                    { t : 36, c: 'red' }
                ];
                fetch('/static/jardines/js/data/data.json').then( i => i.json() ).then( data => {
                    for(const [k, v] of Object.entries(data)){
                        tts.forEach( ref => {
                          console.log( parseInt(max.format('M')) );
                            var val = Math.abs( parseFloat(v['average_temperature'][ parseInt(max.format('M')) ]) - ref.t );
                            if(v.lat && v.lon && val <= 1 ){
                              L.marker([
                                parseFloat(v.lat),
                                parseFloat(v.lon)
                              ], {
                                icon : L.divIcon({
                                  iconSize   : [20, 20],
                                  iconAnchor : [10, 10],
                                  html: `<div class="inner ${ref.c}"></div>`
                                })
                              }).addTo(map);
                            }
                        })
                    };
                });
            });
        });
    })
});