from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework import permissions
from api.serializers import SensorDataSerializer
from api.serializers import FlowDataSerializer
from api.serializers import WateringConfSerializer
from api.serializers import SystemConfSerializer
from api.serializers import LogDataSerializer
from api.serializers import WeatherDataSerializer
from api.models import SensorData
from api.models import FlowData
from api.models import WateringConf
from api.models import SystemConf
from api.models import LogData
from api.models import WeatherData

# auth
from rest_framework.permissions import BasePermission


class ApiPermission(permissions.BasePermission):
    
    def has_permission(self, request, view):
        if view.action == 'list':
            return True
        return request.user.is_authenticated


# doc: https://tests4geeks.com/blog/django-rest-framework-tutorial/
# get data: curl -H 'Accept: application/json; indent=4' http://jardines.tejido.io:7070/api/data/
# post new data: curl X POST -H "Content-Type: application/json" http://jardines.tejido.io:7070/api/data/ -d '{"name" : "test2"}'
# get sensor data by name: curl -H 'Accept: application/json; indent=4' http://jardines.tejido.io:7070/api/data/test2/

class SensorDataViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows SensorData to be viewed or edited.
    """
    permission_classes = (ApiPermission,)
    queryset = SensorData.objects.all()
    serializer_class = SensorDataSerializer
    #retrieve detail by name instead id
    lookup_field = 'name'

class FlowDataViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows FlowData to be viewed or edited.
    """
    permission_classes = (ApiPermission,)
    queryset = FlowData.objects.all()
    serializer_class = FlowDataSerializer
    #retrieve detail by name instead id
    lookup_field = 'name'

class WateringConfViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows WateringConf to be viewed or edited.
    """
    permission_classes = (ApiPermission,)
    queryset = WateringConf.objects.all()
    serializer_class = WateringConfSerializer
    #retrieve detail by name instead id
    lookup_field = 'name'

class SystemConfViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows SystemConf to be viewed or edited.
    """
    permission_classes = (ApiPermission,)
    queryset = SystemConf.objects.all()
    serializer_class = SystemConfSerializer
    #retrieve detail by name instead id
    lookup_field = 'name'

class LogDataViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows FlowData to be viewed or edited.
    """
    permission_classes = (ApiPermission,)
    queryset = LogData.objects.all()
    serializer_class = LogDataSerializer
    #retrieve detail by name instead id
    lookup_field = 'name'

class WeatherViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows FlowData to be viewed or edited.
    """
    permission_classes = (ApiPermission,)
    queryset = WeatherData.objects.all()
    serializer_class = WeatherDataSerializer
