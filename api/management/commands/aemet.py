from django.core.management.base import BaseCommand, CommandError
from api.models import WeatherData
import requests
import json
from datetime import datetime
from pytz import timezone
import pytz
from django.conf import settings

class Command(BaseCommand):
    help = 'Import aemet data'

    def handle(self, *args, **options):


      apikey = settings.AEMET_API_KEY
      currtz = settings.TIME_ZONE

      requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
      try:
        requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += 'HIGH:!DH:!aNULL'
      except AttributeError:
        # no pyopenssl support used / needed / available
        pass

      url = "https://opendata.aemet.es/opendata/api/observacion/convencional/datos/estacion/5783"
      querystring = {"api_key":apikey}
      headers = {
        'cache-control': "no-cache"
      }
      response = requests.request("GET", url, headers=headers, params=querystring)
      json_data = json.loads(response.text)
      #print(json_data)
      #print(json_data['datos'])
      dataurl = json_data['datos']
      response = requests.request("GET", dataurl, headers=headers)
      json_data = json.loads(response.text)
      #print(json_data)
      for w in json_data:
        #get naive date
        wdate = datetime.fromisoformat(w['fint'])
        #print(wdate)
        #add timezone info
        wdatetz = timezone(currtz).localize(wdate)
        #print(wdatetz)
        #convert to UTC
        wdateutc = wdatetz.astimezone(pytz.timezone('Etc/UTC'))
        #print(wdateutc)
        #check if object is already added
        weathert = WeatherData.objects.filter(date__contains=wdateutc.strftime('%Y-%m-%d %H:%M:%S.%f'))
        if weathert.count() > 0:
          #print ('exists')
          pass
        else:
          #print(w['ta'])
          try:
            weather = WeatherData(name='aeropuerto', temperature=w['ta'], humidity=w['hr'], pressure=w['pres'], date=wdateutc)
            weather.save()
          except:
            pass
