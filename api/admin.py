from django.contrib import admin
from .models import SensorData
from .models import FlowData
from .models import WateringConf
from .models import SystemConf
from .models import WeatherData
from .models import LogData


class SensorDataAdmin(admin.ModelAdmin):
    model = SensorData
    list_display = [
        'name',
        'temperature',
        'humidity',
        'pressure',
        'date',
    ]
    list_filter = ['name']

class FlowDataAdmin(admin.ModelAdmin):
    model = FlowData
    list_display = [
        'name',
        'liters',
        'date',
    ]
    list_filter = ['name']

class WateringConfAdmin(admin.ModelAdmin):
    model = WateringConf
    list_display = [
        'name',
        'factor',
    ]

class SystemConfAdmin(admin.ModelAdmin):
    model = SystemConf
    list_display = [
        'name',
        'H1',
        'H2',
        'H3',
        'H4',
        'L',
        'M',
        'X',
        'J',
        'V',
        'S',
        'D',
        'hour1',
        'hour2',
        'hour3',
        'hour4',
        'min',
    ]

class WeatherDataAdmin(admin.ModelAdmin):
    model = WeatherData
    list_display = [
        'name',
        'temperature',
        'humidity',
        'pressure',
        'date',
    ]

class LogDataAdmin(admin.ModelAdmin):
    model = LogData
    list_display = [
        'name',
        'error',
        'date',
    ]
    list_filter = ['name']

admin.site.register(SensorData, SensorDataAdmin)
admin.site.register(FlowData, FlowDataAdmin)
admin.site.register(WateringConf, WateringConfAdmin)
admin.site.register(SystemConf, SystemConfAdmin)
admin.site.register(WeatherData, WeatherDataAdmin)
admin.site.register(LogData, LogDataAdmin)
