from django.db import models
from django.core.validators import int_list_validator
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class SensorData(models.Model):
    name = models.CharField(max_length=50)
    temperature = models.FloatField(blank=False,default=-1)
    humidity = models.FloatField(blank=False,default=-1)
    pressure = models.FloatField(blank=False,default=-1)
    date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name

class FlowData(models.Model):
    name = models.CharField(max_length=50)
    liters = models.FloatField(blank=False,default=-1)
    date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name

class WateringConf(models.Model):
    name = models.CharField(max_length=50)
    factor = models.IntegerField(blank=False, default=0)

    def __unicode__(self):
        return self.name

class SystemConf(models.Model):
    name = models.CharField(max_length=50)
    enable = models.BooleanField(default=True, blank=False)
    L = models.BooleanField(default=False, blank=False)
    M = models.BooleanField(default=False, blank=False)
    X = models.BooleanField(default=False, blank=False)
    J = models.BooleanField(default=False, blank=False)
    V = models.BooleanField(default=False, blank=False)
    S = models.BooleanField(default=False, blank=False)
    D = models.BooleanField(default=False, blank=False)
    H1 = models.IntegerField(default=800, validators=[ MaxValueValidator(1000), MinValueValidator(1) ])
    H2 = models.IntegerField(default=800, validators=[ MaxValueValidator(1000), MinValueValidator(1) ])
    H3 = models.IntegerField(default=800, validators=[ MaxValueValidator(1000), MinValueValidator(1) ])
    H4 = models.IntegerField(default=800, validators=[ MaxValueValidator(1000), MinValueValidator(1) ])
    hour1 = models.IntegerField(blank=True,default=-1)
    hour2 = models.IntegerField(blank=True,default=-1)
    hour3 = models.IntegerField(blank=True,default=-1)
    hour4 = models.IntegerField(blank=True,default=-1)
    min = models.IntegerField(blank=True,default=0)
    
    def __unicode__(self):
        return self.name

class WeatherData(models.Model):
    name = models.CharField(max_length=50)
    temperature = models.FloatField(blank=False,default=-1)
    humidity = models.FloatField(blank=False,default=-1)
    pressure = models.FloatField(blank=False,default=-1)
    date = models.DateTimeField(auto_now=False)

    def __unicode__(self):
        return self.name


class LogData(models.Model):
    name = models.CharField(max_length=50)
    error = models.CharField(max_length=50)
    date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name
