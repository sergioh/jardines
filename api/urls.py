from django.urls import include, path
from rest_framework import routers
from api import views

app_name = 'api'

router = routers.DefaultRouter()
router.register(r'sensor', views.SensorDataViewSet)
router.register(r'flow', views.FlowDataViewSet)
router.register(r'watering', views.WateringConfViewSet)
router.register(r'weather', views.WeatherViewSet)
router.register(r'system', views.SystemConfViewSet)
router.register(r'log', views.LogDataViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
