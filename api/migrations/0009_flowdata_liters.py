# Generated by Django 2.2.20 on 2021-06-20 18:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_sensordata_humidity'),
    ]

    operations = [
        migrations.AddField(
            model_name='flowdata',
            name='liters',
            field=models.FloatField(default=-1),
        ),
    ]
