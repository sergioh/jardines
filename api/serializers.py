from django.utils import timezone
import pytz
from rest_framework import serializers
from .models import SensorData
from .models import FlowData
from .models import WateringConf
from .models import SystemConf
from .models import LogData
from .models import WeatherData

class SensorDataSerializer(serializers.ModelSerializer):
    
    fdate = serializers.SerializerMethodField()
    mapped_humidity = serializers.SerializerMethodField()
    
    def get_mapped_humidity(self, obj):
        if obj.humidity > 500:
            return 200 - obj.humidity/5
        return obj.humidity
    
    def get_fdate(self, obj):
        d = timezone.localtime(obj.date, pytz.timezone('Europe/Madrid'))
        return d.strftime("%m/%d/%Y %H:%M:%S")
        
    class Meta:
        model = SensorData
        fields = [ 'id','name','temperature', 'humidity', 'mapped_humidity','pressure','fdate' ]

class FlowDataSerializer(serializers.ModelSerializer):
    
    fdate = serializers.SerializerMethodField()
    
    def get_fdate(self, obj):
        d = timezone.localtime(obj.date, pytz.timezone('Europe/Madrid'))
        return d.strftime("%m/%d/%Y %H:%M:%S")
    
    class Meta:
        model = FlowData
        fields = ['id','name','liters','fdate']

class WateringConfSerializer(serializers.ModelSerializer):
    n = serializers.CharField(source='name')
    k = serializers.IntegerField(source='factor')
    class Meta:
        model = WateringConf
        fields = ['n','k',]

class SystemConfSerializer(serializers.ModelSerializer):
    dow = serializers.SerializerMethodField()
    hum = serializers.SerializerMethodField()

    def get_dow(self, obj):
        return [0,int(obj.D), int(obj.L), int(obj.M), int(obj.X), int(obj.J), int(obj.V), int(obj.S)]

    def get_hum(self, obj):
        return [0,int(obj.H1), int(obj.H2), int(obj.H3), int(obj.H4)]

    class Meta:
        model = SystemConf
        fields = ['id','name','hour1','hour2','hour3','hour4','min','dow','hum']

class LogDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogData
        fields = ['id','name','error','date',]

class WeatherDataSerializer(serializers.ModelSerializer):
    
    fdate = serializers.SerializerMethodField()
    
    def get_fdate(self, obj):
        return obj.date.strftime("%m/%d/%Y %H:%M:%S")
        
    class Meta:
        model = WeatherData
        fields = ['id', 'fdate','temperature' ]
